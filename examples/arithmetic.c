#include<inttypes.h>

void printInt(int64_t n) {
    if (n < 0) {
        putchar('-');
        n = -n;
    }
    if (n / 10 != 0)
        printInt(n / 10);
    putchar((n % 10) + '0');
}

int64_t sum(int64_t a, int64_t b) {
  return a+b;
}

int64_t main() {
    int64_t i = 0;

    //expected 23
    printInt(1+sum(5,sum(sum(5+6,sum(i++, 5)),i)));
    putchar('\n');

    int64_t a = 1;

    // expected = 2
    printInt((a*2+3%2-2)<<1);

    putchar('\n');
}
