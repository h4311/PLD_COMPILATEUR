#include<inttypes.h>

int64_t foo(char a, int64_t x1, int64_t x2) {
    int64_t y = 5;
    putchar(a);
    putchar('\n');

    return x1+x2;
}

void foo2(char a, char b) {
    if(a == b) {
        char c = a+b;
        putchar(c);
    }
    putchar(a);
    putchar(b);
    putchar('\n');
}

int64_t main() {

    int64_t a = 2;
    int32_t i[2*2] = {1, 2};
    a = 1+2+3;
    int64_t b = a/2;

    putchar(a+48);
    putchar(b+48);
    putchar(2*24);
    putchar('\n');

    i[0] = foo('A', a, b);

    int32_t z = 33*2;
    putchar(z);
    putchar('\n');
}
