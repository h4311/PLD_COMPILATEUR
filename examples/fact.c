#include<inttypes.h>

int64_t fact(int64_t n) {
    if (n <= 0) {
        return 1;
    }
    return n*fact(n-1);
}

int64_t main () {
    //5! = 120 and 120 is 'x' in ASCII
    putchar(fact(5));
    putchar('\n');

    return 0;
}
