#include<inttypes.h>

void printInt(int64_t n) {
    if (n < 0) {
        putchar('-');
        n = -n;
    }
    if (n / 10 != 0)
        printInt(n / 10);
    putchar((n % 10) + '0');
}

int64_t main() {
    int64_t i = 0;
    while(i<10) {
        printInt(i);
        putchar('\n');
        int64_t j = 0;
        while(j<10) {
            printInt(j);
            if(j>i) {
                break;
            }
            j++;
        }
        putchar('\n');
        i++;
    }
    putchar('\n');
}
