#include<inttypes.h>

int64_t main() {
    char a = 'A';

    putchar(a);
    putchar('\n');

    {
        putchar(a);
        char a = 'a';
        putchar(a);
        putchar('\n');
    }

    putchar(a);
    putchar('\n');
}
