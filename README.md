
# Sub-C Compiler Project (H4311)

This git repo contains our work on developing a compiler.
The goal was to implement a semantic analysis of a sub-C language and then to
transform it in assembly language for X86-64 processors.

## Getting Started

This project is built with CMake and uses
[Antlr4](http://www.antlr.org/download.html).
Make sure that you have them before compiling the project.

### Prerequisites

For Antlr it is better to use `make install`. Make sure that the library files and the includes are in **/usr/local**.

You will also need a recent version of g++ to compile the project so try
to update it. It works fine with version 5.4.0.

### Installing

First, clone our git repository :
```
git clone https://gitlab.com/h4311/PLD_COMPILATEUR.git
cd PLD_COMPILATEUR
```
Then, build the project :
```
mkdir build
cd build
cmake ../
make
```
Congratulations, you can know use our gorgeous compiler. The program is called
**PLDComp**.

### Usage
```
./PLDComp [-a] [-o] [-c OUT_FILENAME] IN_FILENAME
```
* `-a`: perform Static Analysis
* `-o`: perform optimization (not implemented)
* `-c OUT_FILENAME`: to specify the output file. The default
file is `main.s`

Now, you can try to compile any program you want.
If you want, the directory [examples](/examples) contains some programs that
show what our compiler can do.

## Running the tests

In the build directory you can execute the tests with the command `test/runTests`.
These tests are made with the [Google Test](https://github.com/google/googletest)
framework.

These tests are for integration purpose and do not use the examples.

## Implemented features

In the front end part, we can evaluate every features defined by the
[grammar](/code/antlr4/Cprogram.g4) and put it in our data structure. Sadly some
of these features are not implemented in the back. Then if a not implemented
feature is used, an error occurs.

What differs from the [subject](/doc/projet.pdf) :
* Global variables are not implemented in the back-end.
* Functions cannot have more than 6 parameters.
* Arrays cannot be used as parameters.
* Bracket initialization can be used for arrays and simple variables too
(yes it is legal).
* The size of an array can be specified with an expression without any symbol, which
will be computed statically.
* Every C operator is implemented, such as the comma (',') the post/prefix
incrementation and decrementation, all the operation assignments ('+=', '*=',  …), etc.
* Multiple and embedded scopes are handled, especially the anonymous scopes
(not useful but it works).
* `break` and `continue` are **implemented** !
* The optimization of the size of the variables in the stack is not done. Each
variable and temporary variable is stored on 8 bytes in the stack.
* The temporary variables are not recycled thus the stack is very big.
* The option `-o` is not implemented, however we perform an optimization anway.
We skip instructions that do not interfere with memory. It does
not skip expressions containing function_calls whether or not they interfere with
memory.
* The static analysis (option `-a`) detects unused variables or functions, and
detects if variables are used but not initialized. However it does not really work
because it only uses the initializations at declaration.
* A tool for error and warning logging has been developped.

## Authors

* Andries Dan
* Arhiliuc Cristina
* Burca Horia
* Céard Axel
* Gonnet Timothee
* Turpin Laurent

