//
// Created by axce on 26/03/18.
//
#pragma once

#include <iostream>
#include <string>
#include <ParserRuleContext.h>
#include "CprogramBaseVisitor.h"
#include "../data_structure/function_definition.h"

using namespace std;
using namespace antlr4;
using namespace antlrcpp;

enum ErrorType {
    SymbolRedefinition, UndefinedReference, ParameterRedefinition, InitializerElementNotConstant,
    VariableNeverDeclared, FunctionNeverDeclared, BreakNotInLoop, ContinueNotInLoop, TooFewArguments, TooManyArguments,
    NotAFunction, NotAVariableId, RValueHasVoid, NegativeArraySize, NotImplemented
};
enum WarningType {
    WrongMainPrototype, VariableUnused, FunctionUnused, ParameterUnused, VariableNotInitialized
};

class ErrorWarningLog {
public:
    static void
    LogWarning(WarningType warningType, antlr4::ParserRuleContext *ctx, const string &warningParameter = "");

    static void LogError(ErrorType errorType, antlr4::ParserRuleContext *ctx, const string &errorParameter = "");

    //pour l'analyse statique
    static void LogWarning(WarningType warningType, int lineNumber, string warningParameter);

    static void LogWarningMain(FunctionDefinition *mainDefinition, ParserRuleContext *ctx);

    static bool error;
    static const string white;
    static const string reset;
};