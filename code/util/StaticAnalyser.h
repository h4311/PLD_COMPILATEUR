//
// Created by Dannemp on 21/03/2018.
//
#pragma once

#include "../data_structure/program.h"
#include "../data_structure/variable_declaration.h"

class StaticAnalyser {
public:
    StaticAnalyser(Program *prog) : program(prog) {}

    void startAnalysis();

    void Analyse();

protected:
    Program *program;
};
