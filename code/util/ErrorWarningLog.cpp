//
// Created by axce on 26/03/18.
//

#include <misc/Interval.h>
#include "ErrorWarningLog.h"
#include "../data_structure/function_definition.h"

using namespace std;
using namespace antlr4;
using namespace antlrcpp;

bool ErrorWarningLog::error = false;
const string ErrorWarningLog::white = "\033[0;37m";
const string ErrorWarningLog::reset = "\033[0m";

void
ErrorWarningLog::LogWarning(WarningType warningType, antlr4::ParserRuleContext *ctx, const string &warningParameter) {
    size_t lineNumber = ctx->getStart()->getLine();
    size_t columnNumber = ctx->getStart()->getCharPositionInLine();

    //getting full source code
    istringstream is(ctx->getStart()->getInputStream()->toString());
    string lineText;

    //retrieving wanted line
    for (int i = 0; i < lineNumber; i++)
        getline(is, lineText);

    cerr << "Warning ! ";

    switch (warningType) {
        case (WrongMainPrototype):
            cerr << "Prototype for 'main' should be 'int64_t main()' or 'int32_t main()'";
            break;
        default:
            cerr << "Unknown warning";
    }

    cerr << "." << endl;
    cerr << "at line " << lineNumber << " :" << endl;
    cerr << lineText << endl;
    //"^" character indicator
    for (int i = 0; i < columnNumber; i++)
        cerr << " ";
    cerr << "^" << endl;
}

void ErrorWarningLog::LogError(ErrorType errorType, antlr4::ParserRuleContext *ctx, const string &errorParameter) {
    size_t lineNumber = ctx->getStart()->getLine();
    //getting full source code and retrieving wanted line
    istringstream is(ctx->getStart()->getInputStream()->toString());
    string lineText;
    for (int i = 0; i < lineNumber; i++) {
        getline(is, lineText);
    }
    cerr << "line " << lineNumber << ": error: ";
    switch (errorType) {
        case (SymbolRedefinition):
            cerr << "redefinition of symbol '" << errorParameter << "'";
            // TODO show where first definition was?
            break;
        case (UndefinedReference):
            cerr << "undefined reference to '" << errorParameter << "'";
            break;
        case (ParameterRedefinition):
            cerr << "redefinition of parameter '" << errorParameter << "'";
            break;
        case (InitializerElementNotConstant):
            cerr << "initializer element is not constant for '" << errorParameter << "'";
            break;
        case (VariableNeverDeclared):
            cerr << "the variable '" << errorParameter << "' is never declared";
            break;
        case (FunctionNeverDeclared):
            cerr << "the function '" << errorParameter << "' is never declared";
            break;
        case (BreakNotInLoop):
            cerr << "'break' is not in loop";
            break;
        case (ContinueNotInLoop):
            cerr << "'continue' is not in loop";
            break;
        case (NotAVariableId):
            cerr << "called object '" << errorParameter << "' is not a variable ID";
            break;
        case (NotAFunction):
            cerr << "called object '" << errorParameter << "' is not a function";
            break;
        case (TooFewArguments):
            cerr << "too few arguments to function '" << errorParameter << "'";
            break;
        case (TooManyArguments):
            cerr << "too many arguments to function '" << errorParameter << "'";
            break;
        case (RValueHasVoid):
            cerr << "void operand used in rvalue expression";
            break;
        case NegativeArraySize:
            cerr << "size of array '" << errorParameter << "' is negative";
            break;
        case NotImplemented:
            cerr << "sorry but this feature is not implemented yet";
            break;
        default:
            cerr << "unknown error";
    }
    cerr << endl << white << lineText << reset << endl;

    error = true;
}

void ErrorWarningLog::LogWarning(WarningType warningType, int lineNumber, string warningParameter) {
    cerr << "line " << lineNumber << ": warning: ";
    switch (warningType) {
        case (VariableUnused):
            cerr << "the declared variable '" << warningParameter << "' is never used";
            break;
        case (FunctionUnused):
            cerr << "the declared function '" << warningParameter << "' is never used";
            break;
        case (ParameterUnused):
            cerr << "the function parameter '" << warningParameter << "' is never used";
            break;
        case (VariableNotInitialized):
            cerr << "the declared variable '" << warningParameter << "' is never initialized";
            break;
        default:
            cerr << "unknown warning";
    }
    cerr << endl;
}

void ErrorWarningLog::LogWarningMain(FunctionDefinition *mainDefinition, antlr4::ParserRuleContext *programContext) {
    size_t lineNumber = mainDefinition->GetLineNr_();

    //getting full source code
    istringstream is(programContext->getStart()->getInputStream()->toString());
    string lineText;

    //retrieving wanted line
    for (int i = 0; i < lineNumber; i++)
        getline(is, lineText);

    cerr << "Warning ! ";

    cerr << "Prototype for 'main' should be 'int64_t main()' or 'int32_t main()'";

    cerr << "." << endl;
    cerr << "at line " << lineNumber << " :" << endl;
    cerr << lineText << endl;
    cerr << "^" << endl;
}