//
// Created by Dannemp on 21/03/2018.
//

#include <set>
#include "StaticAnalyser.h"
#include "ErrorWarningLog.h"
#include "../data_structure/data_structure.h"

void StaticAnalyser::startAnalysis() {
    std::set<VariableDeclaration *> globalVariableDeclarations;
    std::set<FunctionDefinition *> functionDefinitions;
    std::set<string> globalVariableIdInitialized;
    for (Definition *definition: this->program->GetDefinitions_()) {
        VariableDeclaration *variableDeclaration = dynamic_cast<VariableDeclaration *> (definition);
        if (variableDeclaration != NULL) {
            globalVariableDeclarations.insert(variableDeclaration);
        }
        FunctionDefinition *functionDefinition = dynamic_cast<FunctionDefinition *> (definition);
        if (functionDefinition != NULL) {
            functionDefinitions.insert(functionDefinition);
        }
    }
    //for each function check its variables
    for (FunctionDefinition *functionDefinition : functionDefinitions) {
        std::set<VariableDeclaration *> localVariableDeclarations;
        std::set<string> localVariableIdInitialized;
        Block *functionBlock = functionDefinition->GetBlock_();
        for (Statement *statement:functionBlock->GetStatements_()) {
            if (VariableDeclaration *variableDeclaration = dynamic_cast<VariableDeclaration *>(statement)) {
                localVariableDeclarations.insert(variableDeclaration);
            } else if (BinaryOpAssignment *binaryOpAssignment = dynamic_cast<BinaryOpAssignment *>(statement)) {
                bool found = false;
                for (VariableDeclaration *var: localVariableDeclarations) {
                    if (var->GetId_() == binaryOpAssignment->GetVariableId_()->GetId_()) {
                        localVariableIdInitialized.insert(binaryOpAssignment->GetVariableId_()->GetId_());
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    globalVariableIdInitialized.insert(binaryOpAssignment->GetVariableId_()->GetId_());
                }
            }
        }

        for (VariableDeclaration *var: localVariableDeclarations) {
            if (!var->IsUsed_()) {
                ErrorWarningLog::LogWarning(VariableUnused, var->GetLineNr_(),
                                            var->GetUnrenamedId_(functionBlock->GetName_()));
            } else if (var->GetValues_().empty()
                       && localVariableIdInitialized.find(var->GetId_()) == localVariableIdInitialized.end()) {
                ErrorWarningLog::LogWarning(VariableNotInitialized, var->GetLineNr_(),
                                            var->GetUnrenamedId_(functionBlock->GetName_()));
            }
        }
        // check if parameters are used
        for (VariableDeclaration *parameter: functionDefinition->GetParameters_()) {
            if (!parameter->IsUsed_()) {
                ErrorWarningLog::LogWarning(ParameterUnused, parameter->GetLineNr_(),
                                            parameter->GetUnrenamedId_(functionBlock->GetName_()));
            }
        }
    }

    for (VariableDeclaration *var: globalVariableDeclarations) {
        if (!var->IsUsed_()) {
            ErrorWarningLog::LogWarning(VariableUnused, var->GetLineNr_(), var->GetId_());
        } else if (var->GetValues_().empty()
                   && globalVariableIdInitialized.find(var->GetId_()) == globalVariableIdInitialized.end()) {
            ErrorWarningLog::LogWarning(VariableNotInitialized, var->GetLineNr_(), var->GetId_());
        }
    }
    for (FunctionDefinition *var: functionDefinitions) {
        if (!var->IsUsed_() && var->GetId_() != "main") {
            ErrorWarningLog::LogWarning(FunctionUnused, var->GetLineNr_(), var->GetId_());
        }
    }
}