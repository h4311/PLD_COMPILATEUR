#include <iostream>
#include <memory>
#include <getopt.h>
#include <cstring>
#include "antlr4-runtime.h"
#include "CprogramLexer.h"
#include "CprogramParser.h"
#include "antlr4/my_c_program_visitor.h"
#include "data_structure/program.h"
#include "util/StaticAnalyser.h"
#include "util/ErrorWarningLog.h"

#define DEFAULT_OUT_FILE_NAME "main.s"
#define DEFAULT_OUT_FILE_NAME_LENGTH 7
#define USAGE "Usage : ./PLDComp [-a] [-o] [-c OUT_FILENAME] IN_FILENAME"
using namespace antlr4;
using namespace std;

void createElfFile(ofstream &elf) {
    elf << ".text" << endl;
    elf << ".global main" << endl;
    elf << endl;
}

int main(int argc, char **argv) {

    bool static_analysis = false;
    bool optimisation = false;
    char *out_file_name = nullptr;
    char *in_file_name = nullptr;
    //Do not delete these two pointer : the will come from argv
    int c = 0;

    while ((c = getopt(argc, argv, "aoc:")) != -1) {
        switch (c) {
            case 'a':
                static_analysis = true;
                break;
            case 'o':
                optimisation = true;
                break;
            case 'c':
                out_file_name = optarg;
                break;
            default:
                std::cerr << USAGE << std::endl;
                exit(EXIT_FAILURE);
        }
    }

    for (int i = optind; i < argc; i++)
        if (in_file_name == nullptr)
            in_file_name = argv[i];
        else {
            std::cerr << "Please, specify only one input file" << std::endl;
            std::cerr << USAGE << std::endl;
            exit(EXIT_FAILURE);
        }

    if (in_file_name == nullptr) {
        std::cerr << "Please specify an input file" << std::endl;
        std::cerr << USAGE << std::endl;
        exit(EXIT_FAILURE);
    }

    if (out_file_name == nullptr) {
        out_file_name = new char(DEFAULT_OUT_FILE_NAME_LENGTH);
        strcpy(out_file_name, DEFAULT_OUT_FILE_NAME);
    }

    if (static_analysis) {
        std::cout << "Static analysis activated" << std::endl;
    }
    if (optimisation) {
        std::cout << "Optimisation activated" << std::endl;
    }

    ifstream src_file(in_file_name);
    if (!src_file) {
        cerr << "ERROR : cannot open input file : " << in_file_name << endl;
        exit(EXIT_FAILURE);
    }

    ofstream elf(out_file_name);
    if (!elf) {
        cerr << "ERROR : cannot open output file : " << out_file_name << endl;
        exit(EXIT_FAILURE);
    }

    std::cout << "Output file : " << out_file_name << std::endl;

    ANTLRInputStream input(src_file);
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);
    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    if (parser.getNumberOfSyntaxErrors() > 0) {
        exit(EXIT_FAILURE);
    }

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();

    if (ErrorWarningLog::error) {
        exit(EXIT_FAILURE);
    }

    cout << "Semantic analysis done" << endl;
    if (static_analysis) {
        StaticAnalyser analyser(program);
        analyser.startAnalysis();
        cout << "Static analysis done" << endl;
    }

    createElfFile(elf);

    program->BuildIR();
    cout << "IR generation done" << endl;

    program->GenerateAssembler(elf);
    cout << "IR to Assembly language done" << endl;

    delete program;
    cout << "Compilation Done" << endl;

    return 0;
}
