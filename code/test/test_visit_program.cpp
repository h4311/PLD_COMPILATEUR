//
// Created by burca on 20/03/2018.
//
#include <CprogramLexer.h>
#include "gtest/gtest.h"
#include "../antlr4/my_c_program_visitor.h"
#include "../antlr4/CprogramLexer.h"
#include "../data_structure/block.h"
#include "../data_structure/function_definition.h"
#include "../data_structure/program.h"
#include "../data_structure/return.h"
#include "../data_structure/binary_op_expression.h"
#include "../data_structure/function_call.h"
#include "../data_structure/if.h"
#include "../data_structure/binary_op_assignment.h"
#include "../data_structure/const.h"
#include "../data_structure/break.h"
#include "../data_structure/while.h"

#define TEST_DIR "../../test/FrontEndTests/"

using namespace std;
using namespace antlrcpp;
using namespace antlr4;

TEST(visit_program, char_const_special) {
    ifstream program_file(TEST_DIR "ValidPrograms/51_CharConstSpecial.c", ios::in);
    ANTLRInputStream input(program_file);
    vector<Statement *> statements =
            {new VariableDeclaration(CHAR, "a_block1", nullptr),
             new BinaryOpAssignment(new VariableId("a_block1"), new Const(CHAR, 10), new Operator(ASSIGNMENT))};
    Block *expectedFunctionBlock = new Block(statements);
    FunctionDefinition *testFunction = new FunctionDefinition(VOID, "main", vector<VariableDeclaration *>());
    testFunction->AddBlock_(expectedFunctionBlock);
    Program expected({testFunction});
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    ASSERT_TRUE(program->Equals(expected));
    delete program;
}

TEST(visit_program, return_instruction) {
    // edit configurations ... -> set working directory to test directory
    ifstream program_file(TEST_DIR "ValidPrograms/55_Return.c", ios::in);
    ANTLRInputStream input(program_file);
    //first function
    vector<VariableDeclaration *> expectedParams1 = {new VariableDeclaration(INT32_T, "a_block1", nullptr)};
    vector<Statement *> expectedStatements1 = {new Return(new BinaryOpExpression(new VariableId("a_block1"),
                                                                                 new Const(INT32_T, 1),
                                                                                 new Operator(ADDITION)))};
    FunctionDefinition *expectedFunction1 = new FunctionDefinition(INT32_T, "function", expectedParams1);
    expectedFunction1->AddBlock_(new Block(expectedStatements1));
    //second function
    vector<Statement *> statements2 = {new VariableDeclaration(INT32_T, "a_block2", nullptr),
                                       new BinaryOpAssignment(new VariableId("a_block2"), new Const(INT32_T, 1),
                                                              new Operator(ASSIGNMENT)),
                                       new BinaryOpAssignment(new VariableId("a_block2"),
                                                              new FunctionCall("function", vector<Expression *>{
                                                                      new VariableId("a_block2")}),
                                                              new Operator(ASSIGNMENT))};
    FunctionDefinition *expectedFunction2 = new FunctionDefinition(VOID, "main", vector<VariableDeclaration *>());
    expectedFunction2->AddBlock_(new Block(statements2));
    vector<Definition *> expectedDefinitions = {expectedFunction1, expectedFunction2};
    Program expected(expectedDefinitions);
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    ASSERT_TRUE(program->Equals(expected));
    delete program;
}

TEST(visit_program, if_else_if) {
    ifstream program_file(TEST_DIR "ValidPrograms/44_IfElseIf.c", ios::in);
    ANTLRInputStream input(program_file);
    Statement *expected_if_true = new Block(
            {new BinaryOpAssignment(new VariableId("a_block1"), new Const(INT32_T, 2), new Operator(ASSIGNMENT))});
    Statement *expected_if2_true = new Block(
            {new BinaryOpAssignment(new VariableId("a_block1"), new Const(INT32_T, 3), new Operator(ASSIGNMENT))});
    Statement *expected_if_false = new Block({
                                                     new If(new BinaryOpExpression(new VariableId("a_block1"),
                                                                                   new Const(INT32_T, 2),
                                                                                   new Operator(EQUAL)),
                                                            expected_if2_true)});
    vector<Statement *> statements = {new VariableDeclaration(INT32_T, "a_block1", nullptr),
                                      new BinaryOpAssignment(new VariableId("a_block1"), new Const(INT32_T, 1),
                                                             new Operator(ASSIGNMENT)),
                                      new If(new BinaryOpExpression(new VariableId("a_block1"),
                                                                    new Const(INT32_T, 1),
                                                                    new Operator(EQUAL)),
                                             expected_if_true, expected_if_false)};
    Block *expectedFunctionBlock = new Block(statements);
    FunctionDefinition *mainFunction = new FunctionDefinition(VOID, "main", vector<VariableDeclaration *>());
    mainFunction->AddBlock_(expectedFunctionBlock);
    Program expected({mainFunction});
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    ASSERT_TRUE(program->Equals(expected));
    delete program;
}

TEST(visit_program, while_break) {
    ifstream program_file(TEST_DIR "ValidPrograms/59_MixedDeclarationsInitializations.c", ios::in);
    ANTLRInputStream input(program_file);
    Statement *if_statement =
            new If(new BinaryOpExpression(new VariableId("a_block2"), new Const(INT32_T, 4), new Operator(EQUAL)),
                   new Block({new Break()}));
    Statement *while_statement =
            new While(new BinaryOpExpression(new VariableId("a_block1"), new Const(INT32_T, 3), new Operator(EQUAL)),
                      new Block({new VariableDeclaration(INT32_T,
                                                         "a_block2",
                                                         new Const(INT32_T, 2)),
                                 new BinaryOpAssignment(new VariableId("a_block2"),
                                                        new Const(INT32_T, 2),
                                                        new Operator(ASSIGNMENT_SUM)),
                                 if_statement}));
    Block *main_function_block = new Block({new VariableDeclaration(INT64_T,
                                                                    "a_block1",
                                                                    new Const(
                                                                            INT64_T, 3)),
                                            new VariableDeclaration(INT64_T,
                                                                    "b_block1",
                                                                    new Const(
                                                                            INT64_T, 2)),
                                            new VariableDeclaration(INT64_T,
                                                                    "c_block1",
                                                                    nullptr),
                                            while_statement});
    FunctionDefinition
            *main_function = new FunctionDefinition(VOID, "main", vector<VariableDeclaration *>());
    main_function->AddBlock_(main_function_block);
    Program expected({main_function});
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    ASSERT_TRUE(program->Equals(expected));
    delete program;
}

