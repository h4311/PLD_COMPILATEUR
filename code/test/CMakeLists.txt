add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/lib/googletest-master)

add_executable(runTests $<TARGET_OBJECTS:data_structure>
        test_visit_variable_declaration.cpp
        test_visit_function_definition.cpp
        test_visit_expressions.cpp
        test_visit_program.cpp
        test_variable_declaration.cpp
        test_semantic_analysis.cpp
        test_break_continue_in_loop.cpp
        test_analyse_static.cpp
        test_intermediate_types.cpp)

target_link_libraries(runTests gtest gtest_main gmock gmock_main)
target_link_libraries(runTests antlr4)