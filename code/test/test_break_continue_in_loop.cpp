//
// Created by Tim on 26/03/2018.
//

#include "gtest/gtest.h"
#include "../antlr4/my_c_program_visitor.h"
#include "../antlr4/CprogramLexer.h"
#include "../data_structure/program.h"
#include "../data_structure/if.h"
#include "../data_structure/binary_op_expression.h"
#include "../data_structure/variable_id.h"
#include "../data_structure/const.h"
#include "../data_structure/break.h"
#include "../data_structure/while.h"
#include "../data_structure/binary_op_assignment.h"

#define TEST_DIR "../../test/FrontEndTests/"

using namespace std;
using namespace antlrcpp;
using namespace antlr4;

TEST(break_continue_in_loop, break_in_loop) {
    ifstream program_file(TEST_DIR "ValidPrograms/59_MixedDeclarationsInitializations.c", ios::in);
    ANTLRInputStream input(program_file);
    Statement *if_statement =
            new If(new BinaryOpExpression(new VariableId("a_block2"), new Const(INT32_T, 4), new Operator(EQUAL)),
                   new Block({new Break()}));
    Statement *while_statement =
            new While(new BinaryOpExpression(new VariableId("a_block1"), new Const(INT32_T, 3), new Operator(EQUAL)),
                      new Block({new VariableDeclaration(INT32_T,
                                                         "a_block2",
                                                         new Const(INT32_T, 2)),
                                 new BinaryOpAssignment(new VariableId("a_block2"),
                                                        new Const(INT32_T, 2),
                                                        new Operator(ASSIGNMENT_SUM)),
                                 if_statement}));
    Block *main_function_block = new Block({new VariableDeclaration(INT64_T,
                                                                    "a_block1",
                                                                    new Const(
                                                                            INT64_T, 3)),
                                            new VariableDeclaration(INT64_T,
                                                                    "b_block1",
                                                                    new Const(
                                                                            INT64_T, 2)),
                                            new VariableDeclaration(INT64_T,
                                                                    "c_block1",
                                                                    nullptr),
                                            while_statement});
    FunctionDefinition
            *main_function = new FunctionDefinition(VOID, "main", vector<VariableDeclaration *>());
    main_function->AddBlock_(main_function_block);
    Program expected({main_function});
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    ASSERT_TRUE(program->Equals(expected));
    delete program;
}

TEST(break_continue_in_loop, break_not_in_loop) {
    ifstream program_file(TEST_DIR "SemanticError/02_BreakNotInLoop.c", ios::in);
    ANTLRInputStream input(program_file);
    Statement *if_statement =
            new If(new BinaryOpExpression(new VariableId("a_block1"), new Const(INT32_T, 4), new Operator(EQUAL)),
                   new Block({new Break()}));
    Block *main_function_block = new Block({new VariableDeclaration(INT64_T,
                                                                    "a_block1",
                                                                    new Const(
                                                                            INT64_T, 3)),
                                            if_statement});
    FunctionDefinition
            *main_function = new FunctionDefinition(VOID, "main", vector<VariableDeclaration *>());
    main_function->AddBlock_(main_function_block);
    Program expected({main_function});
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    ASSERT_TRUE(program->Equals(expected));
    delete program;
}