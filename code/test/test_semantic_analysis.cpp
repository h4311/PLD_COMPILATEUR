//
// Created by Dannemp on 26/03/2018.
//
#include <CprogramLexer.h>
#include "gtest/gtest.h"
#include "../antlr4/my_c_program_visitor.h"
#include "../antlr4/CprogramLexer.h"
#include "../data_structure/block.h"
#include "../data_structure/function_definition.h"
#include "../data_structure/program.h"
#include "../data_structure/return.h"
#include "../data_structure/binary_op_expression.h"
#include "../data_structure/function_call.h"
#include "../data_structure/if.h"
#include "../data_structure/binary_op_assignment.h"
#include "../data_structure/const.h"
#include "../data_structure/break.h"
#include "../data_structure/while.h"

#define TEST_DIR "../../test/FrontEndTests/"

using namespace std;
using namespace antlrcpp;
using namespace antlr4;

TEST(semantic_analysis, missing_variable_declaration) {
    ifstream program_file(TEST_DIR "SemanticError/01_MissingVarDeclaration.c", ios::in);
    ANTLRInputStream input(program_file);

    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();

    //ASSERT_TRUE(program->Equals(expected));
    delete program;
}

TEST(semantic_analysis, missing_function_declaration) {
    ifstream program_file(TEST_DIR "SemanticError/02_MissingFunctionDeclaration.c", ios::in);
    ANTLRInputStream input(program_file);

    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();

    //ASSERT_TRUE(program->Equals(expected));
    delete program;
}

TEST(semantic_analysis, OperationOnVoid) {
    ifstream program_file(TEST_DIR "SemanticError/04_OperationOnVoid.c", ios::in);
    ANTLRInputStream input(program_file);

    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();

    //ASSERT_TRUE(program->Equals(expected));
    delete program;
}
