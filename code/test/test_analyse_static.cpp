//
// Created by Tim on 28/03/2018.
//

#include "gtest/gtest.h"
#include "../antlr4/my_c_program_visitor.h"
#include "../antlr4/CprogramLexer.h"
#include "../data_structure/program.h"
#include "../util/StaticAnalyser.h"

#define TEST_DIR "../../test/FrontEndTests/"

using namespace std;
using namespace antlrcpp;
using namespace antlr4;

TEST(analyse_static, unitialized_variable) {
    ifstream program_file(TEST_DIR "AnalyseStatic/01_VariableNotInitialized.c", ios::in);
    ANTLRInputStream input(program_file);

    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    StaticAnalyser analyser(program);
    analyser.startAnalysis();
    delete program;
}

TEST(analyse_static, unused_var) {
    ifstream program_file(TEST_DIR "AnalyseStatic/02_UnusedVar.c", ios::in);
    ANTLRInputStream input(program_file);

    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();

    StaticAnalyser analyser(program);
    analyser.startAnalysis();
    delete program;
}


