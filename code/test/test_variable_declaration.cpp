//
// Created by perewoulpy on 26/03/18.
//

#include <antlr4-runtime.h>
#include "gtest/gtest.h"
#include "../data_structure/variable_declaration.h"
#include "../data_structure/const.h"
#include "../data_structure/variable_id.h"
#include "../data_structure/function_call.h"
#include "../data_structure/binary_op_expression.h"

using namespace std;
using namespace antlrcpp;
using namespace antlr4;

TEST(test_variable_declaration, has_const_in_expression) {
    vector<Expression *> expressions = {new Const(VOID, 0)};
    VariableDeclaration *variableDeclaration = new VariableDeclaration(VOID, "vars", expressions, nullptr);

    ASSERT_FALSE(variableDeclaration->HasSymbolInExpression());
}

TEST(test_variable_declaration, has_var_id_in_expression) {
    vector<Expression *> expressions = {new VariableId("var")};
    VariableDeclaration *variableDeclaration = new VariableDeclaration(VOID, "vars", expressions, nullptr);

    ASSERT_TRUE(variableDeclaration->HasSymbolInExpression());
}

TEST(test_variable_declaration, has_call_in_expression) {
    vector<Expression *> expressions = {new FunctionCall("foo", {})};
    VariableDeclaration *variableDeclaration = new VariableDeclaration(VOID, "vars", expressions, nullptr);

    ASSERT_TRUE(variableDeclaration->HasSymbolInExpression());
}

TEST(test_variable_declaration, has_bin_op_in_expression) {

    vector<Expression *> expressions = {new BinaryOpExpression(new Const(VOID, 0), new FunctionCall("foo", {}),
                                                               nullptr)};
    VariableDeclaration *variableDeclaration = new VariableDeclaration(VOID, "vars", expressions, nullptr);

    ASSERT_TRUE(variableDeclaration->HasSymbolInExpression());
}


