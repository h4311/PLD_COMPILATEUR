//
// Created by burca on 18/03/2018.
//
#include "gtest/gtest.h"
#include "../antlr4/my_c_program_visitor.h"
#include "../antlr4/CprogramLexer.h"
#include "../data_structure/definition.h"
#include "../data_structure/variable_declaration.h"
#include "../data_structure/program.h"
#include "../data_structure/const.h"

using namespace std;
using namespace antlrcpp;
using namespace antlr4;

TEST(visit_variables_declaration, visit_variables_declaration_complete) {
    ANTLRInputStream input("int64_t a[2],b=2;");
    vector<Definition *> expectedDefinitions = {
            new VariableDeclaration(INT64_T, "a", vector<Expression *>(), new Const(INT32_T, 2)),
            new VariableDeclaration(INT64_T, "b", new Const(INT64_T, 2))};
    Program expected(expectedDefinitions);
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    ASSERT_TRUE(program->Equals(expected));
    delete program;
}