//
// Created by Dannemp on 28/03/2018.
//

#include "gtest/gtest.h"
#include "../antlr4/my_c_program_visitor.h"
#include "../antlr4/CprogramLexer.h"
#include "../data_structure/variable_declaration.h"
#include "../data_structure/block.h"
#include "../data_structure/function_definition.h"
#include "../data_structure/program.h"
#include "../data_structure/unary_op_assignment.h"
#include "../data_structure/const.h"
#include "../data_structure/data_structure.h"

#define TEST_DIR "../../test/FrontEndTests/"

using namespace std;
using namespace antlrcpp;
using namespace antlr4;

TEST(intermediate_types, unary_op_char) {
    ifstream program_file(TEST_DIR "IntermediateTypes/01_unary_op_char.c", ios::in);
    ANTLRInputStream input(program_file);
    CType expected = INT32_T;
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    Block *mainBlock = ((FunctionDefinition *) program->GetDefinitions_()[0])->GetBlock_();
    Statement *secondStatement = mainBlock->GetStatements_()[1];
    Expression *expression = (Expression *) secondStatement;
    CType type = expression->GetType_();
    ASSERT_TRUE(type == expected);
    delete program;
}

TEST(intermediate_types, unary_assignment_char) {
    ifstream program_file(TEST_DIR "IntermediateTypes/02_unary_assignment_char.c", ios::in);
    ANTLRInputStream input(program_file);
    CType expected = CHAR;
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    Block *mainBlock = ((FunctionDefinition *) program->GetDefinitions_()[0])->GetBlock_();
    Statement *secondStatement = mainBlock->GetStatements_()[1];
    Expression *expression = (Expression *) secondStatement;
    CType type = expression->GetType_();
    ASSERT_TRUE(type == expected);
    delete program;
}

TEST(intermediate_types, binary_op_integers) {
    ifstream program_file(TEST_DIR "IntermediateTypes/03_binary_op_integers.c", ios::in);
    ANTLRInputStream input(program_file);
    CType expected = INT64_T;
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    Block *mainBlock = ((FunctionDefinition *) program->GetDefinitions_()[0])->GetBlock_();
    Statement *secondStatement = mainBlock->GetStatements_()[2];
    Expression *expression = (Expression *) secondStatement;
    CType type = expression->GetType_();
    ASSERT_TRUE(type == expected);
    delete program;
}

TEST(intermediate_types, binary_op_integer_char) {
    ifstream program_file(TEST_DIR "IntermediateTypes/04_binary_op_integer_char.c", ios::in);
    ANTLRInputStream input(program_file);
    CType expected = INT64_T;
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    Block *mainBlock = ((FunctionDefinition *) program->GetDefinitions_()[0])->GetBlock_();
    Statement *secondStatement = mainBlock->GetStatements_()[2];
    Expression *expression = (Expression *) secondStatement;
    CType type = expression->GetType_();
    ASSERT_TRUE(type == expected);
    delete program;
}

TEST(intermediate_types, binary_assignment_integer_char) {
    ifstream program_file(TEST_DIR "IntermediateTypes/05_binary_assignment_integer_char.c", ios::in);
    ANTLRInputStream input(program_file);
    CType expected = CHAR;
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    Block *mainBlock = ((FunctionDefinition *) program->GetDefinitions_()[0])->GetBlock_();
    Statement *secondStatement = mainBlock->GetStatements_()[2];
    Expression *expression = (Expression *) secondStatement;
    CType type = expression->GetType_();
    ASSERT_TRUE(type == expected);
    delete program;
}

TEST(intermediate_types, comma_expression) {
    ifstream program_file(TEST_DIR "IntermediateTypes/06_comma_expression.c", ios::in);
    ANTLRInputStream input(program_file);
    CType expected = INT64_T;
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    Block *mainBlock = ((FunctionDefinition *) program->GetDefinitions_()[0])->GetBlock_();
    Statement *secondStatement = mainBlock->GetStatements_()[2];
    Expression *expression = (Expression *) secondStatement;
    CType type = expression->GetType_();
    ASSERT_TRUE(type == expected);
    delete program;
}

TEST(intermediate_types, complex_test) {
    ifstream program_file(TEST_DIR "IntermediateTypes/07_complex_test.c", ios::in);
    ANTLRInputStream input(program_file);
    CType expectedFinal = CHAR;
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();
    MyCProgramVisitor visitor;

    Program *program = visitor.visit(tree).as<Program *>();
    Block *mainBlock = ((FunctionDefinition *) program->GetDefinitions_()[0])->GetBlock_();
    Statement *secondStatement = mainBlock->GetStatements_()[3];
    Expression *expression = (Expression *) secondStatement;
    CType type = expression->GetType_();
    ASSERT_TRUE(type == expectedFinal);

    OpExpression *opExpression = (OpExpression *) expression;
    BinaryOpAssignment *binaryOpAssignment = (BinaryOpAssignment *) expression;
    Expression *rightExpression = binaryOpAssignment->GetExpression_();
    CType typeRight = rightExpression->GetType_();
    ASSERT_TRUE(typeRight == INT64_T);

    BinaryOpExpression *rightOpExpression = (BinaryOpExpression *) rightExpression;
    Expression *minusExpression = rightOpExpression->GetExpression1_();
    CType typeMinus = minusExpression->GetType_();
    ASSERT_TRUE(typeMinus == INT32_T);

    Expression *multiplyExpression = rightOpExpression->GetExpression2_();
    CType typeMultiply = multiplyExpression->GetType_();
    ASSERT_TRUE(typeMultiply == INT64_T);
    delete program;
}