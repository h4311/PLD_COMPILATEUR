//
// Created by burca on 19/03/2018.
//
#include "gtest/gtest.h"
#include "../antlr4/my_c_program_visitor.h"
#include "../antlr4/CprogramLexer.h"
#include "../data_structure/variable_declaration.h"
#include "../data_structure/block.h"
#include "../data_structure/function_definition.h"
#include "../data_structure/program.h"
#include "../data_structure/unary_op_assignment.h"
#include "../data_structure/const.h"

using namespace std;
using namespace antlrcpp;
using namespace antlr4;

TEST(visit_unary_op_assignment, unary_increment_char) {
    ANTLRInputStream input("int64_t testFunction(int32_t a[], char s) {++a[0];}");
    vector<VariableDeclaration *> parameters = {
            new VariableDeclaration(INT32_T, "a_block1", vector<Expression *>(), nullptr),
            new VariableDeclaration(CHAR, "s_block1", nullptr)};
    Statement *expectedStatement = new UnaryOpAssignment(new VariableId("a_block1", new Const(INT32_T, 0)),
                                                         new Operator(PREFIX_INCREMENT));
    Block *expectedFunctionBlock = new Block({expectedStatement});
    FunctionDefinition *testFunction = new FunctionDefinition(INT64_T, "testFunction", parameters);
    testFunction->AddBlock_(expectedFunctionBlock);
    Program expected({testFunction});
    CprogramLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    CprogramParser parser(&tokens);
    tree::ParseTree *tree = parser.program();

    MyCProgramVisitor visitor;
    Program *program = visitor.visit(tree).as<Program *>();
    ASSERT_TRUE(program->Equals(expected));
    delete program;
}
