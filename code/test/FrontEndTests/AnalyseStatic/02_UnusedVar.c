int64_t globalVarUnused;
int64_t globalVarUsed;

void functionUnused(){}

void functionUsed(){}

void main() {
    int64_t localVarUnused;
    int64_t localVarUsed;

    globalVarUsed++;
    localVarUsed++;
    functionUsed();
}
