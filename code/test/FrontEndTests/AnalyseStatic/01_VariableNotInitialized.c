int64_t varGlobalInitialized;
int64_t varGlobalNotInitialized;
void main() {
    varGlobalInitialized=0;
    int64_t varLocalInitializedInDeclaration=0;
    int64_t varLocalInitializedAfterDeclaration;
    varLocalInitializedAfterDeclaration=0;

    int64_t varLocalNotInitialized;

    varGlobalInitialized++;
    varLocalInitializedInDeclaration++;
    varLocalInitializedAfterDeclaration++;

    varGlobalNotInitialized++;
    varLocalNotInitialized++;
}
