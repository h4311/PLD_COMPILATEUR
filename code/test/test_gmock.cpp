//
// Created by Horia on 3/15/2018.
//
#include "gmock/gmock.h"

using namespace std;
using ::testing::Return;

// Destructor and methods that you want to mock must be virtual.
class Someone {
public:
    Someone() = default;

    virtual string DoSomething() { return string(); }

    virtual ~Someone() = default;
};

class Master {
public:
    explicit Master(Someone *someone) : someone_(someone) {}

    string MakeSomeoneDoSomething() { return this->someone_->DoSomething(); }

    ~Master() = default;

private:
    unique_ptr <Someone> someone_;
};

class SomeoneMock : public Someone {
public:
    // In the public: section of the child class, write MOCK_METHODn(); (or MOCK_CONST_METHODn(); if you are mocking a
    // const method), where n is the number of the arguments; if you counted wrong, shame on you, and a compiler
    // error will tell you so.
    // Now comes the fun part: you take the function signature, cut-and-paste the function name as the first argument to
    // the macro, and leave what's left as the second argument (in case you're curious, this is the type of the function).
    MOCK_METHOD0(DoSomething, string()
    );
};

TEST(mock_check, does_something
) {
SomeoneMock *someone = new SomeoneMock();
EXPECT_CALL(*someone, DoSomething()
).
WillOnce(Return(string("I did something.")));

Master master(someone);
string message = master.MakeSomeoneDoSomething();

EXPECT_EQ(message,
"I did something.");
}



