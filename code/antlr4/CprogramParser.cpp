
// Generated from D:/burca/Works/INSA-works/IF/4/PLD_COMPILATEUR/code/antlr4\Cprogram.g4 by ANTLR 4.7


#include "CprogramVisitor.h"

#include "CprogramParser.h"


using namespace antlrcpp;
using namespace antlr4;
using namespace antlr4;

CprogramParser::CprogramParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

CprogramParser::~CprogramParser() {
  delete _interpreter;
}

std::string CprogramParser::getGrammarFileName() const {
  return "Cprogram.g4";
}

const std::vector<std::string>& CprogramParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& CprogramParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- ProgramContext ------------------------------------------------------------------

CprogramParser::ProgramContext::ProgramContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CprogramParser::ProgramContext::EOF() {
  return getToken(CprogramParser::EOF, 0);
}

std::vector<CprogramParser::DefinitionContext *> CprogramParser::ProgramContext::definition() {
  return getRuleContexts<CprogramParser::DefinitionContext>();
}

CprogramParser::DefinitionContext* CprogramParser::ProgramContext::definition(size_t i) {
  return getRuleContext<CprogramParser::DefinitionContext>(i);
}


size_t CprogramParser::ProgramContext::getRuleIndex() const {
  return CprogramParser::RuleProgram;
}

antlrcpp::Any CprogramParser::ProgramContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitProgram(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::ProgramContext* CprogramParser::program() {
  ProgramContext *_localctx = _tracker.createInstance<ProgramContext>(_ctx, getState());
  enterRule(_localctx, 0, CprogramParser::RuleProgram);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(39);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CprogramParser::VOID

    || _la == CprogramParser::TYPE) {
      setState(36);
      definition();
      setState(41);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(42);
    match(CprogramParser::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DefinitionContext ------------------------------------------------------------------

CprogramParser::DefinitionContext::DefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CprogramParser::VariablesDeclarationContext* CprogramParser::DefinitionContext::variablesDeclaration() {
  return getRuleContext<CprogramParser::VariablesDeclarationContext>(0);
}

CprogramParser::FunctionDefinitionContext* CprogramParser::DefinitionContext::functionDefinition() {
  return getRuleContext<CprogramParser::FunctionDefinitionContext>(0);
}


size_t CprogramParser::DefinitionContext::getRuleIndex() const {
  return CprogramParser::RuleDefinition;
}

antlrcpp::Any CprogramParser::DefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitDefinition(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::DefinitionContext* CprogramParser::definition() {
  DefinitionContext *_localctx = _tracker.createInstance<DefinitionContext>(_ctx, getState());
  enterRule(_localctx, 2, CprogramParser::RuleDefinition);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(46);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx)) {
    case 1: {
      setState(44);
      variablesDeclaration();
      break;
    }

    case 2: {
      setState(45);
      functionDefinition();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionDefinitionContext ------------------------------------------------------------------

CprogramParser::FunctionDefinitionContext::FunctionDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CprogramParser::FunctionDefinitionContext::IDENTIFIER() {
  return getToken(CprogramParser::IDENTIFIER, 0);
}

CprogramParser::ArgumentsDefinitionContext* CprogramParser::FunctionDefinitionContext::argumentsDefinition() {
  return getRuleContext<CprogramParser::ArgumentsDefinitionContext>(0);
}

tree::TerminalNode* CprogramParser::FunctionDefinitionContext::TYPE() {
  return getToken(CprogramParser::TYPE, 0);
}

tree::TerminalNode* CprogramParser::FunctionDefinitionContext::VOID() {
  return getToken(CprogramParser::VOID, 0);
}

std::vector<CprogramParser::StatementContext *> CprogramParser::FunctionDefinitionContext::statement() {
  return getRuleContexts<CprogramParser::StatementContext>();
}

CprogramParser::StatementContext* CprogramParser::FunctionDefinitionContext::statement(size_t i) {
  return getRuleContext<CprogramParser::StatementContext>(i);
}


size_t CprogramParser::FunctionDefinitionContext::getRuleIndex() const {
  return CprogramParser::RuleFunctionDefinition;
}

antlrcpp::Any CprogramParser::FunctionDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitFunctionDefinition(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::FunctionDefinitionContext* CprogramParser::functionDefinition() {
  FunctionDefinitionContext *_localctx = _tracker.createInstance<FunctionDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 4, CprogramParser::RuleFunctionDefinition);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(48);
    _la = _input->LA(1);
    if (!(_la == CprogramParser::VOID

    || _la == CprogramParser::TYPE)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
    setState(49);
    match(CprogramParser::IDENTIFIER);
    setState(50);
    match(CprogramParser::T__0);
    setState(51);
    argumentsDefinition();
    setState(52);
    match(CprogramParser::T__1);
    setState(53);
    match(CprogramParser::T__2);
    setState(57);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CprogramParser::T__0)
      | (1ULL << CprogramParser::T__2)
      | (1ULL << CprogramParser::T__9)
      | (1ULL << CprogramParser::T__10)
      | (1ULL << CprogramParser::T__11)
      | (1ULL << CprogramParser::T__12)
      | (1ULL << CprogramParser::T__13)
      | (1ULL << CprogramParser::T__14)
      | (1ULL << CprogramParser::IF)
      | (1ULL << CprogramParser::WHILE)
      | (1ULL << CprogramParser::RETURN)
      | (1ULL << CprogramParser::BREAK)
      | (1ULL << CprogramParser::CONTINUE)
      | (1ULL << CprogramParser::TYPE)
      | (1ULL << CprogramParser::NUM)
      | (1ULL << CprogramParser::IDENTIFIER)
      | (1ULL << CprogramParser::CHAR))) != 0)) {
      setState(54);
      statement();
      setState(59);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(60);
    match(CprogramParser::T__3);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArgumentsDefinitionContext ------------------------------------------------------------------

CprogramParser::ArgumentsDefinitionContext::ArgumentsDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CprogramParser::ArgumentsDefinitionContext::VOID() {
  return getToken(CprogramParser::VOID, 0);
}

std::vector<CprogramParser::ArgumentDefinitionContext *> CprogramParser::ArgumentsDefinitionContext::argumentDefinition() {
  return getRuleContexts<CprogramParser::ArgumentDefinitionContext>();
}

CprogramParser::ArgumentDefinitionContext* CprogramParser::ArgumentsDefinitionContext::argumentDefinition(size_t i) {
  return getRuleContext<CprogramParser::ArgumentDefinitionContext>(i);
}


size_t CprogramParser::ArgumentsDefinitionContext::getRuleIndex() const {
  return CprogramParser::RuleArgumentsDefinition;
}

antlrcpp::Any CprogramParser::ArgumentsDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitArgumentsDefinition(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::ArgumentsDefinitionContext* CprogramParser::argumentsDefinition() {
  ArgumentsDefinitionContext *_localctx = _tracker.createInstance<ArgumentsDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 6, CprogramParser::RuleArgumentsDefinition);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(73);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CprogramParser::T__1:
      case CprogramParser::TYPE: {
        setState(70);
        _errHandler->sync(this);

        _la = _input->LA(1);
        if (_la == CprogramParser::TYPE) {
          setState(62);
          argumentDefinition();
          setState(67);
          _errHandler->sync(this);
          _la = _input->LA(1);
          while (_la == CprogramParser::T__4) {
            setState(63);
            match(CprogramParser::T__4);
            setState(64);
            argumentDefinition();
            setState(69);
            _errHandler->sync(this);
            _la = _input->LA(1);
          }
        }
        break;
      }

      case CprogramParser::VOID: {
        setState(72);
        match(CprogramParser::VOID);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArgumentDefinitionContext ------------------------------------------------------------------

CprogramParser::ArgumentDefinitionContext::ArgumentDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CprogramParser::ArgumentDefinitionContext::getRuleIndex() const {
  return CprogramParser::RuleArgumentDefinition;
}

void CprogramParser::ArgumentDefinitionContext::copyFrom(ArgumentDefinitionContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ArrayArgumentDefinitionContext ------------------------------------------------------------------

tree::TerminalNode* CprogramParser::ArrayArgumentDefinitionContext::TYPE() {
  return getToken(CprogramParser::TYPE, 0);
}

tree::TerminalNode* CprogramParser::ArrayArgumentDefinitionContext::IDENTIFIER() {
  return getToken(CprogramParser::IDENTIFIER, 0);
}

CprogramParser::BinaryOpAssignmentContext* CprogramParser::ArrayArgumentDefinitionContext::binaryOpAssignment() {
  return getRuleContext<CprogramParser::BinaryOpAssignmentContext>(0);
}

CprogramParser::ArrayArgumentDefinitionContext::ArrayArgumentDefinitionContext(ArgumentDefinitionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::ArrayArgumentDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitArrayArgumentDefinition(this);
  else
    return visitor->visitChildren(this);
}
//----------------- SimpleArgumentDefinitionContext ------------------------------------------------------------------

tree::TerminalNode* CprogramParser::SimpleArgumentDefinitionContext::TYPE() {
  return getToken(CprogramParser::TYPE, 0);
}

tree::TerminalNode* CprogramParser::SimpleArgumentDefinitionContext::IDENTIFIER() {
  return getToken(CprogramParser::IDENTIFIER, 0);
}

CprogramParser::SimpleArgumentDefinitionContext::SimpleArgumentDefinitionContext(ArgumentDefinitionContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::SimpleArgumentDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitSimpleArgumentDefinition(this);
  else
    return visitor->visitChildren(this);
}
CprogramParser::ArgumentDefinitionContext* CprogramParser::argumentDefinition() {
  ArgumentDefinitionContext *_localctx = _tracker.createInstance<ArgumentDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 8, CprogramParser::RuleArgumentDefinition);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(84);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 7, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<ArgumentDefinitionContext *>(_tracker.createInstance<CprogramParser::SimpleArgumentDefinitionContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(75);
      match(CprogramParser::TYPE);
      setState(76);
      match(CprogramParser::IDENTIFIER);
      break;
    }

    case 2: {
      _localctx = dynamic_cast<ArgumentDefinitionContext *>(_tracker.createInstance<CprogramParser::ArrayArgumentDefinitionContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(77);
      match(CprogramParser::TYPE);
      setState(78);
      match(CprogramParser::IDENTIFIER);
      setState(79);
      match(CprogramParser::T__5);
      setState(81);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if ((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << CprogramParser::T__0)
        | (1ULL << CprogramParser::T__9)
        | (1ULL << CprogramParser::T__10)
        | (1ULL << CprogramParser::T__11)
        | (1ULL << CprogramParser::T__12)
        | (1ULL << CprogramParser::T__13)
        | (1ULL << CprogramParser::T__14)
        | (1ULL << CprogramParser::NUM)
        | (1ULL << CprogramParser::IDENTIFIER)
        | (1ULL << CprogramParser::CHAR))) != 0)) {
        setState(80);
        binaryOpAssignment();
      }
      setState(83);
      match(CprogramParser::T__6);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariablesDeclarationContext ------------------------------------------------------------------

CprogramParser::VariablesDeclarationContext::VariablesDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CprogramParser::VariablesDeclarationContext::TYPE() {
  return getToken(CprogramParser::TYPE, 0);
}

std::vector<CprogramParser::VariableDeclarationContext *> CprogramParser::VariablesDeclarationContext::variableDeclaration() {
  return getRuleContexts<CprogramParser::VariableDeclarationContext>();
}

CprogramParser::VariableDeclarationContext* CprogramParser::VariablesDeclarationContext::variableDeclaration(size_t i) {
  return getRuleContext<CprogramParser::VariableDeclarationContext>(i);
}


size_t CprogramParser::VariablesDeclarationContext::getRuleIndex() const {
  return CprogramParser::RuleVariablesDeclaration;
}

antlrcpp::Any CprogramParser::VariablesDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitVariablesDeclaration(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::VariablesDeclarationContext* CprogramParser::variablesDeclaration() {
  VariablesDeclarationContext *_localctx = _tracker.createInstance<VariablesDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 10, CprogramParser::RuleVariablesDeclaration);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(86);
    match(CprogramParser::TYPE);
    setState(87);
    variableDeclaration();
    setState(92);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CprogramParser::T__4) {
      setState(88);
      match(CprogramParser::T__4);
      setState(89);
      variableDeclaration();
      setState(94);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(95);
    match(CprogramParser::T__7);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableDeclarationContext ------------------------------------------------------------------

CprogramParser::VariableDeclarationContext::VariableDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CprogramParser::VariableDeclarationContext::getRuleIndex() const {
  return CprogramParser::RuleVariableDeclaration;
}

void CprogramParser::VariableDeclarationContext::copyFrom(VariableDeclarationContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- SimpleVariableDeclarationContext ------------------------------------------------------------------

tree::TerminalNode* CprogramParser::SimpleVariableDeclarationContext::IDENTIFIER() {
  return getToken(CprogramParser::IDENTIFIER, 0);
}

std::vector<CprogramParser::BinaryOpAssignmentContext *> CprogramParser::SimpleVariableDeclarationContext::binaryOpAssignment() {
  return getRuleContexts<CprogramParser::BinaryOpAssignmentContext>();
}

CprogramParser::BinaryOpAssignmentContext* CprogramParser::SimpleVariableDeclarationContext::binaryOpAssignment(size_t i) {
  return getRuleContext<CprogramParser::BinaryOpAssignmentContext>(i);
}

CprogramParser::SimpleVariableDeclarationContext::SimpleVariableDeclarationContext(VariableDeclarationContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::SimpleVariableDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitSimpleVariableDeclaration(this);
  else
    return visitor->visitChildren(this);
}
//----------------- SizedArrayVariableDeclarationContext ------------------------------------------------------------------

tree::TerminalNode* CprogramParser::SizedArrayVariableDeclarationContext::IDENTIFIER() {
  return getToken(CprogramParser::IDENTIFIER, 0);
}

std::vector<CprogramParser::BinaryOpAssignmentContext *> CprogramParser::SizedArrayVariableDeclarationContext::binaryOpAssignment() {
  return getRuleContexts<CprogramParser::BinaryOpAssignmentContext>();
}

CprogramParser::BinaryOpAssignmentContext* CprogramParser::SizedArrayVariableDeclarationContext::binaryOpAssignment(size_t i) {
  return getRuleContext<CprogramParser::BinaryOpAssignmentContext>(i);
}

CprogramParser::SizedArrayVariableDeclarationContext::SizedArrayVariableDeclarationContext(VariableDeclarationContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::SizedArrayVariableDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitSizedArrayVariableDeclaration(this);
  else
    return visitor->visitChildren(this);
}
//----------------- UnsizedArrayVariableDeclarationContext ------------------------------------------------------------------

tree::TerminalNode* CprogramParser::UnsizedArrayVariableDeclarationContext::IDENTIFIER() {
  return getToken(CprogramParser::IDENTIFIER, 0);
}

std::vector<CprogramParser::BinaryOpAssignmentContext *> CprogramParser::UnsizedArrayVariableDeclarationContext::binaryOpAssignment() {
  return getRuleContexts<CprogramParser::BinaryOpAssignmentContext>();
}

CprogramParser::BinaryOpAssignmentContext* CprogramParser::UnsizedArrayVariableDeclarationContext::binaryOpAssignment(size_t i) {
  return getRuleContext<CprogramParser::BinaryOpAssignmentContext>(i);
}

CprogramParser::UnsizedArrayVariableDeclarationContext::UnsizedArrayVariableDeclarationContext(VariableDeclarationContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::UnsizedArrayVariableDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitUnsizedArrayVariableDeclaration(this);
  else
    return visitor->visitChildren(this);
}
CprogramParser::VariableDeclarationContext* CprogramParser::variableDeclaration() {
  VariableDeclarationContext *_localctx = _tracker.createInstance<VariableDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 12, CprogramParser::RuleVariableDeclaration);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(148);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 15, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<VariableDeclarationContext *>(_tracker.createInstance<CprogramParser::SimpleVariableDeclarationContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(97);
      match(CprogramParser::IDENTIFIER);
      setState(113);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == CprogramParser::T__8) {
        setState(98);
        match(CprogramParser::T__8);
        setState(111);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case CprogramParser::T__0:
          case CprogramParser::T__9:
          case CprogramParser::T__10:
          case CprogramParser::T__11:
          case CprogramParser::T__12:
          case CprogramParser::T__13:
          case CprogramParser::T__14:
          case CprogramParser::NUM:
          case CprogramParser::IDENTIFIER:
          case CprogramParser::CHAR: {
            setState(99);
            binaryOpAssignment();
            break;
          }

          case CprogramParser::T__2: {
            setState(100);
            match(CprogramParser::T__2);
            setState(101);
            binaryOpAssignment();
            setState(106);
            _errHandler->sync(this);
            _la = _input->LA(1);
            while (_la == CprogramParser::T__4) {
              setState(102);
              match(CprogramParser::T__4);
              setState(103);
              binaryOpAssignment();
              setState(108);
              _errHandler->sync(this);
              _la = _input->LA(1);
            }
            setState(109);
            match(CprogramParser::T__3);
            break;
          }

        default:
          throw NoViableAltException(this);
        }
      }
      break;
    }

    case 2: {
      _localctx = dynamic_cast<VariableDeclarationContext *>(_tracker.createInstance<CprogramParser::SizedArrayVariableDeclarationContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(115);
      match(CprogramParser::IDENTIFIER);
      setState(116);
      match(CprogramParser::T__5);
      setState(117);
      binaryOpAssignment();
      setState(118);
      match(CprogramParser::T__6);
      setState(131);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == CprogramParser::T__8) {
        setState(119);
        match(CprogramParser::T__8);
        setState(120);
        match(CprogramParser::T__2);
        setState(121);
        binaryOpAssignment();
        setState(126);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while (_la == CprogramParser::T__4) {
          setState(122);
          match(CprogramParser::T__4);
          setState(123);
          binaryOpAssignment();
          setState(128);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(129);
        match(CprogramParser::T__3);
      }
      break;
    }

    case 3: {
      _localctx = dynamic_cast<VariableDeclarationContext *>(_tracker.createInstance<CprogramParser::UnsizedArrayVariableDeclarationContext>(_localctx));
      enterOuterAlt(_localctx, 3);
      setState(133);
      match(CprogramParser::IDENTIFIER);
      setState(134);
      match(CprogramParser::T__5);
      setState(135);
      match(CprogramParser::T__6);
      setState(136);
      match(CprogramParser::T__8);
      setState(137);
      match(CprogramParser::T__2);
      setState(138);
      binaryOpAssignment();
      setState(143);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == CprogramParser::T__4) {
        setState(139);
        match(CprogramParser::T__4);
        setState(140);
        binaryOpAssignment();
        setState(145);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
      setState(146);
      match(CprogramParser::T__3);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableIdContext ------------------------------------------------------------------

CprogramParser::VariableIdContext::VariableIdContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CprogramParser::VariableIdContext::IDENTIFIER() {
  return getToken(CprogramParser::IDENTIFIER, 0);
}

CprogramParser::ExpressionContext* CprogramParser::VariableIdContext::expression() {
  return getRuleContext<CprogramParser::ExpressionContext>(0);
}


size_t CprogramParser::VariableIdContext::getRuleIndex() const {
  return CprogramParser::RuleVariableId;
}

antlrcpp::Any CprogramParser::VariableIdContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitVariableId(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::VariableIdContext* CprogramParser::variableId() {
  VariableIdContext *_localctx = _tracker.createInstance<VariableIdContext>(_ctx, getState());
  enterRule(_localctx, 14, CprogramParser::RuleVariableId);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(156);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 16, _ctx)) {
    case 1: {
      setState(150);
      match(CprogramParser::IDENTIFIER);
      break;
    }

    case 2: {
      setState(151);
      match(CprogramParser::IDENTIFIER);
      setState(152);
      match(CprogramParser::T__5);
      setState(153);
      expression(0);
      setState(154);
      match(CprogramParser::T__6);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementNoDeclarationContext ------------------------------------------------------------------

CprogramParser::StatementNoDeclarationContext::StatementNoDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CprogramParser::StatementNoDeclarationContext::getRuleIndex() const {
  return CprogramParser::RuleStatementNoDeclaration;
}

void CprogramParser::StatementNoDeclarationContext::copyFrom(StatementNoDeclarationContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- WhileStatementContext ------------------------------------------------------------------

tree::TerminalNode* CprogramParser::WhileStatementContext::WHILE() {
  return getToken(CprogramParser::WHILE, 0);
}

CprogramParser::ExpressionContext* CprogramParser::WhileStatementContext::expression() {
  return getRuleContext<CprogramParser::ExpressionContext>(0);
}

CprogramParser::StatementNoDeclarationContext* CprogramParser::WhileStatementContext::statementNoDeclaration() {
  return getRuleContext<CprogramParser::StatementNoDeclarationContext>(0);
}

CprogramParser::WhileStatementContext::WhileStatementContext(StatementNoDeclarationContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::WhileStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitWhileStatement(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AnonymousBlockStatementContext ------------------------------------------------------------------

std::vector<CprogramParser::StatementContext *> CprogramParser::AnonymousBlockStatementContext::statement() {
  return getRuleContexts<CprogramParser::StatementContext>();
}

CprogramParser::StatementContext* CprogramParser::AnonymousBlockStatementContext::statement(size_t i) {
  return getRuleContext<CprogramParser::StatementContext>(i);
}

CprogramParser::AnonymousBlockStatementContext::AnonymousBlockStatementContext(StatementNoDeclarationContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::AnonymousBlockStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitAnonymousBlockStatement(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BreakStatementContext ------------------------------------------------------------------

tree::TerminalNode* CprogramParser::BreakStatementContext::BREAK() {
  return getToken(CprogramParser::BREAK, 0);
}

CprogramParser::BreakStatementContext::BreakStatementContext(StatementNoDeclarationContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::BreakStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitBreakStatement(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExpressionStatementContext ------------------------------------------------------------------

CprogramParser::ExpressionContext* CprogramParser::ExpressionStatementContext::expression() {
  return getRuleContext<CprogramParser::ExpressionContext>(0);
}

CprogramParser::ExpressionStatementContext::ExpressionStatementContext(StatementNoDeclarationContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::ExpressionStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitExpressionStatement(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ContinueStatementContext ------------------------------------------------------------------

tree::TerminalNode* CprogramParser::ContinueStatementContext::CONTINUE() {
  return getToken(CprogramParser::CONTINUE, 0);
}

CprogramParser::ContinueStatementContext::ContinueStatementContext(StatementNoDeclarationContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::ContinueStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitContinueStatement(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IfStatementContext ------------------------------------------------------------------

tree::TerminalNode* CprogramParser::IfStatementContext::IF() {
  return getToken(CprogramParser::IF, 0);
}

CprogramParser::ExpressionContext* CprogramParser::IfStatementContext::expression() {
  return getRuleContext<CprogramParser::ExpressionContext>(0);
}

std::vector<CprogramParser::StatementNoDeclarationContext *> CprogramParser::IfStatementContext::statementNoDeclaration() {
  return getRuleContexts<CprogramParser::StatementNoDeclarationContext>();
}

CprogramParser::StatementNoDeclarationContext* CprogramParser::IfStatementContext::statementNoDeclaration(size_t i) {
  return getRuleContext<CprogramParser::StatementNoDeclarationContext>(i);
}

tree::TerminalNode* CprogramParser::IfStatementContext::ELSE() {
  return getToken(CprogramParser::ELSE, 0);
}

CprogramParser::IfStatementContext::IfStatementContext(StatementNoDeclarationContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::IfStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitIfStatement(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ReturnStatementContext ------------------------------------------------------------------

tree::TerminalNode* CprogramParser::ReturnStatementContext::RETURN() {
  return getToken(CprogramParser::RETURN, 0);
}

CprogramParser::ExpressionContext* CprogramParser::ReturnStatementContext::expression() {
  return getRuleContext<CprogramParser::ExpressionContext>(0);
}

CprogramParser::ReturnStatementContext::ReturnStatementContext(StatementNoDeclarationContext *ctx) { copyFrom(ctx); }

antlrcpp::Any CprogramParser::ReturnStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitReturnStatement(this);
  else
    return visitor->visitChildren(this);
}
CprogramParser::StatementNoDeclarationContext* CprogramParser::statementNoDeclaration() {
  StatementNoDeclarationContext *_localctx = _tracker.createInstance<StatementNoDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 16, CprogramParser::RuleStatementNoDeclaration);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(193);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CprogramParser::WHILE: {
        _localctx = dynamic_cast<StatementNoDeclarationContext *>(_tracker.createInstance<CprogramParser::WhileStatementContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(158);
        match(CprogramParser::WHILE);
        setState(159);
        match(CprogramParser::T__0);
        setState(160);
        expression(0);
        setState(161);
        match(CprogramParser::T__1);
        setState(162);
        statementNoDeclaration();
        break;
      }

      case CprogramParser::IF: {
        _localctx = dynamic_cast<StatementNoDeclarationContext *>(_tracker.createInstance<CprogramParser::IfStatementContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(164);
        match(CprogramParser::IF);
        setState(165);
        match(CprogramParser::T__0);
        setState(166);
        expression(0);
        setState(167);
        match(CprogramParser::T__1);
        setState(168);
        statementNoDeclaration();
        setState(171);
        _errHandler->sync(this);

        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 17, _ctx)) {
        case 1: {
          setState(169);
          match(CprogramParser::ELSE);
          setState(170);
          statementNoDeclaration();
          break;
        }

        }
        break;
      }

      case CprogramParser::T__0:
      case CprogramParser::T__9:
      case CprogramParser::T__10:
      case CprogramParser::T__11:
      case CprogramParser::T__12:
      case CprogramParser::T__13:
      case CprogramParser::T__14:
      case CprogramParser::NUM:
      case CprogramParser::IDENTIFIER:
      case CprogramParser::CHAR: {
        _localctx = dynamic_cast<StatementNoDeclarationContext *>(_tracker.createInstance<CprogramParser::ExpressionStatementContext>(_localctx));
        enterOuterAlt(_localctx, 3);
        setState(173);
        expression(0);
        setState(174);
        match(CprogramParser::T__7);
        break;
      }

      case CprogramParser::RETURN: {
        _localctx = dynamic_cast<StatementNoDeclarationContext *>(_tracker.createInstance<CprogramParser::ReturnStatementContext>(_localctx));
        enterOuterAlt(_localctx, 4);
        setState(176);
        match(CprogramParser::RETURN);
        setState(178);
        _errHandler->sync(this);

        _la = _input->LA(1);
        if ((((_la & ~ 0x3fULL) == 0) &&
          ((1ULL << _la) & ((1ULL << CprogramParser::T__0)
          | (1ULL << CprogramParser::T__9)
          | (1ULL << CprogramParser::T__10)
          | (1ULL << CprogramParser::T__11)
          | (1ULL << CprogramParser::T__12)
          | (1ULL << CprogramParser::T__13)
          | (1ULL << CprogramParser::T__14)
          | (1ULL << CprogramParser::NUM)
          | (1ULL << CprogramParser::IDENTIFIER)
          | (1ULL << CprogramParser::CHAR))) != 0)) {
          setState(177);
          expression(0);
        }
        setState(180);
        match(CprogramParser::T__7);
        break;
      }

      case CprogramParser::BREAK: {
        _localctx = dynamic_cast<StatementNoDeclarationContext *>(_tracker.createInstance<CprogramParser::BreakStatementContext>(_localctx));
        enterOuterAlt(_localctx, 5);
        setState(181);
        match(CprogramParser::BREAK);
        setState(182);
        match(CprogramParser::T__7);
        break;
      }

      case CprogramParser::CONTINUE: {
        _localctx = dynamic_cast<StatementNoDeclarationContext *>(_tracker.createInstance<CprogramParser::ContinueStatementContext>(_localctx));
        enterOuterAlt(_localctx, 6);
        setState(183);
        match(CprogramParser::CONTINUE);
        setState(184);
        match(CprogramParser::T__7);
        break;
      }

      case CprogramParser::T__2: {
        _localctx = dynamic_cast<StatementNoDeclarationContext *>(_tracker.createInstance<CprogramParser::AnonymousBlockStatementContext>(_localctx));
        enterOuterAlt(_localctx, 7);
        setState(185);
        match(CprogramParser::T__2);
        setState(189);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while ((((_la & ~ 0x3fULL) == 0) &&
          ((1ULL << _la) & ((1ULL << CprogramParser::T__0)
          | (1ULL << CprogramParser::T__2)
          | (1ULL << CprogramParser::T__9)
          | (1ULL << CprogramParser::T__10)
          | (1ULL << CprogramParser::T__11)
          | (1ULL << CprogramParser::T__12)
          | (1ULL << CprogramParser::T__13)
          | (1ULL << CprogramParser::T__14)
          | (1ULL << CprogramParser::IF)
          | (1ULL << CprogramParser::WHILE)
          | (1ULL << CprogramParser::RETURN)
          | (1ULL << CprogramParser::BREAK)
          | (1ULL << CprogramParser::CONTINUE)
          | (1ULL << CprogramParser::TYPE)
          | (1ULL << CprogramParser::NUM)
          | (1ULL << CprogramParser::IDENTIFIER)
          | (1ULL << CprogramParser::CHAR))) != 0)) {
          setState(186);
          statement();
          setState(191);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(192);
        match(CprogramParser::T__3);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementContext ------------------------------------------------------------------

CprogramParser::StatementContext::StatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CprogramParser::StatementNoDeclarationContext* CprogramParser::StatementContext::statementNoDeclaration() {
  return getRuleContext<CprogramParser::StatementNoDeclarationContext>(0);
}

CprogramParser::VariablesDeclarationContext* CprogramParser::StatementContext::variablesDeclaration() {
  return getRuleContext<CprogramParser::VariablesDeclarationContext>(0);
}


size_t CprogramParser::StatementContext::getRuleIndex() const {
  return CprogramParser::RuleStatement;
}

antlrcpp::Any CprogramParser::StatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitStatement(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::StatementContext* CprogramParser::statement() {
  StatementContext *_localctx = _tracker.createInstance<StatementContext>(_ctx, getState());
  enterRule(_localctx, 18, CprogramParser::RuleStatement);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(197);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CprogramParser::T__0:
      case CprogramParser::T__2:
      case CprogramParser::T__9:
      case CprogramParser::T__10:
      case CprogramParser::T__11:
      case CprogramParser::T__12:
      case CprogramParser::T__13:
      case CprogramParser::T__14:
      case CprogramParser::IF:
      case CprogramParser::WHILE:
      case CprogramParser::RETURN:
      case CprogramParser::BREAK:
      case CprogramParser::CONTINUE:
      case CprogramParser::NUM:
      case CprogramParser::IDENTIFIER:
      case CprogramParser::CHAR: {
        enterOuterAlt(_localctx, 1);
        setState(195);
        statementNoDeclaration();
        break;
      }

      case CprogramParser::TYPE: {
        enterOuterAlt(_localctx, 2);
        setState(196);
        variablesDeclaration();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NonOpExpressionContext ------------------------------------------------------------------

CprogramParser::NonOpExpressionContext::NonOpExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CprogramParser::FunctionCallContext* CprogramParser::NonOpExpressionContext::functionCall() {
  return getRuleContext<CprogramParser::FunctionCallContext>(0);
}

CprogramParser::VariableIdContext* CprogramParser::NonOpExpressionContext::variableId() {
  return getRuleContext<CprogramParser::VariableIdContext>(0);
}

CprogramParser::ConstantContext* CprogramParser::NonOpExpressionContext::constant() {
  return getRuleContext<CprogramParser::ConstantContext>(0);
}

CprogramParser::ExpressionContext* CprogramParser::NonOpExpressionContext::expression() {
  return getRuleContext<CprogramParser::ExpressionContext>(0);
}


size_t CprogramParser::NonOpExpressionContext::getRuleIndex() const {
  return CprogramParser::RuleNonOpExpression;
}

antlrcpp::Any CprogramParser::NonOpExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitNonOpExpression(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::NonOpExpressionContext* CprogramParser::nonOpExpression() {
  NonOpExpressionContext *_localctx = _tracker.createInstance<NonOpExpressionContext>(_ctx, getState());
  enterRule(_localctx, 20, CprogramParser::RuleNonOpExpression);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(206);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 22, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(199);
      functionCall();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(200);
      variableId();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(201);
      constant();
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(202);
      match(CprogramParser::T__0);
      setState(203);
      expression(0);
      setState(204);
      match(CprogramParser::T__1);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- UnaryOpAssignmentContext ------------------------------------------------------------------

CprogramParser::UnaryOpAssignmentContext::UnaryOpAssignmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CprogramParser::NonOpExpressionContext* CprogramParser::UnaryOpAssignmentContext::nonOpExpression() {
  return getRuleContext<CprogramParser::NonOpExpressionContext>(0);
}

CprogramParser::VariableIdContext* CprogramParser::UnaryOpAssignmentContext::variableId() {
  return getRuleContext<CprogramParser::VariableIdContext>(0);
}


size_t CprogramParser::UnaryOpAssignmentContext::getRuleIndex() const {
  return CprogramParser::RuleUnaryOpAssignment;
}

antlrcpp::Any CprogramParser::UnaryOpAssignmentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitUnaryOpAssignment(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::UnaryOpAssignmentContext* CprogramParser::unaryOpAssignment() {
  UnaryOpAssignmentContext *_localctx = _tracker.createInstance<UnaryOpAssignmentContext>(_ctx, getState());
  enterRule(_localctx, 22, CprogramParser::RuleUnaryOpAssignment);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(214);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 23, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(208);
      nonOpExpression();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(209);
      variableId();
      setState(210);
      dynamic_cast<UnaryOpAssignmentContext *>(_localctx)->op = _input->LT(1);
      _la = _input->LA(1);
      if (!(_la == CprogramParser::T__9

      || _la == CprogramParser::T__10)) {
        dynamic_cast<UnaryOpAssignmentContext *>(_localctx)->op = _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(212);
      dynamic_cast<UnaryOpAssignmentContext *>(_localctx)->op = _input->LT(1);
      _la = _input->LA(1);
      if (!(_la == CprogramParser::T__9

      || _la == CprogramParser::T__10)) {
        dynamic_cast<UnaryOpAssignmentContext *>(_localctx)->op = _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      setState(213);
      variableId();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- UnaryOpExpressionContext ------------------------------------------------------------------

CprogramParser::UnaryOpExpressionContext::UnaryOpExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CprogramParser::UnaryOpAssignmentContext* CprogramParser::UnaryOpExpressionContext::unaryOpAssignment() {
  return getRuleContext<CprogramParser::UnaryOpAssignmentContext>(0);
}

CprogramParser::UnaryOpExpressionContext* CprogramParser::UnaryOpExpressionContext::unaryOpExpression() {
  return getRuleContext<CprogramParser::UnaryOpExpressionContext>(0);
}


size_t CprogramParser::UnaryOpExpressionContext::getRuleIndex() const {
  return CprogramParser::RuleUnaryOpExpression;
}

antlrcpp::Any CprogramParser::UnaryOpExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitUnaryOpExpression(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::UnaryOpExpressionContext* CprogramParser::unaryOpExpression() {
  UnaryOpExpressionContext *_localctx = _tracker.createInstance<UnaryOpExpressionContext>(_ctx, getState());
  enterRule(_localctx, 24, CprogramParser::RuleUnaryOpExpression);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(219);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CprogramParser::T__0:
      case CprogramParser::T__9:
      case CprogramParser::T__10:
      case CprogramParser::NUM:
      case CprogramParser::IDENTIFIER:
      case CprogramParser::CHAR: {
        enterOuterAlt(_localctx, 1);
        setState(216);
        unaryOpAssignment();
        break;
      }

      case CprogramParser::T__11:
      case CprogramParser::T__12:
      case CprogramParser::T__13:
      case CprogramParser::T__14: {
        enterOuterAlt(_localctx, 2);
        setState(217);
        dynamic_cast<UnaryOpExpressionContext *>(_localctx)->op = _input->LT(1);
        _la = _input->LA(1);
        if (!((((_la & ~ 0x3fULL) == 0) &&
          ((1ULL << _la) & ((1ULL << CprogramParser::T__11)
          | (1ULL << CprogramParser::T__12)
          | (1ULL << CprogramParser::T__13)
          | (1ULL << CprogramParser::T__14))) != 0))) {
          dynamic_cast<UnaryOpExpressionContext *>(_localctx)->op = _errHandler->recoverInline(this);
        }
        else {
          _errHandler->reportMatch(this);
          consume();
        }
        setState(218);
        unaryOpExpression();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BinaryOpExpressionContext ------------------------------------------------------------------

CprogramParser::BinaryOpExpressionContext::BinaryOpExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CprogramParser::UnaryOpExpressionContext* CprogramParser::BinaryOpExpressionContext::unaryOpExpression() {
  return getRuleContext<CprogramParser::UnaryOpExpressionContext>(0);
}

std::vector<CprogramParser::BinaryOpExpressionContext *> CprogramParser::BinaryOpExpressionContext::binaryOpExpression() {
  return getRuleContexts<CprogramParser::BinaryOpExpressionContext>();
}

CprogramParser::BinaryOpExpressionContext* CprogramParser::BinaryOpExpressionContext::binaryOpExpression(size_t i) {
  return getRuleContext<CprogramParser::BinaryOpExpressionContext>(i);
}


size_t CprogramParser::BinaryOpExpressionContext::getRuleIndex() const {
  return CprogramParser::RuleBinaryOpExpression;
}

antlrcpp::Any CprogramParser::BinaryOpExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitBinaryOpExpression(this);
  else
    return visitor->visitChildren(this);
}


CprogramParser::BinaryOpExpressionContext* CprogramParser::binaryOpExpression() {
   return binaryOpExpression(0);
}

CprogramParser::BinaryOpExpressionContext* CprogramParser::binaryOpExpression(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  CprogramParser::BinaryOpExpressionContext *_localctx = _tracker.createInstance<BinaryOpExpressionContext>(_ctx, parentState);
  CprogramParser::BinaryOpExpressionContext *previousContext = _localctx;
  size_t startState = 26;
  enterRecursionRule(_localctx, 26, CprogramParser::RuleBinaryOpExpression, precedence);

    size_t _la = 0;

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(222);
    unaryOpExpression();
    _ctx->stop = _input->LT(-1);
    setState(256);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 26, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(254);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 25, _ctx)) {
        case 1: {
          _localctx = _tracker.createInstance<BinaryOpExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleBinaryOpExpression);
          setState(224);

          if (!(precpred(_ctx, 10))) throw FailedPredicateException(this, "precpred(_ctx, 10)");
          setState(225);
          dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!((((_la & ~ 0x3fULL) == 0) &&
            ((1ULL << _la) & ((1ULL << CprogramParser::T__15)
            | (1ULL << CprogramParser::T__16)
            | (1ULL << CprogramParser::T__17))) != 0))) {
            dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(226);
          binaryOpExpression(11);
          break;
        }

        case 2: {
          _localctx = _tracker.createInstance<BinaryOpExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleBinaryOpExpression);
          setState(227);

          if (!(precpred(_ctx, 9))) throw FailedPredicateException(this, "precpred(_ctx, 9)");
          setState(228);
          dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == CprogramParser::T__11

          || _la == CprogramParser::T__12)) {
            dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(229);
          binaryOpExpression(10);
          break;
        }

        case 3: {
          _localctx = _tracker.createInstance<BinaryOpExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleBinaryOpExpression);
          setState(230);

          if (!(precpred(_ctx, 8))) throw FailedPredicateException(this, "precpred(_ctx, 8)");
          setState(231);
          dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == CprogramParser::T__18

          || _la == CprogramParser::T__19)) {
            dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(232);
          binaryOpExpression(9);
          break;
        }

        case 4: {
          _localctx = _tracker.createInstance<BinaryOpExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleBinaryOpExpression);
          setState(233);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(234);
          dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!((((_la & ~ 0x3fULL) == 0) &&
            ((1ULL << _la) & ((1ULL << CprogramParser::T__20)
            | (1ULL << CprogramParser::T__21)
            | (1ULL << CprogramParser::T__22)
            | (1ULL << CprogramParser::T__23))) != 0))) {
            dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(235);
          binaryOpExpression(8);
          break;
        }

        case 5: {
          _localctx = _tracker.createInstance<BinaryOpExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleBinaryOpExpression);
          setState(236);

          if (!(precpred(_ctx, 6))) throw FailedPredicateException(this, "precpred(_ctx, 6)");
          setState(237);
          dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == CprogramParser::T__24

          || _la == CprogramParser::T__25)) {
            dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(238);
          binaryOpExpression(7);
          break;
        }

        case 6: {
          _localctx = _tracker.createInstance<BinaryOpExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleBinaryOpExpression);
          setState(239);

          if (!(precpred(_ctx, 5))) throw FailedPredicateException(this, "precpred(_ctx, 5)");
          setState(240);
          dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = match(CprogramParser::T__26);
          setState(241);
          binaryOpExpression(6);
          break;
        }

        case 7: {
          _localctx = _tracker.createInstance<BinaryOpExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleBinaryOpExpression);
          setState(242);

          if (!(precpred(_ctx, 4))) throw FailedPredicateException(this, "precpred(_ctx, 4)");
          setState(243);
          dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = match(CprogramParser::T__27);
          setState(244);
          binaryOpExpression(5);
          break;
        }

        case 8: {
          _localctx = _tracker.createInstance<BinaryOpExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleBinaryOpExpression);
          setState(245);

          if (!(precpred(_ctx, 3))) throw FailedPredicateException(this, "precpred(_ctx, 3)");
          setState(246);
          dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = match(CprogramParser::T__28);
          setState(247);
          binaryOpExpression(4);
          break;
        }

        case 9: {
          _localctx = _tracker.createInstance<BinaryOpExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleBinaryOpExpression);
          setState(248);

          if (!(precpred(_ctx, 2))) throw FailedPredicateException(this, "precpred(_ctx, 2)");
          setState(249);
          dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = match(CprogramParser::T__29);
          setState(250);
          binaryOpExpression(3);
          break;
        }

        case 10: {
          _localctx = _tracker.createInstance<BinaryOpExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleBinaryOpExpression);
          setState(251);

          if (!(precpred(_ctx, 1))) throw FailedPredicateException(this, "precpred(_ctx, 1)");
          setState(252);
          dynamic_cast<BinaryOpExpressionContext *>(_localctx)->op = match(CprogramParser::T__30);
          setState(253);
          binaryOpExpression(2);
          break;
        }

        } 
      }
      setState(258);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 26, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- BinaryOpAssignmentContext ------------------------------------------------------------------

CprogramParser::BinaryOpAssignmentContext::BinaryOpAssignmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CprogramParser::BinaryOpExpressionContext* CprogramParser::BinaryOpAssignmentContext::binaryOpExpression() {
  return getRuleContext<CprogramParser::BinaryOpExpressionContext>(0);
}

CprogramParser::VariableIdContext* CprogramParser::BinaryOpAssignmentContext::variableId() {
  return getRuleContext<CprogramParser::VariableIdContext>(0);
}

CprogramParser::BinaryOpAssignmentContext* CprogramParser::BinaryOpAssignmentContext::binaryOpAssignment() {
  return getRuleContext<CprogramParser::BinaryOpAssignmentContext>(0);
}


size_t CprogramParser::BinaryOpAssignmentContext::getRuleIndex() const {
  return CprogramParser::RuleBinaryOpAssignment;
}

antlrcpp::Any CprogramParser::BinaryOpAssignmentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitBinaryOpAssignment(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::BinaryOpAssignmentContext* CprogramParser::binaryOpAssignment() {
  BinaryOpAssignmentContext *_localctx = _tracker.createInstance<BinaryOpAssignmentContext>(_ctx, getState());
  enterRule(_localctx, 28, CprogramParser::RuleBinaryOpAssignment);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(264);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 27, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(259);
      binaryOpExpression(0);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(260);
      variableId();
      setState(261);
      dynamic_cast<BinaryOpAssignmentContext *>(_localctx)->op = _input->LT(1);
      _la = _input->LA(1);
      if (!((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << CprogramParser::T__8)
        | (1ULL << CprogramParser::T__31)
        | (1ULL << CprogramParser::T__32)
        | (1ULL << CprogramParser::T__33)
        | (1ULL << CprogramParser::T__34)
        | (1ULL << CprogramParser::T__35)
        | (1ULL << CprogramParser::T__36)
        | (1ULL << CprogramParser::T__37)
        | (1ULL << CprogramParser::T__38)
        | (1ULL << CprogramParser::T__39)
        | (1ULL << CprogramParser::T__40))) != 0))) {
        dynamic_cast<BinaryOpAssignmentContext *>(_localctx)->op = _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      setState(262);
      binaryOpAssignment();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

CprogramParser::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CprogramParser::BinaryOpAssignmentContext* CprogramParser::ExpressionContext::binaryOpAssignment() {
  return getRuleContext<CprogramParser::BinaryOpAssignmentContext>(0);
}

std::vector<CprogramParser::ExpressionContext *> CprogramParser::ExpressionContext::expression() {
  return getRuleContexts<CprogramParser::ExpressionContext>();
}

CprogramParser::ExpressionContext* CprogramParser::ExpressionContext::expression(size_t i) {
  return getRuleContext<CprogramParser::ExpressionContext>(i);
}


size_t CprogramParser::ExpressionContext::getRuleIndex() const {
  return CprogramParser::RuleExpression;
}

antlrcpp::Any CprogramParser::ExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitExpression(this);
  else
    return visitor->visitChildren(this);
}


CprogramParser::ExpressionContext* CprogramParser::expression() {
   return expression(0);
}

CprogramParser::ExpressionContext* CprogramParser::expression(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  CprogramParser::ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, parentState);
  CprogramParser::ExpressionContext *previousContext = _localctx;
  size_t startState = 30;
  enterRecursionRule(_localctx, 30, CprogramParser::RuleExpression, precedence);

    

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(267);
    binaryOpAssignment();
    _ctx->stop = _input->LT(-1);
    setState(274);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 28, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        _localctx = _tracker.createInstance<ExpressionContext>(parentContext, parentState);
        pushNewRecursionContext(_localctx, startState, RuleExpression);
        setState(269);

        if (!(precpred(_ctx, 1))) throw FailedPredicateException(this, "precpred(_ctx, 1)");
        setState(270);
        dynamic_cast<ExpressionContext *>(_localctx)->op = match(CprogramParser::T__4);
        setState(271);
        expression(2); 
      }
      setState(276);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 28, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- ConstantContext ------------------------------------------------------------------

CprogramParser::ConstantContext::ConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CprogramParser::ConstantContext::NUM() {
  return getToken(CprogramParser::NUM, 0);
}

tree::TerminalNode* CprogramParser::ConstantContext::CHAR() {
  return getToken(CprogramParser::CHAR, 0);
}


size_t CprogramParser::ConstantContext::getRuleIndex() const {
  return CprogramParser::RuleConstant;
}

antlrcpp::Any CprogramParser::ConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitConstant(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::ConstantContext* CprogramParser::constant() {
  ConstantContext *_localctx = _tracker.createInstance<ConstantContext>(_ctx, getState());
  enterRule(_localctx, 32, CprogramParser::RuleConstant);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(277);
    _la = _input->LA(1);
    if (!(_la == CprogramParser::NUM

    || _la == CprogramParser::CHAR)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionCallContext ------------------------------------------------------------------

CprogramParser::FunctionCallContext::FunctionCallContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CprogramParser::FunctionCallContext::IDENTIFIER() {
  return getToken(CprogramParser::IDENTIFIER, 0);
}

std::vector<CprogramParser::BinaryOpAssignmentContext *> CprogramParser::FunctionCallContext::binaryOpAssignment() {
  return getRuleContexts<CprogramParser::BinaryOpAssignmentContext>();
}

CprogramParser::BinaryOpAssignmentContext* CprogramParser::FunctionCallContext::binaryOpAssignment(size_t i) {
  return getRuleContext<CprogramParser::BinaryOpAssignmentContext>(i);
}


size_t CprogramParser::FunctionCallContext::getRuleIndex() const {
  return CprogramParser::RuleFunctionCall;
}

antlrcpp::Any CprogramParser::FunctionCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CprogramVisitor*>(visitor))
    return parserVisitor->visitFunctionCall(this);
  else
    return visitor->visitChildren(this);
}

CprogramParser::FunctionCallContext* CprogramParser::functionCall() {
  FunctionCallContext *_localctx = _tracker.createInstance<FunctionCallContext>(_ctx, getState());
  enterRule(_localctx, 34, CprogramParser::RuleFunctionCall);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(279);
    match(CprogramParser::IDENTIFIER);
    setState(280);
    match(CprogramParser::T__0);
    setState(289);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CprogramParser::T__0)
      | (1ULL << CprogramParser::T__9)
      | (1ULL << CprogramParser::T__10)
      | (1ULL << CprogramParser::T__11)
      | (1ULL << CprogramParser::T__12)
      | (1ULL << CprogramParser::T__13)
      | (1ULL << CprogramParser::T__14)
      | (1ULL << CprogramParser::NUM)
      | (1ULL << CprogramParser::IDENTIFIER)
      | (1ULL << CprogramParser::CHAR))) != 0)) {
      setState(281);
      binaryOpAssignment();
      setState(286);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == CprogramParser::T__4) {
        setState(282);
        match(CprogramParser::T__4);
        setState(283);
        binaryOpAssignment();
        setState(288);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
    }
    setState(291);
    match(CprogramParser::T__1);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

bool CprogramParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 13: return binaryOpExpressionSempred(dynamic_cast<BinaryOpExpressionContext *>(context), predicateIndex);
    case 15: return expressionSempred(dynamic_cast<ExpressionContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool CprogramParser::binaryOpExpressionSempred(BinaryOpExpressionContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 10);
    case 1: return precpred(_ctx, 9);
    case 2: return precpred(_ctx, 8);
    case 3: return precpred(_ctx, 7);
    case 4: return precpred(_ctx, 6);
    case 5: return precpred(_ctx, 5);
    case 6: return precpred(_ctx, 4);
    case 7: return precpred(_ctx, 3);
    case 8: return precpred(_ctx, 2);
    case 9: return precpred(_ctx, 1);

  default:
    break;
  }
  return true;
}

bool CprogramParser::expressionSempred(ExpressionContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 10: return precpred(_ctx, 1);

  default:
    break;
  }
  return true;
}

// Static vars and initialization.
std::vector<dfa::DFA> CprogramParser::_decisionToDFA;
atn::PredictionContextCache CprogramParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN CprogramParser::_atn;
std::vector<uint16_t> CprogramParser::_serializedATN;

std::vector<std::string> CprogramParser::_ruleNames = {
  "program", "definition", "functionDefinition", "argumentsDefinition", 
  "argumentDefinition", "variablesDeclaration", "variableDeclaration", "variableId", 
  "statementNoDeclaration", "statement", "nonOpExpression", "unaryOpAssignment", 
  "unaryOpExpression", "binaryOpExpression", "binaryOpAssignment", "expression", 
  "constant", "functionCall"
};

std::vector<std::string> CprogramParser::_literalNames = {
  "", "'('", "')'", "'{'", "'}'", "','", "'['", "']'", "';'", "'='", "'++'", 
  "'--'", "'+'", "'-'", "'!'", "'~'", "'*'", "'/'", "'%'", "'>>'", "'<<'", 
  "'<'", "'<='", "'>'", "'>='", "'=='", "'!='", "'&'", "'^'", "'|'", "'&&'", 
  "'||'", "'+='", "'-='", "'*='", "'/='", "'%='", "'<<='", "'>>='", "'&='", 
  "'^='", "'|='", "'if'", "'else'", "'while'", "'void'", "'return'", "'break'", 
  "'continue'"
};

std::vector<std::string> CprogramParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "IF", "ELSE", "WHILE", "VOID", "RETURN", "BREAK", 
  "CONTINUE", "TYPE", "NUM", "IDENTIFIER", "CHAR", "COMMENT", "LINE_COMMENT", 
  "PREPROCESSOR", "NEWLINE", "WHITESPACE"
};

dfa::Vocabulary CprogramParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> CprogramParser::_tokenNames;

CprogramParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x3b, 0x128, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x3, 0x2, 0x7, 0x2, 0x28, 0xa, 
    0x2, 0xc, 0x2, 0xe, 0x2, 0x2b, 0xb, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x3, 
    0x3, 0x3, 0x5, 0x3, 0x31, 0xa, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x7, 0x4, 0x3a, 0xa, 0x4, 0xc, 0x4, 
    0xe, 0x4, 0x3d, 0xb, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 
    0x5, 0x7, 0x5, 0x44, 0xa, 0x5, 0xc, 0x5, 0xe, 0x5, 0x47, 0xb, 0x5, 0x5, 
    0x5, 0x49, 0xa, 0x5, 0x3, 0x5, 0x5, 0x5, 0x4c, 0xa, 0x5, 0x3, 0x6, 0x3, 
    0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x5, 0x6, 0x54, 0xa, 0x6, 
    0x3, 0x6, 0x5, 0x6, 0x57, 0xa, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 
    0x7, 0x7, 0x7, 0x5d, 0xa, 0x7, 0xc, 0x7, 0xe, 0x7, 0x60, 0xb, 0x7, 0x3, 
    0x7, 0x3, 0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 
    0x8, 0x3, 0x8, 0x7, 0x8, 0x6b, 0xa, 0x8, 0xc, 0x8, 0xe, 0x8, 0x6e, 0xb, 
    0x8, 0x3, 0x8, 0x3, 0x8, 0x5, 0x8, 0x72, 0xa, 0x8, 0x5, 0x8, 0x74, 0xa, 
    0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 
    0x8, 0x3, 0x8, 0x3, 0x8, 0x7, 0x8, 0x7f, 0xa, 0x8, 0xc, 0x8, 0xe, 0x8, 
    0x82, 0xb, 0x8, 0x3, 0x8, 0x3, 0x8, 0x5, 0x8, 0x86, 0xa, 0x8, 0x3, 0x8, 
    0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 
    0x7, 0x8, 0x90, 0xa, 0x8, 0xc, 0x8, 0xe, 0x8, 0x93, 0xb, 0x8, 0x3, 0x8, 
    0x3, 0x8, 0x5, 0x8, 0x97, 0xa, 0x8, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 
    0x9, 0x3, 0x9, 0x3, 0x9, 0x5, 0x9, 0x9f, 0xa, 0x9, 0x3, 0xa, 0x3, 0xa, 
    0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 
    0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x5, 0xa, 0xae, 0xa, 0xa, 0x3, 
    0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x5, 0xa, 0xb5, 0xa, 0xa, 
    0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 
    0x7, 0xa, 0xbe, 0xa, 0xa, 0xc, 0xa, 0xe, 0xa, 0xc1, 0xb, 0xa, 0x3, 0xa, 
    0x5, 0xa, 0xc4, 0xa, 0xa, 0x3, 0xb, 0x3, 0xb, 0x5, 0xb, 0xc8, 0xa, 0xb, 
    0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 
    0x5, 0xc, 0xd1, 0xa, 0xc, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 
    0xd, 0x3, 0xd, 0x5, 0xd, 0xd9, 0xa, 0xd, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 
    0x5, 0xe, 0xde, 0xa, 0xe, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 
    0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 
    0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 
    0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 
    0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 
    0xf, 0x7, 0xf, 0x101, 0xa, 0xf, 0xc, 0xf, 0xe, 0xf, 0x104, 0xb, 0xf, 
    0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x5, 0x10, 0x10b, 
    0xa, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 
    0x11, 0x7, 0x11, 0x113, 0xa, 0x11, 0xc, 0x11, 0xe, 0x11, 0x116, 0xb, 
    0x11, 0x3, 0x12, 0x3, 0x12, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x7, 0x13, 0x11f, 0xa, 0x13, 0xc, 0x13, 0xe, 0x13, 0x122, 
    0xb, 0x13, 0x5, 0x13, 0x124, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
    0x2, 0x4, 0x1c, 0x20, 0x14, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 
    0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 0x2, 0xc, 
    0x4, 0x2, 0x2f, 0x2f, 0x33, 0x33, 0x3, 0x2, 0xc, 0xd, 0x3, 0x2, 0xe, 
    0x11, 0x3, 0x2, 0x12, 0x14, 0x3, 0x2, 0xe, 0xf, 0x3, 0x2, 0x15, 0x16, 
    0x3, 0x2, 0x17, 0x1a, 0x3, 0x2, 0x1b, 0x1c, 0x4, 0x2, 0xb, 0xb, 0x22, 
    0x2b, 0x4, 0x2, 0x34, 0x34, 0x36, 0x36, 0x2, 0x145, 0x2, 0x29, 0x3, 
    0x2, 0x2, 0x2, 0x4, 0x30, 0x3, 0x2, 0x2, 0x2, 0x6, 0x32, 0x3, 0x2, 0x2, 
    0x2, 0x8, 0x4b, 0x3, 0x2, 0x2, 0x2, 0xa, 0x56, 0x3, 0x2, 0x2, 0x2, 0xc, 
    0x58, 0x3, 0x2, 0x2, 0x2, 0xe, 0x96, 0x3, 0x2, 0x2, 0x2, 0x10, 0x9e, 
    0x3, 0x2, 0x2, 0x2, 0x12, 0xc3, 0x3, 0x2, 0x2, 0x2, 0x14, 0xc7, 0x3, 
    0x2, 0x2, 0x2, 0x16, 0xd0, 0x3, 0x2, 0x2, 0x2, 0x18, 0xd8, 0x3, 0x2, 
    0x2, 0x2, 0x1a, 0xdd, 0x3, 0x2, 0x2, 0x2, 0x1c, 0xdf, 0x3, 0x2, 0x2, 
    0x2, 0x1e, 0x10a, 0x3, 0x2, 0x2, 0x2, 0x20, 0x10c, 0x3, 0x2, 0x2, 0x2, 
    0x22, 0x117, 0x3, 0x2, 0x2, 0x2, 0x24, 0x119, 0x3, 0x2, 0x2, 0x2, 0x26, 
    0x28, 0x5, 0x4, 0x3, 0x2, 0x27, 0x26, 0x3, 0x2, 0x2, 0x2, 0x28, 0x2b, 
    0x3, 0x2, 0x2, 0x2, 0x29, 0x27, 0x3, 0x2, 0x2, 0x2, 0x29, 0x2a, 0x3, 
    0x2, 0x2, 0x2, 0x2a, 0x2c, 0x3, 0x2, 0x2, 0x2, 0x2b, 0x29, 0x3, 0x2, 
    0x2, 0x2, 0x2c, 0x2d, 0x7, 0x2, 0x2, 0x3, 0x2d, 0x3, 0x3, 0x2, 0x2, 
    0x2, 0x2e, 0x31, 0x5, 0xc, 0x7, 0x2, 0x2f, 0x31, 0x5, 0x6, 0x4, 0x2, 
    0x30, 0x2e, 0x3, 0x2, 0x2, 0x2, 0x30, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x31, 
    0x5, 0x3, 0x2, 0x2, 0x2, 0x32, 0x33, 0x9, 0x2, 0x2, 0x2, 0x33, 0x34, 
    0x7, 0x35, 0x2, 0x2, 0x34, 0x35, 0x7, 0x3, 0x2, 0x2, 0x35, 0x36, 0x5, 
    0x8, 0x5, 0x2, 0x36, 0x37, 0x7, 0x4, 0x2, 0x2, 0x37, 0x3b, 0x7, 0x5, 
    0x2, 0x2, 0x38, 0x3a, 0x5, 0x14, 0xb, 0x2, 0x39, 0x38, 0x3, 0x2, 0x2, 
    0x2, 0x3a, 0x3d, 0x3, 0x2, 0x2, 0x2, 0x3b, 0x39, 0x3, 0x2, 0x2, 0x2, 
    0x3b, 0x3c, 0x3, 0x2, 0x2, 0x2, 0x3c, 0x3e, 0x3, 0x2, 0x2, 0x2, 0x3d, 
    0x3b, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x3f, 0x7, 0x6, 0x2, 0x2, 0x3f, 0x7, 
    0x3, 0x2, 0x2, 0x2, 0x40, 0x45, 0x5, 0xa, 0x6, 0x2, 0x41, 0x42, 0x7, 
    0x7, 0x2, 0x2, 0x42, 0x44, 0x5, 0xa, 0x6, 0x2, 0x43, 0x41, 0x3, 0x2, 
    0x2, 0x2, 0x44, 0x47, 0x3, 0x2, 0x2, 0x2, 0x45, 0x43, 0x3, 0x2, 0x2, 
    0x2, 0x45, 0x46, 0x3, 0x2, 0x2, 0x2, 0x46, 0x49, 0x3, 0x2, 0x2, 0x2, 
    0x47, 0x45, 0x3, 0x2, 0x2, 0x2, 0x48, 0x40, 0x3, 0x2, 0x2, 0x2, 0x48, 
    0x49, 0x3, 0x2, 0x2, 0x2, 0x49, 0x4c, 0x3, 0x2, 0x2, 0x2, 0x4a, 0x4c, 
    0x7, 0x2f, 0x2, 0x2, 0x4b, 0x48, 0x3, 0x2, 0x2, 0x2, 0x4b, 0x4a, 0x3, 
    0x2, 0x2, 0x2, 0x4c, 0x9, 0x3, 0x2, 0x2, 0x2, 0x4d, 0x4e, 0x7, 0x33, 
    0x2, 0x2, 0x4e, 0x57, 0x7, 0x35, 0x2, 0x2, 0x4f, 0x50, 0x7, 0x33, 0x2, 
    0x2, 0x50, 0x51, 0x7, 0x35, 0x2, 0x2, 0x51, 0x53, 0x7, 0x8, 0x2, 0x2, 
    0x52, 0x54, 0x5, 0x1e, 0x10, 0x2, 0x53, 0x52, 0x3, 0x2, 0x2, 0x2, 0x53, 
    0x54, 0x3, 0x2, 0x2, 0x2, 0x54, 0x55, 0x3, 0x2, 0x2, 0x2, 0x55, 0x57, 
    0x7, 0x9, 0x2, 0x2, 0x56, 0x4d, 0x3, 0x2, 0x2, 0x2, 0x56, 0x4f, 0x3, 
    0x2, 0x2, 0x2, 0x57, 0xb, 0x3, 0x2, 0x2, 0x2, 0x58, 0x59, 0x7, 0x33, 
    0x2, 0x2, 0x59, 0x5e, 0x5, 0xe, 0x8, 0x2, 0x5a, 0x5b, 0x7, 0x7, 0x2, 
    0x2, 0x5b, 0x5d, 0x5, 0xe, 0x8, 0x2, 0x5c, 0x5a, 0x3, 0x2, 0x2, 0x2, 
    0x5d, 0x60, 0x3, 0x2, 0x2, 0x2, 0x5e, 0x5c, 0x3, 0x2, 0x2, 0x2, 0x5e, 
    0x5f, 0x3, 0x2, 0x2, 0x2, 0x5f, 0x61, 0x3, 0x2, 0x2, 0x2, 0x60, 0x5e, 
    0x3, 0x2, 0x2, 0x2, 0x61, 0x62, 0x7, 0xa, 0x2, 0x2, 0x62, 0xd, 0x3, 
    0x2, 0x2, 0x2, 0x63, 0x73, 0x7, 0x35, 0x2, 0x2, 0x64, 0x71, 0x7, 0xb, 
    0x2, 0x2, 0x65, 0x72, 0x5, 0x1e, 0x10, 0x2, 0x66, 0x67, 0x7, 0x5, 0x2, 
    0x2, 0x67, 0x6c, 0x5, 0x1e, 0x10, 0x2, 0x68, 0x69, 0x7, 0x7, 0x2, 0x2, 
    0x69, 0x6b, 0x5, 0x1e, 0x10, 0x2, 0x6a, 0x68, 0x3, 0x2, 0x2, 0x2, 0x6b, 
    0x6e, 0x3, 0x2, 0x2, 0x2, 0x6c, 0x6a, 0x3, 0x2, 0x2, 0x2, 0x6c, 0x6d, 
    0x3, 0x2, 0x2, 0x2, 0x6d, 0x6f, 0x3, 0x2, 0x2, 0x2, 0x6e, 0x6c, 0x3, 
    0x2, 0x2, 0x2, 0x6f, 0x70, 0x7, 0x6, 0x2, 0x2, 0x70, 0x72, 0x3, 0x2, 
    0x2, 0x2, 0x71, 0x65, 0x3, 0x2, 0x2, 0x2, 0x71, 0x66, 0x3, 0x2, 0x2, 
    0x2, 0x72, 0x74, 0x3, 0x2, 0x2, 0x2, 0x73, 0x64, 0x3, 0x2, 0x2, 0x2, 
    0x73, 0x74, 0x3, 0x2, 0x2, 0x2, 0x74, 0x97, 0x3, 0x2, 0x2, 0x2, 0x75, 
    0x76, 0x7, 0x35, 0x2, 0x2, 0x76, 0x77, 0x7, 0x8, 0x2, 0x2, 0x77, 0x78, 
    0x5, 0x1e, 0x10, 0x2, 0x78, 0x85, 0x7, 0x9, 0x2, 0x2, 0x79, 0x7a, 0x7, 
    0xb, 0x2, 0x2, 0x7a, 0x7b, 0x7, 0x5, 0x2, 0x2, 0x7b, 0x80, 0x5, 0x1e, 
    0x10, 0x2, 0x7c, 0x7d, 0x7, 0x7, 0x2, 0x2, 0x7d, 0x7f, 0x5, 0x1e, 0x10, 
    0x2, 0x7e, 0x7c, 0x3, 0x2, 0x2, 0x2, 0x7f, 0x82, 0x3, 0x2, 0x2, 0x2, 
    0x80, 0x7e, 0x3, 0x2, 0x2, 0x2, 0x80, 0x81, 0x3, 0x2, 0x2, 0x2, 0x81, 
    0x83, 0x3, 0x2, 0x2, 0x2, 0x82, 0x80, 0x3, 0x2, 0x2, 0x2, 0x83, 0x84, 
    0x7, 0x6, 0x2, 0x2, 0x84, 0x86, 0x3, 0x2, 0x2, 0x2, 0x85, 0x79, 0x3, 
    0x2, 0x2, 0x2, 0x85, 0x86, 0x3, 0x2, 0x2, 0x2, 0x86, 0x97, 0x3, 0x2, 
    0x2, 0x2, 0x87, 0x88, 0x7, 0x35, 0x2, 0x2, 0x88, 0x89, 0x7, 0x8, 0x2, 
    0x2, 0x89, 0x8a, 0x7, 0x9, 0x2, 0x2, 0x8a, 0x8b, 0x7, 0xb, 0x2, 0x2, 
    0x8b, 0x8c, 0x7, 0x5, 0x2, 0x2, 0x8c, 0x91, 0x5, 0x1e, 0x10, 0x2, 0x8d, 
    0x8e, 0x7, 0x7, 0x2, 0x2, 0x8e, 0x90, 0x5, 0x1e, 0x10, 0x2, 0x8f, 0x8d, 
    0x3, 0x2, 0x2, 0x2, 0x90, 0x93, 0x3, 0x2, 0x2, 0x2, 0x91, 0x8f, 0x3, 
    0x2, 0x2, 0x2, 0x91, 0x92, 0x3, 0x2, 0x2, 0x2, 0x92, 0x94, 0x3, 0x2, 
    0x2, 0x2, 0x93, 0x91, 0x3, 0x2, 0x2, 0x2, 0x94, 0x95, 0x7, 0x6, 0x2, 
    0x2, 0x95, 0x97, 0x3, 0x2, 0x2, 0x2, 0x96, 0x63, 0x3, 0x2, 0x2, 0x2, 
    0x96, 0x75, 0x3, 0x2, 0x2, 0x2, 0x96, 0x87, 0x3, 0x2, 0x2, 0x2, 0x97, 
    0xf, 0x3, 0x2, 0x2, 0x2, 0x98, 0x9f, 0x7, 0x35, 0x2, 0x2, 0x99, 0x9a, 
    0x7, 0x35, 0x2, 0x2, 0x9a, 0x9b, 0x7, 0x8, 0x2, 0x2, 0x9b, 0x9c, 0x5, 
    0x20, 0x11, 0x2, 0x9c, 0x9d, 0x7, 0x9, 0x2, 0x2, 0x9d, 0x9f, 0x3, 0x2, 
    0x2, 0x2, 0x9e, 0x98, 0x3, 0x2, 0x2, 0x2, 0x9e, 0x99, 0x3, 0x2, 0x2, 
    0x2, 0x9f, 0x11, 0x3, 0x2, 0x2, 0x2, 0xa0, 0xa1, 0x7, 0x2e, 0x2, 0x2, 
    0xa1, 0xa2, 0x7, 0x3, 0x2, 0x2, 0xa2, 0xa3, 0x5, 0x20, 0x11, 0x2, 0xa3, 
    0xa4, 0x7, 0x4, 0x2, 0x2, 0xa4, 0xa5, 0x5, 0x12, 0xa, 0x2, 0xa5, 0xc4, 
    0x3, 0x2, 0x2, 0x2, 0xa6, 0xa7, 0x7, 0x2c, 0x2, 0x2, 0xa7, 0xa8, 0x7, 
    0x3, 0x2, 0x2, 0xa8, 0xa9, 0x5, 0x20, 0x11, 0x2, 0xa9, 0xaa, 0x7, 0x4, 
    0x2, 0x2, 0xaa, 0xad, 0x5, 0x12, 0xa, 0x2, 0xab, 0xac, 0x7, 0x2d, 0x2, 
    0x2, 0xac, 0xae, 0x5, 0x12, 0xa, 0x2, 0xad, 0xab, 0x3, 0x2, 0x2, 0x2, 
    0xad, 0xae, 0x3, 0x2, 0x2, 0x2, 0xae, 0xc4, 0x3, 0x2, 0x2, 0x2, 0xaf, 
    0xb0, 0x5, 0x20, 0x11, 0x2, 0xb0, 0xb1, 0x7, 0xa, 0x2, 0x2, 0xb1, 0xc4, 
    0x3, 0x2, 0x2, 0x2, 0xb2, 0xb4, 0x7, 0x30, 0x2, 0x2, 0xb3, 0xb5, 0x5, 
    0x20, 0x11, 0x2, 0xb4, 0xb3, 0x3, 0x2, 0x2, 0x2, 0xb4, 0xb5, 0x3, 0x2, 
    0x2, 0x2, 0xb5, 0xb6, 0x3, 0x2, 0x2, 0x2, 0xb6, 0xc4, 0x7, 0xa, 0x2, 
    0x2, 0xb7, 0xb8, 0x7, 0x31, 0x2, 0x2, 0xb8, 0xc4, 0x7, 0xa, 0x2, 0x2, 
    0xb9, 0xba, 0x7, 0x32, 0x2, 0x2, 0xba, 0xc4, 0x7, 0xa, 0x2, 0x2, 0xbb, 
    0xbf, 0x7, 0x5, 0x2, 0x2, 0xbc, 0xbe, 0x5, 0x14, 0xb, 0x2, 0xbd, 0xbc, 
    0x3, 0x2, 0x2, 0x2, 0xbe, 0xc1, 0x3, 0x2, 0x2, 0x2, 0xbf, 0xbd, 0x3, 
    0x2, 0x2, 0x2, 0xbf, 0xc0, 0x3, 0x2, 0x2, 0x2, 0xc0, 0xc2, 0x3, 0x2, 
    0x2, 0x2, 0xc1, 0xbf, 0x3, 0x2, 0x2, 0x2, 0xc2, 0xc4, 0x7, 0x6, 0x2, 
    0x2, 0xc3, 0xa0, 0x3, 0x2, 0x2, 0x2, 0xc3, 0xa6, 0x3, 0x2, 0x2, 0x2, 
    0xc3, 0xaf, 0x3, 0x2, 0x2, 0x2, 0xc3, 0xb2, 0x3, 0x2, 0x2, 0x2, 0xc3, 
    0xb7, 0x3, 0x2, 0x2, 0x2, 0xc3, 0xb9, 0x3, 0x2, 0x2, 0x2, 0xc3, 0xbb, 
    0x3, 0x2, 0x2, 0x2, 0xc4, 0x13, 0x3, 0x2, 0x2, 0x2, 0xc5, 0xc8, 0x5, 
    0x12, 0xa, 0x2, 0xc6, 0xc8, 0x5, 0xc, 0x7, 0x2, 0xc7, 0xc5, 0x3, 0x2, 
    0x2, 0x2, 0xc7, 0xc6, 0x3, 0x2, 0x2, 0x2, 0xc8, 0x15, 0x3, 0x2, 0x2, 
    0x2, 0xc9, 0xd1, 0x5, 0x24, 0x13, 0x2, 0xca, 0xd1, 0x5, 0x10, 0x9, 0x2, 
    0xcb, 0xd1, 0x5, 0x22, 0x12, 0x2, 0xcc, 0xcd, 0x7, 0x3, 0x2, 0x2, 0xcd, 
    0xce, 0x5, 0x20, 0x11, 0x2, 0xce, 0xcf, 0x7, 0x4, 0x2, 0x2, 0xcf, 0xd1, 
    0x3, 0x2, 0x2, 0x2, 0xd0, 0xc9, 0x3, 0x2, 0x2, 0x2, 0xd0, 0xca, 0x3, 
    0x2, 0x2, 0x2, 0xd0, 0xcb, 0x3, 0x2, 0x2, 0x2, 0xd0, 0xcc, 0x3, 0x2, 
    0x2, 0x2, 0xd1, 0x17, 0x3, 0x2, 0x2, 0x2, 0xd2, 0xd9, 0x5, 0x16, 0xc, 
    0x2, 0xd3, 0xd4, 0x5, 0x10, 0x9, 0x2, 0xd4, 0xd5, 0x9, 0x3, 0x2, 0x2, 
    0xd5, 0xd9, 0x3, 0x2, 0x2, 0x2, 0xd6, 0xd7, 0x9, 0x3, 0x2, 0x2, 0xd7, 
    0xd9, 0x5, 0x10, 0x9, 0x2, 0xd8, 0xd2, 0x3, 0x2, 0x2, 0x2, 0xd8, 0xd3, 
    0x3, 0x2, 0x2, 0x2, 0xd8, 0xd6, 0x3, 0x2, 0x2, 0x2, 0xd9, 0x19, 0x3, 
    0x2, 0x2, 0x2, 0xda, 0xde, 0x5, 0x18, 0xd, 0x2, 0xdb, 0xdc, 0x9, 0x4, 
    0x2, 0x2, 0xdc, 0xde, 0x5, 0x1a, 0xe, 0x2, 0xdd, 0xda, 0x3, 0x2, 0x2, 
    0x2, 0xdd, 0xdb, 0x3, 0x2, 0x2, 0x2, 0xde, 0x1b, 0x3, 0x2, 0x2, 0x2, 
    0xdf, 0xe0, 0x8, 0xf, 0x1, 0x2, 0xe0, 0xe1, 0x5, 0x1a, 0xe, 0x2, 0xe1, 
    0x102, 0x3, 0x2, 0x2, 0x2, 0xe2, 0xe3, 0xc, 0xc, 0x2, 0x2, 0xe3, 0xe4, 
    0x9, 0x5, 0x2, 0x2, 0xe4, 0x101, 0x5, 0x1c, 0xf, 0xd, 0xe5, 0xe6, 0xc, 
    0xb, 0x2, 0x2, 0xe6, 0xe7, 0x9, 0x6, 0x2, 0x2, 0xe7, 0x101, 0x5, 0x1c, 
    0xf, 0xc, 0xe8, 0xe9, 0xc, 0xa, 0x2, 0x2, 0xe9, 0xea, 0x9, 0x7, 0x2, 
    0x2, 0xea, 0x101, 0x5, 0x1c, 0xf, 0xb, 0xeb, 0xec, 0xc, 0x9, 0x2, 0x2, 
    0xec, 0xed, 0x9, 0x8, 0x2, 0x2, 0xed, 0x101, 0x5, 0x1c, 0xf, 0xa, 0xee, 
    0xef, 0xc, 0x8, 0x2, 0x2, 0xef, 0xf0, 0x9, 0x9, 0x2, 0x2, 0xf0, 0x101, 
    0x5, 0x1c, 0xf, 0x9, 0xf1, 0xf2, 0xc, 0x7, 0x2, 0x2, 0xf2, 0xf3, 0x7, 
    0x1d, 0x2, 0x2, 0xf3, 0x101, 0x5, 0x1c, 0xf, 0x8, 0xf4, 0xf5, 0xc, 0x6, 
    0x2, 0x2, 0xf5, 0xf6, 0x7, 0x1e, 0x2, 0x2, 0xf6, 0x101, 0x5, 0x1c, 0xf, 
    0x7, 0xf7, 0xf8, 0xc, 0x5, 0x2, 0x2, 0xf8, 0xf9, 0x7, 0x1f, 0x2, 0x2, 
    0xf9, 0x101, 0x5, 0x1c, 0xf, 0x6, 0xfa, 0xfb, 0xc, 0x4, 0x2, 0x2, 0xfb, 
    0xfc, 0x7, 0x20, 0x2, 0x2, 0xfc, 0x101, 0x5, 0x1c, 0xf, 0x5, 0xfd, 0xfe, 
    0xc, 0x3, 0x2, 0x2, 0xfe, 0xff, 0x7, 0x21, 0x2, 0x2, 0xff, 0x101, 0x5, 
    0x1c, 0xf, 0x4, 0x100, 0xe2, 0x3, 0x2, 0x2, 0x2, 0x100, 0xe5, 0x3, 0x2, 
    0x2, 0x2, 0x100, 0xe8, 0x3, 0x2, 0x2, 0x2, 0x100, 0xeb, 0x3, 0x2, 0x2, 
    0x2, 0x100, 0xee, 0x3, 0x2, 0x2, 0x2, 0x100, 0xf1, 0x3, 0x2, 0x2, 0x2, 
    0x100, 0xf4, 0x3, 0x2, 0x2, 0x2, 0x100, 0xf7, 0x3, 0x2, 0x2, 0x2, 0x100, 
    0xfa, 0x3, 0x2, 0x2, 0x2, 0x100, 0xfd, 0x3, 0x2, 0x2, 0x2, 0x101, 0x104, 
    0x3, 0x2, 0x2, 0x2, 0x102, 0x100, 0x3, 0x2, 0x2, 0x2, 0x102, 0x103, 
    0x3, 0x2, 0x2, 0x2, 0x103, 0x1d, 0x3, 0x2, 0x2, 0x2, 0x104, 0x102, 0x3, 
    0x2, 0x2, 0x2, 0x105, 0x10b, 0x5, 0x1c, 0xf, 0x2, 0x106, 0x107, 0x5, 
    0x10, 0x9, 0x2, 0x107, 0x108, 0x9, 0xa, 0x2, 0x2, 0x108, 0x109, 0x5, 
    0x1e, 0x10, 0x2, 0x109, 0x10b, 0x3, 0x2, 0x2, 0x2, 0x10a, 0x105, 0x3, 
    0x2, 0x2, 0x2, 0x10a, 0x106, 0x3, 0x2, 0x2, 0x2, 0x10b, 0x1f, 0x3, 0x2, 
    0x2, 0x2, 0x10c, 0x10d, 0x8, 0x11, 0x1, 0x2, 0x10d, 0x10e, 0x5, 0x1e, 
    0x10, 0x2, 0x10e, 0x114, 0x3, 0x2, 0x2, 0x2, 0x10f, 0x110, 0xc, 0x3, 
    0x2, 0x2, 0x110, 0x111, 0x7, 0x7, 0x2, 0x2, 0x111, 0x113, 0x5, 0x20, 
    0x11, 0x4, 0x112, 0x10f, 0x3, 0x2, 0x2, 0x2, 0x113, 0x116, 0x3, 0x2, 
    0x2, 0x2, 0x114, 0x112, 0x3, 0x2, 0x2, 0x2, 0x114, 0x115, 0x3, 0x2, 
    0x2, 0x2, 0x115, 0x21, 0x3, 0x2, 0x2, 0x2, 0x116, 0x114, 0x3, 0x2, 0x2, 
    0x2, 0x117, 0x118, 0x9, 0xb, 0x2, 0x2, 0x118, 0x23, 0x3, 0x2, 0x2, 0x2, 
    0x119, 0x11a, 0x7, 0x35, 0x2, 0x2, 0x11a, 0x123, 0x7, 0x3, 0x2, 0x2, 
    0x11b, 0x120, 0x5, 0x1e, 0x10, 0x2, 0x11c, 0x11d, 0x7, 0x7, 0x2, 0x2, 
    0x11d, 0x11f, 0x5, 0x1e, 0x10, 0x2, 0x11e, 0x11c, 0x3, 0x2, 0x2, 0x2, 
    0x11f, 0x122, 0x3, 0x2, 0x2, 0x2, 0x120, 0x11e, 0x3, 0x2, 0x2, 0x2, 
    0x120, 0x121, 0x3, 0x2, 0x2, 0x2, 0x121, 0x124, 0x3, 0x2, 0x2, 0x2, 
    0x122, 0x120, 0x3, 0x2, 0x2, 0x2, 0x123, 0x11b, 0x3, 0x2, 0x2, 0x2, 
    0x123, 0x124, 0x3, 0x2, 0x2, 0x2, 0x124, 0x125, 0x3, 0x2, 0x2, 0x2, 
    0x125, 0x126, 0x7, 0x4, 0x2, 0x2, 0x126, 0x25, 0x3, 0x2, 0x2, 0x2, 0x21, 
    0x29, 0x30, 0x3b, 0x45, 0x48, 0x4b, 0x53, 0x56, 0x5e, 0x6c, 0x71, 0x73, 
    0x80, 0x85, 0x91, 0x96, 0x9e, 0xad, 0xb4, 0xbf, 0xc3, 0xc7, 0xd0, 0xd8, 
    0xdd, 0x100, 0x102, 0x10a, 0x114, 0x120, 0x123, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

CprogramParser::Initializer CprogramParser::_init;
