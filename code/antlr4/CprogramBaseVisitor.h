
// Generated from D:/burca/Works/INSA-works/IF/4/PLD_COMPILATEUR/code/antlr4\Cprogram.g4 by ANTLR 4.7

#pragma once


#include "antlr4-runtime.h"
#include "CprogramVisitor.h"


namespace antlr4 {

/**
 * This class provides an empty implementation of CprogramVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  CprogramBaseVisitor : public CprogramVisitor {
public:

  virtual antlrcpp::Any visitProgram(CprogramParser::ProgramContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDefinition(CprogramParser::DefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunctionDefinition(CprogramParser::FunctionDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitArgumentsDefinition(CprogramParser::ArgumentsDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSimpleArgumentDefinition(CprogramParser::SimpleArgumentDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitArrayArgumentDefinition(CprogramParser::ArrayArgumentDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariablesDeclaration(CprogramParser::VariablesDeclarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSimpleVariableDeclaration(CprogramParser::SimpleVariableDeclarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSizedArrayVariableDeclaration(CprogramParser::SizedArrayVariableDeclarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitUnsizedArrayVariableDeclaration(CprogramParser::UnsizedArrayVariableDeclarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariableId(CprogramParser::VariableIdContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitWhileStatement(CprogramParser::WhileStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIfStatement(CprogramParser::IfStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExpressionStatement(CprogramParser::ExpressionStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitReturnStatement(CprogramParser::ReturnStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBreakStatement(CprogramParser::BreakStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitContinueStatement(CprogramParser::ContinueStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAnonymousBlockStatement(CprogramParser::AnonymousBlockStatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStatement(CprogramParser::StatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNonOpExpression(CprogramParser::NonOpExpressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitUnaryOpAssignment(CprogramParser::UnaryOpAssignmentContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitUnaryOpExpression(CprogramParser::UnaryOpExpressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinaryOpExpression(CprogramParser::BinaryOpExpressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinaryOpAssignment(CprogramParser::BinaryOpAssignmentContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExpression(CprogramParser::ExpressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitConstant(CprogramParser::ConstantContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunctionCall(CprogramParser::FunctionCallContext *ctx) override {
    return visitChildren(ctx);
  }


};

}  // namespace antlr4
