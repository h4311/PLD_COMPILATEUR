
// Generated from D:/burca/Works/INSA-works/IF/4/PLD_COMPILATEUR/code/antlr4\Cprogram.g4 by ANTLR 4.7

#pragma once


#include "antlr4-runtime.h"
#include "CprogramParser.h"


namespace antlr4 {

/**
 * This class defines an abstract visitor for a parse tree
 * produced by CprogramParser.
 */
class  CprogramVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by CprogramParser.
   */
    virtual antlrcpp::Any visitProgram(CprogramParser::ProgramContext *context) = 0;

    virtual antlrcpp::Any visitDefinition(CprogramParser::DefinitionContext *context) = 0;

    virtual antlrcpp::Any visitFunctionDefinition(CprogramParser::FunctionDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitArgumentsDefinition(CprogramParser::ArgumentsDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitSimpleArgumentDefinition(CprogramParser::SimpleArgumentDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitArrayArgumentDefinition(CprogramParser::ArrayArgumentDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitVariablesDeclaration(CprogramParser::VariablesDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitSimpleVariableDeclaration(CprogramParser::SimpleVariableDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitSizedArrayVariableDeclaration(CprogramParser::SizedArrayVariableDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitUnsizedArrayVariableDeclaration(CprogramParser::UnsizedArrayVariableDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitVariableId(CprogramParser::VariableIdContext *context) = 0;

    virtual antlrcpp::Any visitWhileStatement(CprogramParser::WhileStatementContext *context) = 0;

    virtual antlrcpp::Any visitIfStatement(CprogramParser::IfStatementContext *context) = 0;

    virtual antlrcpp::Any visitExpressionStatement(CprogramParser::ExpressionStatementContext *context) = 0;

    virtual antlrcpp::Any visitReturnStatement(CprogramParser::ReturnStatementContext *context) = 0;

    virtual antlrcpp::Any visitBreakStatement(CprogramParser::BreakStatementContext *context) = 0;

    virtual antlrcpp::Any visitContinueStatement(CprogramParser::ContinueStatementContext *context) = 0;

    virtual antlrcpp::Any visitAnonymousBlockStatement(CprogramParser::AnonymousBlockStatementContext *context) = 0;

    virtual antlrcpp::Any visitStatement(CprogramParser::StatementContext *context) = 0;

    virtual antlrcpp::Any visitNonOpExpression(CprogramParser::NonOpExpressionContext *context) = 0;

    virtual antlrcpp::Any visitUnaryOpAssignment(CprogramParser::UnaryOpAssignmentContext *context) = 0;

    virtual antlrcpp::Any visitUnaryOpExpression(CprogramParser::UnaryOpExpressionContext *context) = 0;

    virtual antlrcpp::Any visitBinaryOpExpression(CprogramParser::BinaryOpExpressionContext *context) = 0;

    virtual antlrcpp::Any visitBinaryOpAssignment(CprogramParser::BinaryOpAssignmentContext *context) = 0;

    virtual antlrcpp::Any visitExpression(CprogramParser::ExpressionContext *context) = 0;

    virtual antlrcpp::Any visitConstant(CprogramParser::ConstantContext *context) = 0;

    virtual antlrcpp::Any visitFunctionCall(CprogramParser::FunctionCallContext *context) = 0;


};

}  // namespace antlr4
