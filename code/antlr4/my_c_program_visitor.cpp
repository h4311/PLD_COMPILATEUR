//
// Created by Horia on 3/12/2018.
//
#include "my_c_program_visitor.h"
#include "../util/ErrorWarningLog.h"
#include "../data_structure/data_structure.h"

using namespace std;
using namespace antlr4;
using namespace antlrcpp;

char unescape(string character) {
    if (character.front() == '\\') {
        switch (character.back()) {
            case 'a' :
                return '\a';
            case 'b' :
                return '\b';
            case 'f' :
                return '\f';
            case 'n' :
                return '\n';
            case 'r':
                return '\r';
            case 't':
                return '\t';
            case 'v':
                return '\v';
            case '\\':
                return '\\';
            case '\'':
                return '\'';
            case '"':
                return '\"';
            case '?':
                return '\?';
            default:
                return character.back();
        }
    }
    return character.back();
}

Any MyCProgramVisitor::visitProgram(CprogramParser::ProgramContext *ctx) {
    auto *program = new Program();
    this->scopeCounter_++;
    this->currentScope_.push(program);
    program->InsertStandardDefinitions();
    for (CprogramParser::DefinitionContext *definitionContext : ctx->definition()) {
        // we need to retrieve a vector here to take into account multiple var declarations into the same definition
        vector<Definition *> visitedDefinitions = visit(definitionContext).as<vector<Definition *>>();

        for (Definition *definition : visitedDefinitions) {
            if (auto *variableDeclaration = dynamic_cast<VariableDeclaration *>(definition)) {
                if (variableDeclaration->HasSymbolInExpression()) {
                    ErrorWarningLog::LogError(InitializerElementNotConstant, definitionContext,
                                              variableDeclaration->GetId_());
                }
                //TODO : Not implemented feature
                ErrorWarningLog::LogError(NotImplemented, ctx);
            }
            program->AddDefinition(definition);
        }
    }
    //check that main exists and has good prototype
    Definition *mainDefinition = program->LookupSymbol("main");
    if (mainDefinition && dynamic_cast<FunctionDefinition *>(mainDefinition)) {
        auto *main = dynamic_cast<FunctionDefinition *>(mainDefinition);
        if (!(main->GetType_() == INT64_T || main->GetType_() == INT32_T) || !main->GetParameters_().empty()) {
            ErrorWarningLog::LogWarningMain(main, ctx);
        }
    } else {
        ErrorWarningLog::LogError(UndefinedReference, ctx, "main");
    }
    this->currentScope_.pop();
    return program;
}

Any MyCProgramVisitor::visitDefinition(CprogramParser::DefinitionContext *ctx) {
    vector<Definition *> definitions;
    if (ctx->functionDefinition()) {
        Any anyFunctionDefinition = visit(ctx->functionDefinition());
        if (anyFunctionDefinition.isNotNull()) {
            definitions.push_back(anyFunctionDefinition.as<FunctionDefinition *>());
        }
    } else {
        vector<VariableDeclaration *> variableDeclarations = visit(ctx->variablesDeclaration())
                .as<vector<VariableDeclaration *>>();
        definitions.insert(definitions.end(), variableDeclarations.begin(), variableDeclarations.end());
    }
    return definitions;
}

Block *MyCProgramVisitor::visitBlock(antlr4::ParserRuleContext *ctx, vector<VariableDeclaration *> parameters) {
    auto *block = new Block(this->currentScope_.top(), "_block" + to_string(this->scopeCounter_));
    this->currentScope_.top()->AddNestedScope(block);
    this->scopeCounter_++;
    this->currentScope_.push(block);
    // in case this block is the block of a function, we need to add the parameters to the scope
    for (int i = 0; i < parameters.size(); i++) {
        // rename for backend symbol table
        parameters[i]->Rename(parameters[i]->GetId_() + this->currentScope_.top()->GetName_());
        if (Definition *symbol = block->InsertSymbol(parameters[i]->GetId_(), parameters[i])) {
            string x = symbol->GetUnrenamedId_(block->GetName_());
            ErrorWarningLog::LogError(ParameterRedefinition, ctx, x);
        }
    }
    for (CprogramParser::StatementContext *statementContext : ctx->getRuleContexts<CprogramParser::StatementContext>()) {
        // we need to retrieve a vector here to take into account multiple var declarations into the same statement
        vector<Statement *> visitedStatements = visit(statementContext).as<vector<Statement *>>();
        for (Statement *statement : visitedStatements) {
            if (auto returnStatement = dynamic_cast<Return *>(statement)) {
                // implicit conversion of returned expression to return type
                if (this->functionReturnType != VOID) {
                    if (returnStatement->GetValue_() != nullptr)
                        returnStatement->GetValue_()->SetType_(this->functionReturnType);
                }
            }
            block->AddStatement(statement);
        }
    }
    this->currentScope_.pop();
    return block;
}

Any MyCProgramVisitor::visitFunctionDefinition(CprogramParser::FunctionDefinitionContext *ctx) {
    vector<VariableDeclaration *> params = visit(ctx->argumentsDefinition()).as<vector<VariableDeclaration *>>();

    if (params.size() > 6) {
        ErrorWarningLog::LogError(NotImplemented, ctx);
    }

    CType type;
    if (ctx->TYPE()) {
        type = CTypeUtil::stringToCType(ctx->TYPE()->getText());
    } else {
        type = VOID;
    }
    string name = ctx->IDENTIFIER()->getText();
    FunctionDefinition *functionDefinition = new FunctionDefinition(type, name, params);
    functionDefinition->SetLineNr_(static_cast<int>(ctx->getStart()->getLine()));
    if (Definition *foundDefinition = currentScope_.top()->InsertSymbol(functionDefinition->GetId_(),
                                                                        functionDefinition)) {
        ErrorWarningLog::LogError(SymbolRedefinition, ctx, functionDefinition->GetId_());
        delete functionDefinition;
        return nullptr;
    } else {
        this->functionReturnType = type;
        Block *block = visitBlock(ctx, params);
        functionDefinition->AddBlock_(block);
        return functionDefinition;
    }
}

Any MyCProgramVisitor::visitArgumentsDefinition(CprogramParser::ArgumentsDefinitionContext *ctx) {
    vector<VariableDeclaration *> parameters;
    for (CprogramParser::ArgumentDefinitionContext *argumentDefinitionContext: ctx->argumentDefinition()) {
        VariableDeclaration *parameter = visit(argumentDefinitionContext).as<VariableDeclaration *>();
        parameter->SetLineNr_(static_cast<int>(ctx->getStart()->getLine()));
        parameters.push_back(parameter);
    }
    return parameters;
}

Any MyCProgramVisitor::visitSimpleArgumentDefinition(CprogramParser::SimpleArgumentDefinitionContext *ctx) {
    CType type = CTypeUtil::stringToCType(ctx->TYPE()->getText());
    string id = ctx->IDENTIFIER()->getText();
    return new VariableDeclaration(type, id, nullptr);
}

Any MyCProgramVisitor::visitArrayArgumentDefinition(CprogramParser::ArrayArgumentDefinitionContext *ctx) {
    //TODO : Not implemented feature
    CType type = CTypeUtil::stringToCType(ctx->TYPE()->getText());
    string id = ctx->IDENTIFIER()->getText();
    Expression *array_size = nullptr;
    if (ctx->binaryOpAssignment()) {
        array_size = visit(ctx->binaryOpAssignment()).as<Expression *>();
    }

    ErrorWarningLog::LogError(NotImplemented, ctx);

    return new VariableDeclaration(type, id, vector<Expression *>(), array_size);
}

// this allows to visit multiple var declarations and mixed initializations
Any MyCProgramVisitor::visitVariablesDeclaration(CprogramParser::VariablesDeclarationContext *ctx) {
    vector<VariableDeclaration *> variable_declarations;
    for (CprogramParser::VariableDeclarationContext *variableDeclarationContext: ctx->variableDeclaration()) {
        // we obtain the type; new pointer each time to avoid dangling pointers when deleting a VariableDeclaration
        CType type = CTypeUtil::stringToCType(ctx->TYPE()->getText());
        VariableDeclaration *variableDeclaration = visit(variableDeclarationContext).as<VariableDeclaration *>();
        // rename for backend symbol table
        variableDeclaration->Rename(variableDeclaration->GetId_() + this->currentScope_.top()->GetName_());
        if (Definition *foundDefinition = currentScope_.top()->InsertSymbol(variableDeclaration->GetId_(),
                                                                            variableDeclaration)) {
            ErrorWarningLog::LogError(SymbolRedefinition, ctx, variableDeclaration->GetId_());
            delete variableDeclaration;
        } else {
            // we set the type (visitVariableDeclaration returns a VariableDeclaration which has null for type)
            variableDeclaration->SetType_(type);
            //implicit conversion of initializer expressions to declared variable's type
            for (Expression *expression : variableDeclaration->GetValues_()) {
                // if the initializer expression has void operands we log error,
                // else we change it's type to the declared type
                if (expression->GetType_() == VOID) {
                    ErrorWarningLog::LogError(RValueHasVoid, ctx);
                } else {
                    expression->SetType_(variableDeclaration->GetType_());
                }
            }
            variableDeclaration->SetLineNr_(static_cast<int>(ctx->getStart()->getLine()));
            variable_declarations.push_back(variableDeclaration);
        }
    }
    return variable_declarations;
}

Any MyCProgramVisitor::visitSimpleVariableDeclaration(CprogramParser::SimpleVariableDeclarationContext *ctx) {
    string id = ctx->IDENTIFIER()->getText();
    if (ctx->binaryOpAssignment(0)) {
        // in case of a simple variable only the first value is taken (even if an initializer list is given)
        Expression *initializer = visit(ctx->binaryOpAssignment(0)).as<Expression *>();
        return new VariableDeclaration(VOID, id, initializer);
    }
    return new VariableDeclaration(VOID, id, nullptr);
}

Any MyCProgramVisitor::visitSizedArrayVariableDeclaration(CprogramParser::SizedArrayVariableDeclarationContext *ctx) {
    string id = ctx->IDENTIFIER()->getText();
    Expression *array_size = visit(ctx->binaryOpAssignment(0)).as<Expression *>();
    if (array_size->HasSymbol()) {
        ErrorWarningLog::LogError(InitializerElementNotConstant, ctx, id);
    } else if (*(array_size->EvaluateExpression()) < 0) {
        ErrorWarningLog::LogError(NegativeArraySize, ctx, id);
    }
    vector<Expression *> values;
    for (int i = 1; i < ctx->binaryOpAssignment().size(); i++) {
        values.push_back(visit(ctx->binaryOpAssignment(static_cast<size_t>(i))).as<Expression *>());
    }
    return new VariableDeclaration(VOID, id, values, array_size);
}

Any
MyCProgramVisitor::visitUnsizedArrayVariableDeclaration(CprogramParser::UnsizedArrayVariableDeclarationContext *ctx) {
    string id = ctx->IDENTIFIER()->getText();
    vector<Expression *> values;
    for (CprogramParser::BinaryOpAssignmentContext *binaryOpAssignmentContext : ctx->binaryOpAssignment()) {
        values.push_back(visit(binaryOpAssignmentContext).as<Expression *>());
    }
    return new VariableDeclaration(VOID, id, values, nullptr);
}

Any MyCProgramVisitor::visitVariableId(CprogramParser::VariableIdContext *ctx) {
    string id = ctx->IDENTIFIER()->getText();
    VariableId *variableId;
    if (ctx->expression()) {
        Expression *index = visit(ctx->expression()).as<Expression *>();
        variableId = new VariableId(id, index);
    } else {
        variableId = new VariableId(id);
    }
    // check that the associated variable was declared
    Definition *definition = currentScope_.top()->LookupSymbol(variableId->GetId_());
    if (definition) {
        if (dynamic_cast<VariableDeclaration *>(definition)) {
            variableId->SetType_(definition->GetType_());
            definition->SetUsed_(true);
            // rename for backend symbol table
            variableId->Rename(definition->GetId_());
        } else {
            ErrorWarningLog::LogError(NotAVariableId, ctx, variableId->GetId_());
            // assume type is int so that we can continue compilation to find other errors
            variableId->SetType_(INT64_T);
        }
    } else {
        ErrorWarningLog::LogError(VariableNeverDeclared, ctx, variableId->GetId_());
        // assume type is int so that we can continue compilation to find other errors
        variableId->SetType_(INT64_T);
    }
    return variableId;
}

Any MyCProgramVisitor::visitWhileStatement(CprogramParser::WhileStatementContext *ctx) {
    Expression *condition = visit(ctx->expression()).as<Expression *>();
    inLoop++;
    Statement *statement = visit(ctx->statementNoDeclaration()).as<Statement *>();
    inLoop--;
    Statement *whileStatement = new While(condition, statement);
    return whileStatement;
}

Any MyCProgramVisitor::visitIfStatement(CprogramParser::IfStatementContext *ctx) {
    Expression *condition = visit(ctx->expression()).as<Expression *>();
    Statement *statement_true = visit(ctx->statementNoDeclaration(0)).as<Statement *>();
    Statement *ifStatement;
    if (ctx->ELSE()) {
        Statement *statement_false = visit(ctx->statementNoDeclaration(1)).as<Statement *>();
        ifStatement = new If(condition, statement_true, statement_false);
    } else {
        ifStatement = new If(condition, statement_true);
    }
    return ifStatement;
}

Any MyCProgramVisitor::visitExpressionStatement(CprogramParser::ExpressionStatementContext *ctx) {
    Statement *expressionStatement = visit(ctx->expression()).as<Expression *>();
    return expressionStatement;
}

Any MyCProgramVisitor::visitReturnStatement(CprogramParser::ReturnStatementContext *ctx) {
    Statement *returnStatement;
    if (ctx->expression()) {
        Expression *returnExpression = visit(ctx->expression()).as<Expression *>();
        returnStatement = new Return(returnExpression);
    } else {
        returnStatement = new Return();
    }
    return returnStatement;
}

Any MyCProgramVisitor::visitBreakStatement(CprogramParser::BreakStatementContext *ctx) {
    if (inLoop == 0)
        ErrorWarningLog::LogError(BreakNotInLoop, ctx);
    Statement *breakStatement = new Break();
    return breakStatement;
}

Any MyCProgramVisitor::visitContinueStatement(CprogramParser::ContinueStatementContext *ctx) {
    if (inLoop == 0) {
        ErrorWarningLog::LogError(ContinueNotInLoop, ctx);
    }
    Statement *continueStatement = new Continue();
    return continueStatement;
}

Any MyCProgramVisitor::visitAnonymousBlockStatement(CprogramParser::AnonymousBlockStatementContext *ctx) {
    Statement *block = visitBlock(ctx, {});
    return block;
}

Any MyCProgramVisitor::visitStatement(CprogramParser::StatementContext *ctx) {
    vector<Statement *> statements;
    if (ctx->variablesDeclaration()) {
        vector<VariableDeclaration *> declarations = visit(
                ctx->variablesDeclaration()).as<vector<VariableDeclaration *>>();
        statements.insert(statements.end(), declarations.begin(), declarations.end());
    } else {
        statements.push_back(visit(ctx->statementNoDeclaration()).as<Statement *>());
    }

    return statements;
}

Any MyCProgramVisitor::visitNonOpExpression(CprogramParser::NonOpExpressionContext *ctx) {
    Expression *nonOpExpression;
    if (ctx->functionCall()) {
        nonOpExpression = visit(ctx->functionCall()).as<FunctionCall *>();
    } else if (ctx->variableId()) {
        nonOpExpression = visit(ctx->variableId()).as<VariableId *>();
    } else if (ctx->constant()) {
        nonOpExpression = visit(ctx->constant()).as<Const *>();
    } else {
        nonOpExpression = visit(ctx->expression()).as<Expression *>();
    }
    return nonOpExpression;
}

Any MyCProgramVisitor::visitUnaryOpAssignment(CprogramParser::UnaryOpAssignmentContext *ctx) {
    Expression *unaryOpAssignmentExpression;
    if (ctx->nonOpExpression()) {
        unaryOpAssignmentExpression = visit(ctx->nonOpExpression()).as<Expression *>();
    } else {
        VariableId *variableId = visit(ctx->variableId()).as<VariableId *>();
        Operator *unaryAssignmentOp;
        if (ctx->children[0]->getText() == ctx->op->getText()) {
            unaryAssignmentOp = new Operator(ctx->op->getText(), 0);
        } else {
            unaryAssignmentOp = new Operator(ctx->op->getText(), 1);
        }
        unaryOpAssignmentExpression = new UnaryOpAssignment(variableId, unaryAssignmentOp);
        //resolve type
        unaryOpAssignmentExpression->SetType_(variableId->GetType_());
    }
    return unaryOpAssignmentExpression;
}

Any MyCProgramVisitor::visitUnaryOpExpression(CprogramParser::UnaryOpExpressionContext *ctx) {
    Expression *unaryOpExpression;
    if (ctx->unaryOpAssignment()) {
        unaryOpExpression = visit(ctx->unaryOpAssignment()).as<Expression *>();
    } else {
        Expression *expression = visit(ctx->unaryOpExpression()).as<Expression *>();
        Operator *unaryOp = new Operator(ctx->op->getText(), 0);
        unaryOpExpression = new UnaryOpExpression(expression, unaryOp);
        if (expression->GetType_() == VOID) {
            ErrorWarningLog::LogError(RValueHasVoid, ctx);
        }
        //resolve type
        if (expression->GetType_() == CHAR)
            unaryOpExpression->SetType_(INT32_T);
        else unaryOpExpression->SetType_(expression->GetType_());
    }
    return unaryOpExpression;
}

Any MyCProgramVisitor::visitBinaryOpExpression(CprogramParser::BinaryOpExpressionContext *ctx) {
    Expression *binaryOpExpression;
    if (ctx->unaryOpExpression()) {
        binaryOpExpression = visit(ctx->unaryOpExpression()).as<Expression *>();
    } else {
        Expression *expressionLeft = visit(ctx->binaryOpExpression(0)).as<Expression *>();
        Expression *expressionRight = visit(ctx->binaryOpExpression(1)).as<Expression *>();
        Operator *binaryOp = new Operator(ctx->op->getText(), 1);
        binaryOpExpression = new BinaryOpExpression(expressionLeft, expressionRight, binaryOp);
        //resolve type
        CType type = CTypeUtil::GetBiggestType(expressionLeft->GetType_(), expressionRight->GetType_());
        binaryOpExpression->SetType_(type);
        //check that neither operand is of void type
        if (expressionLeft->GetType_() == VOID) {
            ErrorWarningLog::LogError(RValueHasVoid, ctx);
        }
        if (expressionRight->GetType_() == VOID) {
            ErrorWarningLog::LogError(RValueHasVoid, ctx);
        }
    }
    return binaryOpExpression;
}

Any MyCProgramVisitor::visitBinaryOpAssignment(CprogramParser::BinaryOpAssignmentContext *ctx) {
    Expression *binaryOpAssignmentExpression;
    if (ctx->binaryOpExpression()) {
        binaryOpAssignmentExpression = visit(ctx->binaryOpExpression()).as<Expression *>();
    } else {
        VariableId *variableId = visit(ctx->variableId()).as<VariableId *>();
        Expression *expressionRight = visit(ctx->binaryOpAssignment()).as<Expression *>();
        // check that the expression to be assigned to the variable doesn't have void operands
        if (expressionRight->GetType_() == VOID) {
            ErrorWarningLog::LogError(RValueHasVoid, ctx);
        }
        Operator *binaryAssignmentOp = new Operator(ctx->op->getText(), 1);
        binaryOpAssignmentExpression = new BinaryOpAssignment(variableId, expressionRight, binaryAssignmentOp);
        //resolve type
        binaryOpAssignmentExpression->SetType_(variableId->GetType_());
    }
    return binaryOpAssignmentExpression;
}

Any MyCProgramVisitor::visitExpression(CprogramParser::ExpressionContext *ctx) {
    Expression *expression;
    if (ctx->binaryOpAssignment()) {
        expression = visit(ctx->binaryOpAssignment());
    } else {
        Expression *expressionLeft = visit(ctx->expression(0)).as<Expression *>();
        Expression *expressionRight = visit(ctx->expression(1)).as<Expression *>();
        auto *binaryOp = new Operator(COMMA);
        expression = new BinaryOpExpression(expressionLeft, expressionRight, binaryOp);
        //resolve type
        expression->SetType_(expressionRight->GetType_());
    }
    return expression;
}

Any MyCProgramVisitor::visitConstant(CprogramParser::ConstantContext *ctx) {
    if (ctx->CHAR()) {
        string rawCharacter = ctx->CHAR()->getText();
        return new Const(CHAR, unescape(rawCharacter.substr(1, rawCharacter.size() - 2)));
    } else if (ctx->NUM()) {
        try {
            return new Const(INT32_T, stoi(ctx->NUM()->getText()));
        } catch (std::out_of_range &ex) {
            //TODO should we try/catch here as well?
            return new Const(INT64_T, stol(ctx->NUM()->getText()));
        }
    }
}

Any MyCProgramVisitor::visitFunctionCall(CprogramParser::FunctionCallContext *ctx) {
    string id = ctx->IDENTIFIER()->getText();
    vector<Expression *> arguments;
    for (CprogramParser::BinaryOpAssignmentContext *binaryOpAssignmentContext : ctx->binaryOpAssignment()) {
        arguments.push_back(visit(binaryOpAssignmentContext).as<Expression *>());
    }
    FunctionCall *functionCall = new FunctionCall(id, arguments);
    Definition *definition = currentScope_.top()->LookupSymbol(functionCall->GetId_());
    if (definition) {
        if (auto functionDefinition = dynamic_cast<FunctionDefinition *>(definition)) {
            //check parameters
            const vector<VariableDeclaration *> &parameters = functionDefinition->GetParameters_();
            for (int i = 0; i < min(arguments.size(), parameters.size()); i++) {
                //implicit conversion of call arguments to parameters' type
                arguments[i]->SetType_(parameters[i]->GetType_());
            }
            functionCall->SetType_(definition->GetType_());
            definition->SetUsed_(true);
            if (arguments.size() < parameters.size()) {
                ErrorWarningLog::LogError(TooFewArguments, ctx, functionCall->GetId_());
            } else if (arguments.size() > parameters.size()) {
                ErrorWarningLog::LogError(TooManyArguments, ctx, functionCall->GetId_());
            }
        } else {
            ErrorWarningLog::LogError(NotAFunction, ctx, functionCall->GetId_());
            // assume type is int so that we can continue compilation to find other errors
            functionCall->SetType_(INT64_T);
        }
    } else {
        ErrorWarningLog::LogError(FunctionNeverDeclared, ctx, functionCall->GetId_());
        // assume type is int so that we can continue compilation to find other errors
        functionCall->SetType_(INT64_T);
    }
    return functionCall;
}
