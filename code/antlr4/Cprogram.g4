grammar Cprogram ;

program : ( definition )* EOF;

definition: ( variablesDeclaration | functionDefinition ) ;

// Function
functionDefinition : ( TYPE | VOID ) IDENTIFIER '(' argumentsDefinition ')' '{' ( statement )* '}';

argumentsDefinition : ( ( argumentDefinition ( ',' argumentDefinition )* )? | VOID ) ;

argumentDefinition : TYPE IDENTIFIER                                    #simpleArgumentDefinition
                   | TYPE IDENTIFIER '[' ( binaryOpAssignment )? ']'    #arrayArgumentDefinition
                   ;

// Variable
variablesDeclaration : TYPE variableDeclaration ( ',' variableDeclaration )* ';' ;

variableDeclaration : IDENTIFIER ( '=' ( binaryOpAssignment | '{' binaryOpAssignment ( ',' binaryOpAssignment )* '}' ) )?       #simpleVariableDeclaration
                    | IDENTIFIER '[' binaryOpAssignment ']' ('=' '{' binaryOpAssignment ( ',' binaryOpAssignment )* '}' )?      #sizedArrayVariableDeclaration
                    | IDENTIFIER '[' ']' '=' '{' binaryOpAssignment ( ',' binaryOpAssignment )* '}'                             #unsizedArrayVariableDeclaration
                    ;

variableId : ( IDENTIFIER | IDENTIFIER '[' expression ']' ) ;

// Statements
statementNoDeclaration : WHILE '(' expression ')' statementNoDeclaration                                    #whileStatement
                       | IF '(' expression ')' statementNoDeclaration ( ELSE statementNoDeclaration )?      #ifStatement
                       | expression ';'                                                                     #expressionStatement
                       | RETURN expression? ';'                                                             #returnStatement
                       | BREAK ';'                                                                          #breakStatement
                       | CONTINUE ';'                                                                       #continueStatement
                       | '{' ( statement )* '}'                                                             #anonymousBlockStatement
                       ;

statement : statementNoDeclaration | variablesDeclaration;

nonOpExpression : functionCall
                | variableId
                | constant
                | '(' expression ')'
                ;

unaryOpAssignment : nonOpExpression
                  | variableId op=('++' | '--')
                  | op=('++' | '--') variableId
                  ;

unaryOpExpression : unaryOpAssignment
                  | op=('+' | '-' | '!' | '~') unaryOpExpression
                  ;

binaryOpExpression : unaryOpExpression
                   | binaryOpExpression op=('*' | '/' | '%') binaryOpExpression
                   | binaryOpExpression op=('+' | '-') binaryOpExpression
                   | binaryOpExpression op=('>>' | '<<') binaryOpExpression
                   | binaryOpExpression op=('<' | '<=' | '>' | '>=') binaryOpExpression
                   | binaryOpExpression op=('==' | '!=') binaryOpExpression
                   | binaryOpExpression op='&' binaryOpExpression
                   | binaryOpExpression op='^' binaryOpExpression
                   | binaryOpExpression op='|' binaryOpExpression
                   | binaryOpExpression op='&&' binaryOpExpression
                   | binaryOpExpression op='||' binaryOpExpression
                   ;

binaryOpAssignment : binaryOpExpression
                   | variableId op=('=' | '+=' | '-=' | '*=' | '/=' | '%=' | '<<=' | '>>=' | '&=' | '^=' | '|=') binaryOpAssignment
                   ;

expression : binaryOpAssignment
           | expression op=',' expression
           ;

constant : ( NUM | CHAR ) ;

functionCall : IDENTIFIER '(' ( binaryOpAssignment ( ',' binaryOpAssignment )* )? ')' ;

// lexer
IF : 'if' ;
ELSE : 'else' ;
WHILE : 'while' ;
VOID : 'void' ;
RETURN : 'return' ;
BREAK : 'break';
CONTINUE : 'continue';
TYPE : 'int32_t' | 'int64_t' | 'char' ;
NUM : [0-9]+ ;
IDENTIFIER : [a-zA-Z] [a-zA-Z0-9]* ;
CHAR : '\'' '\\'? . '\'' ;
COMMENT : '/*' .*? '*/' -> skip ;
LINE_COMMENT: '//' ~[\r\n]* -> skip ;
PREPROCESSOR : '#' ~[\r\n]* -> skip ;
NEWLINE : '\r'?'\n' -> skip ;
WHITESPACE : (' '|'\t')+ -> skip ;
