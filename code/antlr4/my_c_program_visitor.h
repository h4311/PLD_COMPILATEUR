//
// Created by Horia on 3/10/2018.
//
#pragma once

#include "CprogramBaseVisitor.h"
#include "../data_structure/scope.h"
#include "../data_structure/variable_declaration.h"
#include "../data_structure/block.h"
#include <stack>

class MyCProgramVisitor : public antlr4::CprogramBaseVisitor {
public:
    antlrcpp::Any visitProgram(antlr4::CprogramParser::ProgramContext *ctx) override;

    antlrcpp::Any visitDefinition(antlr4::CprogramParser::DefinitionContext *ctx) override;

    antlrcpp::Any visitFunctionDefinition(antlr4::CprogramParser::FunctionDefinitionContext *ctx) override;

    antlrcpp::Any visitArgumentsDefinition(antlr4::CprogramParser::ArgumentsDefinitionContext *ctx) override;

    antlrcpp::Any visitSimpleArgumentDefinition(antlr4::CprogramParser::SimpleArgumentDefinitionContext *ctx) override;

    antlrcpp::Any visitArrayArgumentDefinition(antlr4::CprogramParser::ArrayArgumentDefinitionContext *ctx) override;

    antlrcpp::Any visitVariablesDeclaration(antlr4::CprogramParser::VariablesDeclarationContext *ctx) override;

    antlrcpp::Any visitSimpleVariableDeclaration(
            antlr4::CprogramParser::SimpleVariableDeclarationContext *ctx) override;

    antlrcpp::Any visitSizedArrayVariableDeclaration(
            antlr4::CprogramParser::SizedArrayVariableDeclarationContext *ctx) override;

    antlrcpp::Any visitUnsizedArrayVariableDeclaration(
            antlr4::CprogramParser::UnsizedArrayVariableDeclarationContext *ctx) override;

    antlrcpp::Any visitVariableId(antlr4::CprogramParser::VariableIdContext *ctx) override;

    antlrcpp::Any visitWhileStatement(antlr4::CprogramParser::WhileStatementContext *ctx) override;

    antlrcpp::Any visitIfStatement(antlr4::CprogramParser::IfStatementContext *ctx) override;

    antlrcpp::Any visitExpressionStatement(antlr4::CprogramParser::ExpressionStatementContext *ctx) override;

    antlrcpp::Any visitReturnStatement(antlr4::CprogramParser::ReturnStatementContext *ctx) override;

    antlrcpp::Any visitBreakStatement(antlr4::CprogramParser::BreakStatementContext *ctx) override;

    antlrcpp::Any visitContinueStatement(antlr4::CprogramParser::ContinueStatementContext *ctx) override;

    antlrcpp::Any visitAnonymousBlockStatement(antlr4::CprogramParser::AnonymousBlockStatementContext *ctx) override;

    antlrcpp::Any visitStatement(antlr4::CprogramParser::StatementContext *ctx) override;

    antlrcpp::Any visitNonOpExpression(antlr4::CprogramParser::NonOpExpressionContext *ctx) override;

    antlrcpp::Any visitUnaryOpAssignment(antlr4::CprogramParser::UnaryOpAssignmentContext *ctx) override;

    antlrcpp::Any visitUnaryOpExpression(antlr4::CprogramParser::UnaryOpExpressionContext *ctx) override;

    antlrcpp::Any visitBinaryOpExpression(antlr4::CprogramParser::BinaryOpExpressionContext *ctx) override;

    antlrcpp::Any visitBinaryOpAssignment(antlr4::CprogramParser::BinaryOpAssignmentContext *ctx) override;

    antlrcpp::Any visitExpression(antlr4::CprogramParser::ExpressionContext *ctx) override;

    antlrcpp::Any visitConstant(antlr4::CprogramParser::ConstantContext *ctx) override;

    antlrcpp::Any visitFunctionCall(antlr4::CprogramParser::FunctionCallContext *ctx) override;

private:
    Block *visitBlock(antlr4::ParserRuleContext *ctx, std::vector<VariableDeclaration *> parameters);

    std::stack<Scope *> currentScope_;
    int scopeCounter_ = 0;
    int inLoop = 0;
    //is only well-defined when visiting a block
    CType functionReturnType;
};
