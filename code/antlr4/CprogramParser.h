
// Generated from D:/burca/Works/INSA-works/IF/4/PLD_COMPILATEUR/code/antlr4\Cprogram.g4 by ANTLR 4.7

#pragma once


#include "antlr4-runtime.h"


namespace antlr4 {


class  CprogramParser : public antlr4::Parser {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, 
    T__7 = 8, T__8 = 9, T__9 = 10, T__10 = 11, T__11 = 12, T__12 = 13, T__13 = 14, 
    T__14 = 15, T__15 = 16, T__16 = 17, T__17 = 18, T__18 = 19, T__19 = 20, 
    T__20 = 21, T__21 = 22, T__22 = 23, T__23 = 24, T__24 = 25, T__25 = 26, 
    T__26 = 27, T__27 = 28, T__28 = 29, T__29 = 30, T__30 = 31, T__31 = 32, 
    T__32 = 33, T__33 = 34, T__34 = 35, T__35 = 36, T__36 = 37, T__37 = 38, 
    T__38 = 39, T__39 = 40, T__40 = 41, IF = 42, ELSE = 43, WHILE = 44, 
    VOID = 45, RETURN = 46, BREAK = 47, CONTINUE = 48, TYPE = 49, NUM = 50, 
    IDENTIFIER = 51, CHAR = 52, COMMENT = 53, LINE_COMMENT = 54, PREPROCESSOR = 55, 
    NEWLINE = 56, WHITESPACE = 57
  };

  enum {
    RuleProgram = 0, RuleDefinition = 1, RuleFunctionDefinition = 2, RuleArgumentsDefinition = 3, 
    RuleArgumentDefinition = 4, RuleVariablesDeclaration = 5, RuleVariableDeclaration = 6, 
    RuleVariableId = 7, RuleStatementNoDeclaration = 8, RuleStatement = 9, 
    RuleNonOpExpression = 10, RuleUnaryOpAssignment = 11, RuleUnaryOpExpression = 12, 
    RuleBinaryOpExpression = 13, RuleBinaryOpAssignment = 14, RuleExpression = 15, 
    RuleConstant = 16, RuleFunctionCall = 17
  };

  CprogramParser(antlr4::TokenStream *input);
  ~CprogramParser();

  virtual std::string getGrammarFileName() const override;
  virtual const antlr4::atn::ATN& getATN() const override { return _atn; };
  virtual const std::vector<std::string>& getTokenNames() const override { return _tokenNames; }; // deprecated: use vocabulary instead.
  virtual const std::vector<std::string>& getRuleNames() const override;
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;


  class ProgramContext;
  class DefinitionContext;
  class FunctionDefinitionContext;
  class ArgumentsDefinitionContext;
  class ArgumentDefinitionContext;
  class VariablesDeclarationContext;
  class VariableDeclarationContext;
  class VariableIdContext;
  class StatementNoDeclarationContext;
  class StatementContext;
  class NonOpExpressionContext;
  class UnaryOpAssignmentContext;
  class UnaryOpExpressionContext;
  class BinaryOpExpressionContext;
  class BinaryOpAssignmentContext;
  class ExpressionContext;
  class ConstantContext;
  class FunctionCallContext; 

  class  ProgramContext : public antlr4::ParserRuleContext {
  public:
    ProgramContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *EOF();
    std::vector<DefinitionContext *> definition();
    DefinitionContext* definition(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ProgramContext* program();

  class  DefinitionContext : public antlr4::ParserRuleContext {
  public:
    DefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    VariablesDeclarationContext *variablesDeclaration();
    FunctionDefinitionContext *functionDefinition();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  DefinitionContext* definition();

  class  FunctionDefinitionContext : public antlr4::ParserRuleContext {
  public:
    FunctionDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *IDENTIFIER();
    ArgumentsDefinitionContext *argumentsDefinition();
    antlr4::tree::TerminalNode *TYPE();
    antlr4::tree::TerminalNode *VOID();
    std::vector<StatementContext *> statement();
    StatementContext* statement(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionDefinitionContext* functionDefinition();

  class  ArgumentsDefinitionContext : public antlr4::ParserRuleContext {
  public:
    ArgumentsDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *VOID();
    std::vector<ArgumentDefinitionContext *> argumentDefinition();
    ArgumentDefinitionContext* argumentDefinition(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ArgumentsDefinitionContext* argumentsDefinition();

  class  ArgumentDefinitionContext : public antlr4::ParserRuleContext {
  public:
    ArgumentDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ArgumentDefinitionContext() : antlr4::ParserRuleContext() { }
    void copyFrom(ArgumentDefinitionContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  ArrayArgumentDefinitionContext : public ArgumentDefinitionContext {
  public:
    ArrayArgumentDefinitionContext(ArgumentDefinitionContext *ctx);

    antlr4::tree::TerminalNode *TYPE();
    antlr4::tree::TerminalNode *IDENTIFIER();
    BinaryOpAssignmentContext *binaryOpAssignment();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  SimpleArgumentDefinitionContext : public ArgumentDefinitionContext {
  public:
    SimpleArgumentDefinitionContext(ArgumentDefinitionContext *ctx);

    antlr4::tree::TerminalNode *TYPE();
    antlr4::tree::TerminalNode *IDENTIFIER();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ArgumentDefinitionContext* argumentDefinition();

  class  VariablesDeclarationContext : public antlr4::ParserRuleContext {
  public:
    VariablesDeclarationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *TYPE();
    std::vector<VariableDeclarationContext *> variableDeclaration();
    VariableDeclarationContext* variableDeclaration(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariablesDeclarationContext* variablesDeclaration();

  class  VariableDeclarationContext : public antlr4::ParserRuleContext {
  public:
    VariableDeclarationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    VariableDeclarationContext() : antlr4::ParserRuleContext() { }
    void copyFrom(VariableDeclarationContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  SimpleVariableDeclarationContext : public VariableDeclarationContext {
  public:
    SimpleVariableDeclarationContext(VariableDeclarationContext *ctx);

    antlr4::tree::TerminalNode *IDENTIFIER();
    std::vector<BinaryOpAssignmentContext *> binaryOpAssignment();
    BinaryOpAssignmentContext* binaryOpAssignment(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  SizedArrayVariableDeclarationContext : public VariableDeclarationContext {
  public:
    SizedArrayVariableDeclarationContext(VariableDeclarationContext *ctx);

    antlr4::tree::TerminalNode *IDENTIFIER();
    std::vector<BinaryOpAssignmentContext *> binaryOpAssignment();
    BinaryOpAssignmentContext* binaryOpAssignment(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  UnsizedArrayVariableDeclarationContext : public VariableDeclarationContext {
  public:
    UnsizedArrayVariableDeclarationContext(VariableDeclarationContext *ctx);

    antlr4::tree::TerminalNode *IDENTIFIER();
    std::vector<BinaryOpAssignmentContext *> binaryOpAssignment();
    BinaryOpAssignmentContext* binaryOpAssignment(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  VariableDeclarationContext* variableDeclaration();

  class  VariableIdContext : public antlr4::ParserRuleContext {
  public:
    VariableIdContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *IDENTIFIER();
    ExpressionContext *expression();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariableIdContext* variableId();

  class  StatementNoDeclarationContext : public antlr4::ParserRuleContext {
  public:
    StatementNoDeclarationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    StatementNoDeclarationContext() : antlr4::ParserRuleContext() { }
    void copyFrom(StatementNoDeclarationContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  WhileStatementContext : public StatementNoDeclarationContext {
  public:
    WhileStatementContext(StatementNoDeclarationContext *ctx);

    antlr4::tree::TerminalNode *WHILE();
    ExpressionContext *expression();
    StatementNoDeclarationContext *statementNoDeclaration();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  AnonymousBlockStatementContext : public StatementNoDeclarationContext {
  public:
    AnonymousBlockStatementContext(StatementNoDeclarationContext *ctx);

    std::vector<StatementContext *> statement();
    StatementContext* statement(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  BreakStatementContext : public StatementNoDeclarationContext {
  public:
    BreakStatementContext(StatementNoDeclarationContext *ctx);

    antlr4::tree::TerminalNode *BREAK();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExpressionStatementContext : public StatementNoDeclarationContext {
  public:
    ExpressionStatementContext(StatementNoDeclarationContext *ctx);

    ExpressionContext *expression();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ContinueStatementContext : public StatementNoDeclarationContext {
  public:
    ContinueStatementContext(StatementNoDeclarationContext *ctx);

    antlr4::tree::TerminalNode *CONTINUE();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  IfStatementContext : public StatementNoDeclarationContext {
  public:
    IfStatementContext(StatementNoDeclarationContext *ctx);

    antlr4::tree::TerminalNode *IF();
    ExpressionContext *expression();
    std::vector<StatementNoDeclarationContext *> statementNoDeclaration();
    StatementNoDeclarationContext* statementNoDeclaration(size_t i);
    antlr4::tree::TerminalNode *ELSE();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ReturnStatementContext : public StatementNoDeclarationContext {
  public:
    ReturnStatementContext(StatementNoDeclarationContext *ctx);

    antlr4::tree::TerminalNode *RETURN();
    ExpressionContext *expression();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  StatementNoDeclarationContext* statementNoDeclaration();

  class  StatementContext : public antlr4::ParserRuleContext {
  public:
    StatementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    StatementNoDeclarationContext *statementNoDeclaration();
    VariablesDeclarationContext *variablesDeclaration();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StatementContext* statement();

  class  NonOpExpressionContext : public antlr4::ParserRuleContext {
  public:
    NonOpExpressionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    FunctionCallContext *functionCall();
    VariableIdContext *variableId();
    ConstantContext *constant();
    ExpressionContext *expression();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  NonOpExpressionContext* nonOpExpression();

  class  UnaryOpAssignmentContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *op = nullptr;;
    UnaryOpAssignmentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    NonOpExpressionContext *nonOpExpression();
    VariableIdContext *variableId();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  UnaryOpAssignmentContext* unaryOpAssignment();

  class  UnaryOpExpressionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *op = nullptr;;
    UnaryOpExpressionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    UnaryOpAssignmentContext *unaryOpAssignment();
    UnaryOpExpressionContext *unaryOpExpression();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  UnaryOpExpressionContext* unaryOpExpression();

  class  BinaryOpExpressionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *op = nullptr;;
    BinaryOpExpressionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    UnaryOpExpressionContext *unaryOpExpression();
    std::vector<BinaryOpExpressionContext *> binaryOpExpression();
    BinaryOpExpressionContext* binaryOpExpression(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BinaryOpExpressionContext* binaryOpExpression();
  BinaryOpExpressionContext* binaryOpExpression(int precedence);
  class  BinaryOpAssignmentContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *op = nullptr;;
    BinaryOpAssignmentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    BinaryOpExpressionContext *binaryOpExpression();
    VariableIdContext *variableId();
    BinaryOpAssignmentContext *binaryOpAssignment();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BinaryOpAssignmentContext* binaryOpAssignment();

  class  ExpressionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *op = nullptr;;
    ExpressionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    BinaryOpAssignmentContext *binaryOpAssignment();
    std::vector<ExpressionContext *> expression();
    ExpressionContext* expression(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ExpressionContext* expression();
  ExpressionContext* expression(int precedence);
  class  ConstantContext : public antlr4::ParserRuleContext {
  public:
    ConstantContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *NUM();
    antlr4::tree::TerminalNode *CHAR();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ConstantContext* constant();

  class  FunctionCallContext : public antlr4::ParserRuleContext {
  public:
    FunctionCallContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *IDENTIFIER();
    std::vector<BinaryOpAssignmentContext *> binaryOpAssignment();
    BinaryOpAssignmentContext* binaryOpAssignment(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionCallContext* functionCall();


  virtual bool sempred(antlr4::RuleContext *_localctx, size_t ruleIndex, size_t predicateIndex) override;
  bool binaryOpExpressionSempred(BinaryOpExpressionContext *_localctx, size_t predicateIndex);
  bool expressionSempred(ExpressionContext *_localctx, size_t predicateIndex);

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

}  // namespace antlr4
