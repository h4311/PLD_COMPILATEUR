cmake_minimum_required(VERSION 3.5)
project(PLDComp)

set(CMAKE_CXX_STANDARD 11)

#these flags can be used to have full information about what cmake does
#set(CMAKE_RULE_MESSAGES OFF)
#set(CMAKE_VERBOSE_MAKEFILE ON)

find_library(ANTLR4_LIB libantlr4-runtime.a HINTS ${CMAKE_SOURCE_DIR}/antlr4 /usr/local/lib)
find_path(ANTLR4_INC antlr4-runtime.h HINTS ${CMAKE_SOURCE_DIR}/antlr4/include/antlr4-runtime /usr/local/include/antlr4-runtime)

message("lib : " ${ANTLR4_LIB})
message("inc : " ${ANTLR4_INC})

#gtest library is compiled with this flag;
#we need to use it for the rest of the code to avoid runtime errors during testing.
add_definitions(-DGTEST_HAS_PTHREAD=1)

add_subdirectory(antlr4)
add_subdirectory(data_structure)
add_subdirectory(test)

add_executable(PLDComp main.cpp $<TARGET_OBJECTS:data_structure>)

target_link_libraries(PLDComp antlr4)
