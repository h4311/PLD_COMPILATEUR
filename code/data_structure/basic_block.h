//
// Created by burca on 3/26/2018.
//
#pragma once

/**  The class for a basic block */
#include <string>
#include <vector>
#include <ostream>
#include "ir_instruction.h"

class CFG;

class Statement;

class BasicBlock {
public:
    BasicBlock(CFG *cfg, std::string entryLabel);

    BasicBlock *GetExitTrue_() const;

    BasicBlock *GetExitFalse_() const;

    void GenAsm(std::ostream &o); /**< x86 assembly code generation for this basic block (very simple) */

    void SetExitTrue_(BasicBlock *block);

    void SetExitFalse_(BasicBlock *block);

    CFG *cfg_; /** < the CFG where this block belongs */

    void AddIRInstr(IRInstruction::Operation op, CType t, std::vector<std::string> params);

    const std::string &GetLabel_() const;

    void SetConditionVar_(const std::string &conditionVar);

    void LockExits();

    virtual ~BasicBlock();

protected:
    BasicBlock *exitTrue_ = nullptr;  /**< pointer to the next basic block, true branch. If nullptr, return from procedure */
    BasicBlock *exitFalse_ = nullptr; /**< pointer to the next basic block, false branch. If null_ptr, the basic block ends with an unconditional jump */
    std::string label_; /**< label of the BB, also will be the label in the generated code */
    std::vector<IRInstruction *> instrs_; /** < the instructions themselves. */
    std::string conditionVar_;
    bool exitsLocked_;
};
