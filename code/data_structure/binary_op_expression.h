#pragma once

#include "op_expression.h"

class BinaryOpExpression : public OpExpression {
public:
    BinaryOpExpression(Expression *expression1, Expression *expression2, Operator *expressionOperator)
            : OpExpression(expressionOperator), expression1_(expression1), expression2_(expression2) {}

    bool Equals(const Statement &other) const override;

    std::string Print() const override;

    Expression *GetExpression1_() const;

    Expression *GetExpression2_() const;

    ~BinaryOpExpression() override;

    std::string BuildIR(CFG *cfg) const override;

    bool HasSymbol() const override;

    bool IsUseful() const override;

    long *EvaluateExpression() override;

private:
    Expression *expression1_;
    Expression *expression2_;
};
