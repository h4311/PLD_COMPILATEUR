#pragma once

#include "statement.h"
#include "type.h"

class Expression : public Statement {
public:
    Expression() = default;

    Expression(CType type) : type_(type) {}

    std::string Print() const override = 0;

    virtual bool Equals(const Statement &other) const = 0;

    ~Expression() = default;

    CType GetType_() const {
        return type_;
    }

    void SetType_(CType type) {
        type_ = type;
    }

    virtual bool HasSymbol() const = 0;

    virtual long *EvaluateExpression() = 0;

protected:
    CType type_;
};
