#pragma once

#include <string>
#include "cfg.h"

class Statement {
public:
    Statement() = default;

    virtual std::string Print() const = 0;

    virtual bool Equals(const Statement &other) const = 0;

    virtual ~Statement() = default;

    virtual std::string BuildIR(CFG *cfg) const = 0;

    virtual bool IsUseful() const = 0;
};
