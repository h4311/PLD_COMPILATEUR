#pragma once

#include "type.h"

class Scope;

class Definition {
public:
    Definition(CType type, const std::string &id)
            : type_(type), id_(id), used_(false) {}

    virtual bool Equals(const Definition &other) const = 0;

    virtual std::string Print() const { return this->id_; };

    CType GetType_() const {
        return type_;
    }

    void SetType_(CType type_) {
        Definition::type_ = type_;
    }

    const std::string &GetId_() const {
        return id_;
    }

    std::string GetUnrenamedId_(std::string suffix) const {
        unsigned long idLastChar = id_.find(suffix);
        if (idLastChar != std::string::npos) {
            return id_.substr(0, idLastChar);
        } else {
            return id_;
        }
    }

    int GetLineNr_() const {
        return this->lineNr_;
    }

    void SetLineNr_(int lineNr) {
        this->lineNr_ = lineNr;
    }

    virtual ~Definition() = default;

    bool IsUsed_() const {
        return used_;
    }

    void SetUsed_(bool used_) {
        Definition::used_ = used_;
    }

    void Rename(std::string newId) {
        this->id_ = newId;
    }

protected:
    CType type_;
    std::string id_;
    int lineNr_;
    bool used_;
};
