#pragma once

#include "op_expression.h"
#include "variable_id.h"

class UnaryOpAssignment : public OpExpression {
public:
    UnaryOpAssignment(VariableId *variable_id, Operator *expression_operator)
            : OpExpression(expression_operator), variableId_(variable_id) {}

    bool Equals(const Statement &other) const override;

    std::string Print() const override;

    VariableId *GetVariableId_() const;

    ~UnaryOpAssignment() override;

    bool HasSymbol() const override;

    bool IsUseful() const override;

    long *EvaluateExpression() override;

    std::string BuildIR(CFG *cfg) const override;

private:
    VariableId *variableId_;
};
