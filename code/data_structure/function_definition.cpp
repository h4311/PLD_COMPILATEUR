//
// Created by Horia on 3/11/2018.
//
#include <iostream>
#include "function_definition.h"

using namespace std;

bool FunctionDefinition::Equals(const Definition &other) const {
    if (const auto *otherFunctionDefinition = dynamic_cast<const FunctionDefinition *>(&other)) {
        if (this->type_) {
            if (this->type_ != otherFunctionDefinition->type_) {
                return false;
            }
        }
        if (this->id_ != otherFunctionDefinition->id_) {
            return false;
        }
        if (this->parameters_.size() != otherFunctionDefinition->parameters_.size()) {
            return false;
        }
        for (int i = 0; i < this->parameters_.size(); i++) {
            if (!this->parameters_[i]->Equals(*otherFunctionDefinition->parameters_[i])) {
                return false;
            }
        }
        return this->block_->Equals(*otherFunctionDefinition->block_);
    }
    return false;
}

string FunctionDefinition::Print() const {
    string to_return = Definition::Print() + "(";
    for (int i = 0; i < (int) this->parameters_.size() - 1; i++) {
        to_return += this->parameters_[i]->Print() + ",";
    }
    to_return += this->parameters_.empty() ? "" : this->parameters_.back()->Print();
    to_return += ")\n" + this->block_->Print();
    return to_return;
}

const vector<VariableDeclaration *> &FunctionDefinition::GetParameters_() const {
    return parameters_;
}

Block *FunctionDefinition::GetBlock_() const {
    return block_;
}

void FunctionDefinition::AddBlock_(Block *block) {
    this->block_ = block;
}

FunctionDefinition::~FunctionDefinition() {
    //The destruction of the parameter will be done by the Scope defining the function
    this->parameters_.clear();
}

void FunctionDefinition::BuildIR(CFG *cfg) const {
    for (Statement *statement : block_->GetStatements_()) {
        statement->BuildIR(cfg);
    }
}
