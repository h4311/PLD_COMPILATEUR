//
// Created by burca on 3/26/2018.
//
#pragma once

#include <map>
#include <string>
#include <ostream>
#include <vector>
#include <stack>
#include "basic_block.h"
#include "type.h"

class FunctionDefinition;

/** The class for the control flow graph, also includes the symbol table */

class CFG {
public:
    explicit CFG(FunctionDefinition *ast);

    void AddBB(BasicBlock *bb);

    // x86 code generation: could be encapsulated in a processor class in a retargetable compiler
    void GenAsm(std::ostream &o);

    /** helper method: inputs a IR reg or input variable, returns
     * e.g. "-24(%rbp)" for the proper value of !tmp24 */
    std::string IRRegToAsm(std::string reg);

    void GenAsmPrologue(std::ostream &o);

    void GenAsmEpilogue(std::ostream &o);

    // symbol table methods
    void GenerateSymbolTable();

    void AddToSymbolTable(std::string name, CType t, long *size = nullptr);

    std::string CreateNewTempVar(CType t);

    int GetVarIndex(std::string name);

    int GetStackSize();

    // basic block management
    std::string NewBBName();

    std::string GetReturnLabel() const;

    BasicBlock *currentBB_;

    std::stack<std::pair<BasicBlock *, BasicBlock *>> currentLoop_;

    virtual ~CFG();

protected:
    void TransferParameters(std::ostream &o);

    FunctionDefinition *ast_; /**< The AST this CFG comes from */

    std::map<std::string, CType> symbolType_; /**< part of the symbol table  */
    std::map<std::string, int> symbolIndex_; /**< part of the symbol table  */
    int nextFreeSymbolIndex_; /**< to allocate new symbols in the symbol table */
    int nextBBNumber_; /**< just for naming */

    std::map<std::string, BasicBlock *> bbs_; /**< all the basic blocks of this CFG*/
};
