//
// Created by perewoulpy on 24/03/18.
//

#include <iostream>
#include "scope.h"
#include "definition.h"

using namespace std;

Definition *Scope::LookupSymbol(const string &symbolName) const {
    auto it = this->symbolTable_.find(symbolName + this->name_);
    if (it != this->symbolTable_.end()) {
        return it->second;
    }
    // if we are in the global scope and no definition was found we return nullptr
    if (this->IsGlobalScope()) {
        return nullptr;
    }
    // else we need to recurse on the enclosing scope
    return this->enclosingScope_->LookupSymbol(symbolName);
}

// in case that the symbol to be inserted exists already, it's definition is returned
Definition *Scope::InsertSymbol(const std::string &symbolName, Definition *definition) {
    auto insertResult = this->symbolTable_.insert({symbolName, definition});
    if (insertResult.second) {
        return nullptr;
    } else {
        return (*insertResult.first).second;
    }
}

bool Scope::IsGlobalScope() const {
    return this->enclosingScope_ == nullptr;
}

const string &Scope::GetName_() const {
    return name_;
}

void Scope::AddNestedScope(Scope *nestedScope) {
    this->nestedScopes_.push_back(nestedScope);
}

std::vector<VariableDeclaration *> Scope::GetAllVariableDeclarations() const {
    vector<VariableDeclaration *> variableDeclarations;
    for (pair<string, Definition *> symbol : this->symbolTable_) {
        if (auto variableDeclaration = dynamic_cast<VariableDeclaration *>(symbol.second)) {
            variableDeclarations.push_back(variableDeclaration);
        }
    }
    for (Scope *nestedScope : this->nestedScopes_) {
        vector<VariableDeclaration *> nestedVariableDeclarations = nestedScope->GetAllVariableDeclarations();
        variableDeclarations.insert(variableDeclarations.end(), nestedVariableDeclarations.begin(),
                                    nestedVariableDeclarations.end());
    }
    return variableDeclarations;
}

Scope::~Scope() {
    for (auto elem : symbolTable_) {
        delete elem.second;
        elem.second = nullptr;
    }
    symbolTable_.clear();

    for (auto elem : nestedScopes_) {
        delete elem;
        elem = nullptr;
    }
    nestedScopes_.clear();
}