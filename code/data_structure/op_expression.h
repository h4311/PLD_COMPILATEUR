#pragma once

#include "expression.h"
#include "operator.h"

class OpExpression : public Expression {
public:
    explicit OpExpression(Operator *expression_operator)
            : expressionOperator_(expression_operator) {}

    virtual bool Equals(const Statement &other) const = 0;

    std::string Print() const override { return this->expressionOperator_->Print(); }

    Operator *GetExpressionOperator_() const {
        return expressionOperator_;
    }

    ~OpExpression() override { delete this->expressionOperator_; }


protected:
    Operator *expressionOperator_;
};
