#pragma once

#include "scope.h"
#include "function_definition.h"
#include <vector>

class Definition;

class Program : public Scope {
public:
    Program() = default;

    explicit Program(std::vector<Definition *> definitions)
            : definitions_(std::move(definitions)) {}

    bool Equals(const Program &other);

    std::string Print() const;

    ~Program();

    void AddDefinition(Definition *definition);

    void InsertStandardDefinitions();

    std::vector<Definition *> GetDefinitions_() const;

    std::vector<FunctionDefinition *> GetFunctionDefinitions() const;

    void BuildIR();

    void GenerateAssembler(std::ostream &o) const;

private:
    std::vector<Definition *> definitions_;
    std::vector<CFG *> cfgs;
};
