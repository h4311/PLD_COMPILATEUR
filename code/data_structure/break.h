#pragma once

#include "statement.h"

class Break : public Statement {
public:
    Break() = default;

    bool Equals(const Statement &other) const override { return dynamic_cast<const Break *>(&other) != nullptr; }

    std::string Print() const override { return "break;"; }

    ~Break() override = default;

    std::string BuildIR(CFG *cfg) const override {
        cfg->currentBB_->SetExitTrue_(cfg->currentLoop_.top().second);
        cfg->currentBB_->SetExitFalse_(nullptr);
        cfg->currentBB_->LockExits();
        return "nope";
    }

    bool IsUseful() const override {
        return true;
    }
};
