//
// Created by Horia on 3/13/2018.
//
#include <iostream>
#include "binary_op_expression.h"

using namespace std;

bool BinaryOpExpression::Equals(const Statement &other) const {
    if (const auto *otherBinaryOpExpression = dynamic_cast<const BinaryOpExpression *>(&other)) {
        return this->expression1_->Equals(*otherBinaryOpExpression->expression1_)
               && this->expression2_->Equals(*otherBinaryOpExpression->expression2_)
               && this->expressionOperator_->Equals(*otherBinaryOpExpression->expressionOperator_);
    }
    return false;
}

string BinaryOpExpression::Print() const {
    return this->expression1_->Print() + OpExpression::Print() + this->expression2_->Print();
}

Expression *BinaryOpExpression::GetExpression1_() const {
    return expression1_;
}

Expression *BinaryOpExpression::GetExpression2_() const {
    return expression2_;
}

BinaryOpExpression::~BinaryOpExpression() {
    delete this->expression1_;
    delete this->expression2_;
}

bool BinaryOpExpression::HasSymbol() const {
    if (expression1_->HasSymbol()) {
        return true;
    }
    return expression2_->HasSymbol();
}

bool BinaryOpExpression::IsUseful() const {
    if (expression1_->IsUseful()) {
        return true;
    }
    return expression2_->IsUseful();
}

long *BinaryOpExpression::EvaluateExpression() {
    if (expression1_->EvaluateExpression() == nullptr || expression2_->EvaluateExpression() == nullptr)
        return nullptr;
    auto op = static_cast<int>(*expressionOperator_);
    long expression1 = *(expression1_->EvaluateExpression());
    long expression2 = *(expression2_->EvaluateExpression());
    long toReturn;
    switch (op) {
        case ADDITION:
            toReturn = expression1 + expression2;
            break;
        case SUBTRACTION:
            toReturn = expression1 - expression2;
            break;
        case MULTIPLICATION:
            toReturn = expression1 * expression2;
            break;
        case DIVISION:
            toReturn = expression1 / expression2;
            break;
        case REMAINDER:
            toReturn = expression1 % expression2;
            break;
        case LESS:
            toReturn = expression1 < expression2;
            break;
        case LESSEQUAL:
            toReturn = expression1 <= expression2;
            break;
        case MORE:
            toReturn = expression1 > expression2;
            break;
        case MOREEQUAL:
            toReturn = expression1 >= expression2;
            break;
        case EQUAL:
            toReturn = expression1 == expression2;
            break;
        case NOTEQUAL:
            toReturn = expression1 != expression2;
            break;
        case AND:
            toReturn = expression1 && expression2;
            break;
        case OR:
            toReturn = expression1 || expression2;
            break;
        case BITWISE_AND:
            toReturn = expression1 & expression2;
            break;
        case BITWISE_OR:
            toReturn = expression1 | expression2;
            break;
        case BITWISE_XOR:
            toReturn = expression1 ^ expression2;
            break;
        case BITWISE_LSHIFT:
            toReturn = expression1 << expression2;
            break;
        case BITWISE_RSHIFT:
            toReturn = expression1 >> expression2;
            break;
        case COMMA:
            toReturn = expression1;
            break;
        default:
            return nullptr;
    }
    return new long(toReturn);
}

string BinaryOpExpression::BuildIR(CFG *cfg) const {
    string var1 = expression1_->BuildIR(cfg);
    string var2 = expression2_->BuildIR(cfg);

    string res = cfg->CreateNewTempVar(GetType_());
    auto op = static_cast<int>(*expressionOperator_);
    switch (op) {
        case ADDITION:
            cfg->currentBB_->AddIRInstr(IRInstruction::add, GetType_(), {res, var1, var2});
            break;
        case SUBTRACTION:
            cfg->currentBB_->AddIRInstr(IRInstruction::sub, GetType_(), {res, var1, var2});
            break;
        case MULTIPLICATION:
            cfg->currentBB_->AddIRInstr(IRInstruction::mul, GetType_(), {res, var1, var2});
            break;
        case DIVISION:
            cfg->currentBB_->AddIRInstr(IRInstruction::div, GetType_(), {res, var1, var2});
            break;
        case REMAINDER:
            cfg->currentBB_->AddIRInstr(IRInstruction::rem, GetType_(), {res, var1, var2});
            break;
        case BITWISE_LSHIFT:
            cfg->currentBB_->AddIRInstr(IRInstruction::shl, GetType_(), {res, var1, var2});
            break;
        case BITWISE_RSHIFT:
            cfg->currentBB_->AddIRInstr(IRInstruction::shr, GetType_(), {res, var1, var2});
            break;
        case LESS:
            cfg->currentBB_->AddIRInstr(IRInstruction::cmp_lt, GetType_(), {res, var1, var2});
            break;
        case LESSEQUAL:
            cfg->currentBB_->AddIRInstr(IRInstruction::cmp_le, GetType_(), {res, var1, var2});
            break;
        case MORE:
            cfg->currentBB_->AddIRInstr(IRInstruction::cmp_lt, GetType_(), {res, var2, var1});
            break;
        case MOREEQUAL:
            cfg->currentBB_->AddIRInstr(IRInstruction::cmp_le, GetType_(), {res, var2, var1});
            break;
        case EQUAL:
            cfg->currentBB_->AddIRInstr(IRInstruction::cmp_eq, GetType_(), {res, var1, var2});
            break;
        case NOTEQUAL:
            cfg->currentBB_->AddIRInstr(IRInstruction::cmp_ne, GetType_(), {res, var1, var2});
            break;
        case BITWISE_XOR:
            cfg->currentBB_->AddIRInstr(IRInstruction::bit_xor, GetType_(), {res, var1, var2});
            break;
        case BITWISE_AND:
        case AND:
            cfg->currentBB_->AddIRInstr(IRInstruction::bit_and, GetType_(), {res, var1, var2});
            break;
        case BITWISE_OR:
        case OR:
            cfg->currentBB_->AddIRInstr(IRInstruction::bit_or, GetType_(), {res, var1, var2});
            break;
        case COMMA:
            return var2;
        default:
            break;
    }

    return res;
}
