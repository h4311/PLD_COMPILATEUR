#pragma once

#include "expression.h"

class If : public Statement {
public:
    If(Expression *condition, Statement *if_statement, Statement *else_statement)
            : condition_(condition), ifStatement_(if_statement), elseStatement_(else_statement) {}

    If(Expression *condition, Statement *if_statement)
            : condition_(condition), ifStatement_(if_statement), elseStatement_(nullptr) {}

    bool Equals(const Statement &other) const override;

    std::string Print() const override;

    Expression *GetCondition_() const;

    Statement *GetIfStatement_() const;

    Statement *GetElseStatement_() const;

    ~If();

    bool IsUseful() const override;

    std::string BuildIR(CFG *cfg) const override;

private:
    Expression *condition_;
    Statement *ifStatement_;
    Statement *elseStatement_;
};
