#pragma once

#include "expression.h"
#include "definition.h"
#include <string>
#include <utility>
#include <vector>

class FunctionCall : public Expression {
public:
    FunctionCall(std::string id, std::vector<Expression *> arguments)
            : id_(std::move(id)), arguments_(std::move(arguments)) {}

    bool Equals(const Statement &other) const override;

    std::string Print() const override;

    const std::string &GetId_() const;

    const std::vector<Expression *> &GetArguments_() const;

    ~FunctionCall() override;

    std::string BuildIR(CFG *cfg) const override;

    bool HasSymbol() const override;

    bool IsUseful() const;

    long *EvaluateExpression() override;

private:
    std::string id_;
    std::vector<Expression *> arguments_;
};
