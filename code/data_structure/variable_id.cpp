//
// Created by Horia on 3/12/2018.
//
#include "variable_id.h"

using namespace std;

bool VariableId::Equals(const Statement &other) const {
    if (const auto *otherVariableId = dynamic_cast<const VariableId *>(&other)) {
        if (this->id_ != otherVariableId->id_) {
            return false;
        }
        if (this->arraySubscription_) {
            if (!this->index_->Equals(*otherVariableId->index_)) {
                return false;
            }
        }
        return true;
    }
    return false;
}

string VariableId::Print() const {
    string to_return = this->id_;
    if (arraySubscription_) {
        to_return += "[" + this->index_->Print() + "]";
    }
    return to_return;
}

const string &VariableId::GetId_() const {
    return id_;
}

Expression *VariableId::GetIndex_() const {
    return index_;
}

bool VariableId::IsArraySubscription_() const {
    return arraySubscription_;
}

VariableId::~VariableId() {
    delete this->index_;
}

bool VariableId::HasSymbol() const {
    return true;
}

bool VariableId::IsUseful() const {
    return false;
}

long *VariableId::EvaluateExpression() {
    return nullptr;
}

string VariableId::BuildLValue(CFG *cfg) const {
    int offset = -cfg->GetVarIndex(id_);
    string var = cfg->CreateNewTempVar(INT64_T);
    cfg->currentBB_->AddIRInstr(IRInstruction::ldconst, INT64_T, {var, to_string(offset)});
    cfg->currentBB_->AddIRInstr(IRInstruction::add, INT64_T, {var, "!bp", var});
    if (arraySubscription_) {
        string index = index_->BuildIR(cfg);
        string arrayElemSize = cfg->CreateNewTempVar(INT64_T);
        cfg->currentBB_->AddIRInstr(IRInstruction::ldconst, INT64_T,
                                    {arrayElemSize, to_string(8/*variableId_->GetIndex_()->GetType_()*/)});
        string temp1 = cfg->CreateNewTempVar(INT64_T);
        cfg->currentBB_->AddIRInstr(IRInstruction::mul, INT64_T,
                                    {temp1, arrayElemSize, index});
        cfg->currentBB_->AddIRInstr(IRInstruction::add, INT64_T, {var, temp1, var});
    }
    //All types above are INT64_T because we play with memory address
    return var;
}

string VariableId::BuildIR(CFG *cfg) const {
    if (!arraySubscription_)
        return id_;
    int offset = -cfg->GetVarIndex(GetId_());
    string var = cfg->CreateNewTempVar(INT64_T);
    cfg->currentBB_->AddIRInstr(IRInstruction::ldconst, INT64_T, {var, to_string(offset)});
    cfg->currentBB_->AddIRInstr(IRInstruction::add, INT64_T, {var, "!bp", var});
    string index = GetIndex_()->BuildIR(cfg);
    string arrayElemSize = cfg->CreateNewTempVar(INT64_T);
    cfg->currentBB_->AddIRInstr(IRInstruction::ldconst, INT64_T,
                                {arrayElemSize, to_string(8/*variableId_->GetIndex_()->GetType_()*/)});
    string temp1 = cfg->CreateNewTempVar(INT64_T);
    cfg->currentBB_->AddIRInstr(IRInstruction::mul, INT64_T,
                                {temp1, arrayElemSize, index});
    cfg->currentBB_->AddIRInstr(IRInstruction::add, INT64_T, {var, temp1, var});
    //All types above are INT64_T because we play with memory address

    string dest = cfg->CreateNewTempVar(GetType_());
    cfg->currentBB_->AddIRInstr(IRInstruction::rmem, GetType_(), {dest, var});

    return dest;
}

void VariableId::Rename(std::string newName) {
    this->id_ = newName;
}
