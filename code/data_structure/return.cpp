//
// Created by Horia on 3/13/2018.
//
#include "return.h"

using namespace std;

bool Return::Equals(const Statement &other) const {
    if (const auto *otherReturn = dynamic_cast<const Return *>(&other)) {
        if (this->value_) {
            if (!this->value_->Equals(*otherReturn->value_)) {
                return false;
            }
        }
        return true;
    }
    return false;
}

string Return::Print() const {
    if (this->value_) {
        return "return " + this->value_->Print();
    }
    return "return;";
}

Expression *Return::GetValue_() const {
    return value_;
}

Return::~Return() {
    if (value_ != nullptr)
        delete value_;
}

string Return::BuildIR(CFG *cfg) const {
    cfg->currentBB_->SetExitTrue_(nullptr);
    cfg->currentBB_->SetExitFalse_(nullptr);

    if (value_ != nullptr) {
        string ret = value_->BuildIR(cfg);

        cfg->currentBB_->AddIRInstr(IRInstruction::copy, value_->GetType_(), {"retvalue", ret});
    }
    return "nope";
}

bool Return::IsUseful() const {
    return true;
}

