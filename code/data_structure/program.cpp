//
// Created by Horia on 3/11/2018.
//
#include "program.h"
#include "definition.h"

using namespace std;

bool Program::Equals(const Program &other) {
    if (this->definitions_.size() != other.definitions_.size()) {
        return false;
    }
    for (int i = 0; i < this->definitions_.size(); i++) {
        if (!this->definitions_[i]->Equals(*other.definitions_[i])) {
            return false;
        }
    }
    return true;
}

string Program::Print() const {
    string to_return;
    for (Definition *definition: this->definitions_) {
        to_return += definition->Print() + "\n";
    }
    return to_return;
}

void Program::AddDefinition(Definition *definition) {
    this->definitions_.push_back(definition);
}

void Program::InsertStandardDefinitions() {
    // putchar
    FunctionDefinition *putcharFunction = new FunctionDefinition(VOID, "putchar",
                                                                 {new VariableDeclaration(CHAR, "char", nullptr)});
    this->InsertSymbol("putchar", putcharFunction);
    // getchar
    FunctionDefinition *getcharFunction = new FunctionDefinition(CHAR, "getchar", {});
    this->InsertSymbol("getchar", getcharFunction);
}

vector<Definition *> Program::GetDefinitions_() const {
    return this->definitions_;
}

std::vector<FunctionDefinition *> Program::GetFunctionDefinitions() const {
    vector<FunctionDefinition *> functionDefinitions;
    for (Definition *def : definitions_) {
        auto *fDef = dynamic_cast<FunctionDefinition *>(def);
        if (fDef) {
            functionDefinitions.push_back(fDef);
        }
    }
    return functionDefinitions;
}

void Program::BuildIR() {
    for (Definition *definition : definitions_) {
        if (auto function = dynamic_cast<FunctionDefinition *>(definition)) {
            auto cfg = new CFG(function);
            this->cfgs.push_back(cfg);
            function->BuildIR(cfg);
        } else {
            // TODO implement here IR of global variables
        }
    }
}

void Program::GenerateAssembler(std::ostream &o) const {
    for (CFG *cfg : cfgs) {
        cfg->GenAsm(o);
    }
}

Program::~Program() {
    // definitions are delete by the scope class
    for (CFG *cfg : cfgs) {
        delete cfg;
    }
}
