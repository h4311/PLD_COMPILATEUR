//
// Created by Horia on 3/14/2018.
//
#include "unary_op_assignment.h"

using namespace std;

bool UnaryOpAssignment::Equals(const Statement &other) const {
    if (const auto *otherUnaryOpAssignment = dynamic_cast<const UnaryOpAssignment *>(&other)) {
        return this->variableId_->Equals(*otherUnaryOpAssignment->variableId_)
               && this->expressionOperator_->Equals(*otherUnaryOpAssignment->expressionOperator_);
    }
    return false;
}

string UnaryOpAssignment::Print() const {
    switch (static_cast<int>(*this->expressionOperator_)) {
        case SUFFIX_DECREMENT:
        case SUFFIX_INCREMENT:
            return this->variableId_->Print() + this->expressionOperator_->Print();
        default:
            return this->expressionOperator_->Print() + this->variableId_->Print();
    }
}

VariableId *UnaryOpAssignment::GetVariableId_() const {
    return variableId_;
}

UnaryOpAssignment::~UnaryOpAssignment() {
    delete this->variableId_;
}

bool UnaryOpAssignment::HasSymbol() const {
    return true;
}

long *UnaryOpAssignment::EvaluateExpression() {
    return nullptr;
}

bool UnaryOpAssignment::IsUseful() const {
    return true;
}

string UnaryOpAssignment::BuildIR(CFG *cfg) const {
    string lvalue = variableId_->BuildLValue(cfg);
    string lvalueAsRValue = variableId_->BuildIR(cfg);
    switch (static_cast<int>(*expressionOperator_)) {
        case SUFFIX_INCREMENT: {
            string temp1 = cfg->CreateNewTempVar(type_);
            cfg->currentBB_->AddIRInstr(IRInstruction::rmem, type_, {temp1, lvalue});
            cfg->currentBB_->AddIRInstr(IRInstruction::inc, type_, {lvalueAsRValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, type_, {lvalue, lvalueAsRValue});
            return temp1;
        }
        case SUFFIX_DECREMENT: {
            string temp2 = cfg->CreateNewTempVar(type_);
            cfg->currentBB_->AddIRInstr(IRInstruction::rmem, type_, {temp2, lvalue});
            cfg->currentBB_->AddIRInstr(IRInstruction::dec, type_, {lvalueAsRValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, type_, {lvalue, lvalueAsRValue});
            return temp2;
        }
        case PREFIX_INCREMENT:
            cfg->currentBB_->AddIRInstr(IRInstruction::inc, type_, {lvalueAsRValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, type_, {lvalue, lvalueAsRValue});
            break;
        case PREFIX_DECREMENT:
            cfg->currentBB_->AddIRInstr(IRInstruction::dec, type_, {lvalueAsRValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, type_, {lvalue, lvalueAsRValue});
            break;
        default:
            break;
    }
    return variableId_->GetId_();
}
