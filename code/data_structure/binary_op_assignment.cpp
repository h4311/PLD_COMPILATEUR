//
// Created by Horia on 3/12/2018.
//
#include "binary_op_assignment.h"

using namespace std;

bool BinaryOpAssignment::Equals(const Statement &other) const {
    if (const auto *otherBinaryOpAssignment = dynamic_cast<const BinaryOpAssignment *>(&other)) {
        return this->expression_->Equals(*otherBinaryOpAssignment->expression_)
               && this->variableId_->Equals(*otherBinaryOpAssignment->variableId_)
               && this->expressionOperator_->Equals(*otherBinaryOpAssignment->expressionOperator_);
    }
    return false;
}

string BinaryOpAssignment::Print() const {
    return this->variableId_->Print() + OpExpression::Print() + this->expression_->Print();
}

VariableId *BinaryOpAssignment::GetVariableId_() const {
    return variableId_;
}

Expression *BinaryOpAssignment::GetExpression_() const {
    return expression_;
}

BinaryOpAssignment::~BinaryOpAssignment() {
    delete this->variableId_;
    delete this->expression_;
}

bool BinaryOpAssignment::HasSymbol() const {
    return true;
}

long *BinaryOpAssignment::EvaluateExpression() {
    return nullptr;
}

bool BinaryOpAssignment::IsUseful() const {
    return true;
}

string BinaryOpAssignment::BuildIR(CFG *cfg) const {
    string rValue = expression_->BuildIR(cfg);
    string lValue = variableId_->BuildLValue(cfg);
    string lValueAsRValue = variableId_->BuildIR(cfg);
    auto op = static_cast<int>(*expressionOperator_);
    switch (op) {
        case ASSIGNMENT:
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        case ASSIGNMENT_SUM:
            cfg->currentBB_->AddIRInstr(IRInstruction::add, variableId_->GetType_(), {rValue, lValueAsRValue, rValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        case ASSIGNMENT_DIFFERENCE:
            cfg->currentBB_->AddIRInstr(IRInstruction::sub, variableId_->GetType_(), {rValue, lValueAsRValue, rValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        case ASSIGNMENT_PRODUCT:
            cfg->currentBB_->AddIRInstr(IRInstruction::mul, variableId_->GetType_(), {rValue, lValueAsRValue, rValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        case ASSIGNMENT_QUOTIENT:
            cfg->currentBB_->AddIRInstr(IRInstruction::div, variableId_->GetType_(), {rValue, lValueAsRValue, rValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        case ASSIGNMENT_REMAINDER:
            cfg->currentBB_->AddIRInstr(IRInstruction::rem, variableId_->GetType_(), {rValue, lValueAsRValue, rValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        case ASSIGNMENT_BITWISE_LSHIFT:
            cfg->currentBB_->AddIRInstr(IRInstruction::shl, variableId_->GetType_(), {rValue, lValueAsRValue, rValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        case ASSIGNMENT_BITWISE_RSHIFT:
            cfg->currentBB_->AddIRInstr(IRInstruction::shr, variableId_->GetType_(), {rValue, lValueAsRValue, rValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        case ASSIGNMENT_BITWISE_AND:
            cfg->currentBB_->AddIRInstr(IRInstruction::bit_and, variableId_->GetType_(),
                                        {rValue, lValueAsRValue, rValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        case ASSIGNMENT_BITWISE_OR:
            cfg->currentBB_->AddIRInstr(IRInstruction::bit_or, variableId_->GetType_(),
                                        {rValue, lValueAsRValue, rValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        case ASSIGNMENT_BITWISE_XOR:
            cfg->currentBB_->AddIRInstr(IRInstruction::bit_xor, variableId_->GetType_(),
                                        {rValue, lValueAsRValue, rValue});
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, variableId_->GetType_(), {lValue, rValue});
            break;
        default:
            break;
    }
    return variableId_->GetId_();
}

