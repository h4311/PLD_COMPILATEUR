//
// Created by Horia on 3/13/2018.
//
#include "if.h"
#include "block.h"

using namespace std;

bool If::Equals(const Statement &other) const {
    if (const auto *otherIf = dynamic_cast<const If *>(&other)) {
        if (!this->condition_->Equals(*otherIf->condition_) ||
            !this->ifStatement_->Equals(*otherIf->ifStatement_)) {
            return false;
        }
        if (this->elseStatement_) {
            if (!this->elseStatement_->Equals(*otherIf->elseStatement_)) {
                return false;
            }
        }
        return true;
    }
    return false;
}

string If::Print() const {
    string to_return = "if(" + this->condition_->Print() + ")\n" + this->ifStatement_->Print();
    if (this->elseStatement_ != nullptr) {
        to_return += "\nelse\n" + this->elseStatement_->Print();
    }
    return to_return;
}

Expression *If::GetCondition_() const {
    return this->condition_;
}

Statement *If::GetIfStatement_() const {
    return this->ifStatement_;
}

Statement *If::GetElseStatement_() const {
    return this->elseStatement_;
}

If::~If() {
    delete this->condition_;

    //Blocks will be destroyed by their enclosing scope
    auto *bIf = dynamic_cast<Block *>(ifStatement_);
    auto *bEl = dynamic_cast<Block *>(elseStatement_);
    if (!bIf)
        delete this->ifStatement_;
    if (!bEl)
        delete this->elseStatement_;
}

string If::BuildIR(CFG *cfg) const {
    // construct test block
    string res = condition_->BuildIR(cfg);
    BasicBlock *testBB = cfg->currentBB_;
    testBB->SetConditionVar_(res);
    //construct true block
    BasicBlock *trueBB = new BasicBlock(cfg, cfg->NewBBName());
    cfg->AddBB(trueBB);
    cfg->currentBB_ = trueBB;
    ifStatement_->BuildIR(cfg);
    // construct false block
    if (elseStatement_) {
        BasicBlock *falseBB = new BasicBlock(cfg, cfg->NewBBName());
        cfg->AddBB(falseBB);
        cfg->currentBB_ = falseBB;
        elseStatement_->BuildIR(cfg);
        BasicBlock *nextBB = new BasicBlock(cfg, cfg->NewBBName());
        cfg->AddBB(nextBB);
        nextBB->SetExitTrue_(testBB->GetExitTrue_());
        nextBB->SetExitFalse_(testBB->GetExitFalse_());
        testBB->SetExitTrue_(trueBB);
        testBB->SetExitFalse_(falseBB);
        trueBB->SetExitTrue_(nextBB);
        trueBB->SetExitFalse_(nullptr);
        falseBB->SetExitTrue_(nextBB);
        falseBB->SetExitFalse_(nullptr);
        cfg->currentBB_ = nextBB;
    } else {
        BasicBlock *nextBB = new BasicBlock(cfg, cfg->NewBBName());
        cfg->AddBB(nextBB);
        nextBB->SetExitTrue_(testBB->GetExitTrue_());
        nextBB->SetExitFalse_(testBB->GetExitFalse_());
        testBB->SetExitTrue_(trueBB);
        testBB->SetExitFalse_(nextBB);
        trueBB->SetExitTrue_(nextBB);
        trueBB->SetExitFalse_(nullptr);
        cfg->currentBB_ = nextBB;
    }

    return res;
}

bool If::IsUseful() const {
    if (condition_->IsUseful())
        return true;
    if (ifStatement_->IsUseful())
        return true;
    if (elseStatement_ != nullptr)
        if (elseStatement_->IsUseful())
            return true;

    return false;
}
