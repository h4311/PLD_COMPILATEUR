//
// Created by Horia on 3/13/2018.
//
#include "while.h"
#include "block.h"

using namespace std;

bool While::Equals(const Statement &other) const {
    if (const auto *otherWhile = dynamic_cast<const While *>(&other)) {
        return this->condition_->Equals(*otherWhile->condition_) && this->statement_->Equals(*otherWhile->statement_);
    }
    return false;
}

string While::Print() const {
    return "while(" + this->condition_->Print() + ")\n" + this->statement_->Print();
}

Expression *While::GetCondition_() const {
    return this->condition_;
}

Statement *While::GetStatement_() const {
    return this->statement_;
}

While::~While() {
    delete this->condition_;

    //Block will be destroyed by its enclosing scope
    auto *b = dynamic_cast<Block *>(statement_);
    if (!b)
        delete this->statement_;
}

string While::BuildIR(CFG *cfg) const {
    BasicBlock *prevBB = cfg->currentBB_;

    // begin constructing next block
    BasicBlock *nextBB = new BasicBlock(cfg, cfg->NewBBName());
    cfg->AddBB(nextBB);

    // construct test block
    BasicBlock *testBB = new BasicBlock(cfg, prevBB->GetLabel_() + "_testStmt");
    cfg->AddBB(testBB);
    cfg->currentBB_ = testBB;
    // push current loop on loop stack so that we can use it for break and continue statements
    cfg->currentLoop_.push(make_pair(testBB, nextBB));
    string res = condition_->BuildIR(cfg);
    testBB->SetConditionVar_(res);

    // construct loop block
    BasicBlock *loopBB = new BasicBlock(cfg, prevBB->GetLabel_() + "_bodyStmt");
    cfg->AddBB(loopBB);
    cfg->currentBB_ = loopBB;
    loopBB->SetExitTrue_(testBB);
    loopBB->SetExitFalse_(nullptr);
    statement_->BuildIR(cfg);

    // construct next block
    nextBB->SetExitTrue_(prevBB->GetExitTrue_());
    nextBB->SetExitFalse_(prevBB->GetExitFalse_());
    prevBB->SetExitTrue_(testBB);
    prevBB->SetExitFalse_(nullptr);
    testBB->SetExitTrue_(loopBB);
    testBB->SetExitFalse_(nextBB);

    cfg->currentBB_ = nextBB;
    // pop current loop after we finished building it
    cfg->currentLoop_.pop();
    return res;
}

bool While::IsUseful() const {
    if (condition_->IsUseful())
        return true;
    if (statement_->IsUseful())
        return true;

    return false;
}
