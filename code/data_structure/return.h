#pragma once

#include "expression.h"

class Return : public Statement {
public:
    Return() = default;

    explicit Return(Expression *value)
            : value_(value) {}

    bool Equals(const Statement &other) const override;

    std::string Print() const override;

    Expression *GetValue_() const;

    std::string BuildIR(CFG *cfg) const override;

    ~Return();

    bool IsUseful() const override;

private:
    Expression *value_ = nullptr;
};
