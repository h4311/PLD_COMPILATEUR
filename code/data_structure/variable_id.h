#pragma once

#include "expression.h"
#include "definition.h"
#include <string>
#include <utility>

class VariableId : public Expression {
public:
    explicit VariableId(std::string id)
            : id_(std::move(id)), index_(nullptr), arraySubscription_(false) {}

    VariableId(std::string id, Expression *index)
            : id_(std::move(id)), index_(index), arraySubscription_(true) {}

    bool Equals(const Statement &other) const override;

    std::string Print() const override;

    const std::string &GetId_() const;

    void Rename(std::string newName);

    Expression *GetIndex_() const;

    bool IsArraySubscription_() const;

    ~VariableId() override;

    bool HasSymbol() const override;

    bool IsUseful() const override;

    long *EvaluateExpression() override;

    std::string BuildLValue(CFG *cfg) const;

    std::string BuildIR(CFG *cfg) const override;

private:
    std::string id_;
    Expression *index_;
    bool arraySubscription_;
};
