#pragma once

#include "definition.h"
#include "expression.h"
#include <string>
#include <vector>

class VariableDeclaration : public Statement, public Definition {
public:
    VariableDeclaration(CType type, const std::string &id, Expression *value);

    VariableDeclaration(CType type, const std::string &id, std::vector<Expression *> values, Expression *array_size);

    void InitVariable(Expression *value);

    void InitVariable(Expression *value, unsigned long index);

    std::string Print() const;

    const bool GetArray_() const;

    const std::vector<Expression *> &GetValues_() const;

    Expression *GetArraySize_() const;

    bool Equals(const Definition &other) const override;

    bool Equals(const Statement &other) const override;

    bool Equals(const VariableDeclaration &other) const;

    bool IsUseful() const override;

    bool HasSymbolInExpression();

    ~VariableDeclaration();

    std::string BuildIR(CFG *cfg) const override;

    long *EvaluateSize();

private:
    const bool array_;
    std::vector<Expression *> values_;
    Expression *arraySize_ = nullptr;
};
