//
// Created by Horia on 3/11/2018.
//
#include <iostream>
#include "block.h"
#include "data_structure.h"

using namespace std;

bool Block::Equals(const Statement &other) const {
    if (const auto *otherBlock = dynamic_cast<const Block *>(&other)) {
        if (this->statements_.size() != otherBlock->statements_.size()) {
            return false;
        }
        for (int i = 0; i < this->statements_.size(); i++) {
            if (!this->statements_[i]->Equals(*otherBlock->statements_[i])) {
                return false;
            }
        }
        return true;
    }
    return false;
}

string Block::Print() const {
    string to_return = "{\n";
    for (Statement *statement : this->statements_) {
        to_return += statement->Print() + "\n";
    }
    to_return += "}";
    return to_return;
}

void Block::AddStatement(Statement *statement) {
    // TODO implement optimization in a separate class, because optimization is optional
    try {
        if (statement->IsUseful()) {
            this->statements_.push_back(statement);
        } else {
            cout << "ignored this statement: " << statement->Print() << endl;
            delete statement;
        }
    } catch (exception &e) {
        cout << "failed dynamic cast" << endl;
    }
}

vector<Statement *> Block::GetStatements_() const {
    return this->statements_;
}

Block::~Block() {
    for (auto *statement : statements_) {
        auto *var = dynamic_cast<VariableDeclaration *>(statement);
        if (var) // All the definition will be destroyed by the scope : Bad practice !!
            continue;
        delete statement;
    }
    this->statements_.clear();
}

string Block::BuildIR(CFG *cfg) const {
    for (Statement *statement : statements_) {
        statement->BuildIR(cfg);
    }
    return "nope";
}

bool Block::IsUseful() const {
    for (auto s : statements_) {
        if (s->IsUseful())
            return true;
    }
    return false;
}
