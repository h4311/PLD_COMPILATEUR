//
// Created by perewoulpy on 27/03/18.
//
#include <iostream>
#include "cfg.h"
#include "function_definition.h"
#include "if.h"

CFG::CFG(FunctionDefinition *ast) {
    ast_ = ast;
    nextBBNumber_ = 0;
    nextFreeSymbolIndex_ = 0;
    GenerateSymbolTable();
    currentBB_ = new BasicBlock(this, NewBBName());
    AddBB(currentBB_);
}

void CFG::GenAsm(std::ostream &o) {
    GenAsmPrologue(o);

    for (auto elem_bb : bbs_) {
        elem_bb.second->GenAsm(o);
    }

    GenAsmEpilogue(o);
}

void CFG::TransferParameters(std::ostream &o) {
    auto params = ast_->GetParameters_();

    if (params.size() > 0) o << "\tmovq\t%rdi, " << IRRegToAsm(params[0]->GetId_()) << std::endl;
    if (params.size() > 1) o << "\tmovq\t%rsi, " << IRRegToAsm(params[1]->GetId_()) << std::endl;
    if (params.size() > 2) o << "\tmovq\t%rdx, " << IRRegToAsm(params[2]->GetId_()) << std::endl;
    if (params.size() > 3) o << "\tmovq\t%rcx, " << IRRegToAsm(params[3]->GetId_()) << std::endl;
    if (params.size() > 4) o << "\tmovq\t%r8, " << IRRegToAsm(params[4]->GetId_()) << std::endl;
    if (params.size() > 5) o << "\tmovq\t%r9, " << IRRegToAsm(params[5]->GetId_()) << std::endl;

    if (params.size() > 6) {
        std::cerr << "Too many arguments : Not implemented" << std::endl;
    }
}

void CFG::GenAsmPrologue(std::ostream &o) {
    o << ast_->GetId_() << ":" << std::endl;
    o << "\tpushq\t%rbp" << std::endl;
    o << "\tmovq\t%rsp, %rbp" << std::endl;
    if (GetStackSize()) {
        //TODO : rsp or rbp can only be multiple of 8
        //it seems that when they are a stack, there is 8 octets that are reserved (i do not know why)
        o << "\tsubq\t$" << GetStackSize() + 8 << ", %rsp" << std::endl;
    }
    TransferParameters(o);
}

void CFG::GenAsmEpilogue(std::ostream &o) {
    o << ast_->GetId_() + "_ret" << ":" << std::endl;
    if (ast_->GetType_() != VOID)
        o << "\tmovq\t" << IRRegToAsm("retvalue") << ", %rax" << std::endl;
    o << "\tleave" << std::endl;
    o << "\tretq" << std::endl;
    o << std::endl;
}

int CFG::GetStackSize() {
    return nextFreeSymbolIndex_;
}

void CFG::GenerateSymbolTable() {
    if (ast_->GetType_() != VOID)
        AddToSymbolTable("retvalue", ast_->GetType_());

    Block *block = ast_->GetBlock_();
    for (VariableDeclaration *variableDeclaration : block->GetAllVariableDeclarations()) {
        if (variableDeclaration->IsUsed_())
            AddToSymbolTable(variableDeclaration->GetId_(), variableDeclaration->GetType_(),
                             variableDeclaration->EvaluateSize());
    }
}

void CFG::AddToSymbolTable(std::string name, CType t, long *size /*=nullptr*/) {
    //TODO : optimize the size of the used stack per variable
    long lsize = 1;
    if (size != nullptr) {
        lsize = *size;
    }
    nextFreeSymbolIndex_ += 8 * lsize;
    symbolIndex_[name] = nextFreeSymbolIndex_;
    symbolType_[name] = t;
}

int CFG::GetVarIndex(std::string name) {
    return symbolIndex_[name];
}

std::string CFG::CreateNewTempVar(CType t) {
    //TODO : optimize the size of the used stack per tmp variable
    std::string name = "!tmp" + std::to_string(nextFreeSymbolIndex_ + 8);
    AddToSymbolTable(name, t);
    return name;
}

std::string CFG::NewBBName() {
    return ast_->GetId_() + "_" + std::to_string(nextBBNumber_++);
}

std::string CFG::IRRegToAsm(std::string reg) {
    if (reg == "!bp") {
        return "%rbp";
    }
    return "-" + std::to_string(symbolIndex_[reg]) + "(%rbp)";
}

void CFG::AddBB(BasicBlock *bb) {
    bbs_[bb->GetLabel_()] = bb;
}

std::string CFG::GetReturnLabel() const {
    return ast_->GetId_() + "_ret";
}

CFG::~CFG() {
    for (auto bb : bbs_) {
        delete bb.second;
    }
    bbs_.clear();
}
