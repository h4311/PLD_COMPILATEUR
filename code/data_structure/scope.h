//
// Created by perewoulpy on 24/03/18.
//
#pragma once

#include <unordered_map>
#include <utility>
#include "variable_declaration.h"

class Definition;

class Scope {
public:
    Scope() = default;

    Scope(Scope *enclosingScope, const std::string &name)
            : enclosingScope_(enclosingScope), name_(std::move(name)) {}

    Definition *LookupSymbol(const std::string &symbolName) const;

    Definition *InsertSymbol(const std::string &symbolName, Definition *definition);

    bool IsGlobalScope() const;

    const std::string &GetName_() const;

    void AddNestedScope(Scope *nestedScope);

    std::vector<VariableDeclaration *> GetAllVariableDeclarations() const;

    virtual ~Scope();

protected:
    std::unordered_map<std::string, Definition *> symbolTable_;
    Scope *enclosingScope_ = nullptr;
    std::string name_;
    std::vector<Scope *> nestedScopes_;
};
