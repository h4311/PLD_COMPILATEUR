#pragma once

#include "op_expression.h"
#include "variable_id.h"

class BinaryOpAssignment : public OpExpression {
public:
    BinaryOpAssignment(VariableId *variableId, Expression *expression, Operator *expressionOperator)
            : OpExpression(expressionOperator), variableId_(variableId), expression_(expression) {}

    bool Equals(const Statement &other) const override;

    std::string Print() const override;

    VariableId *GetVariableId_() const;

    Expression *GetExpression_() const;

    ~BinaryOpAssignment() override;

    bool HasSymbol() const override;

    bool IsUseful() const override;

    long *EvaluateExpression() override;

    std::string BuildIR(CFG *cfg) const override;

private:
    VariableId *variableId_;
    Expression *expression_;
};
