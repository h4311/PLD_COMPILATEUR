#pragma once

#include "expression.h"

class While : public Statement {
public:
    While(Expression *condition, Statement *statement)
            : condition_(condition), statement_(statement) {}

    bool Equals(const Statement &other) const override;

    std::string Print() const override;

    Expression *GetCondition_() const;

    std::string BuildIR(CFG *cfg) const override;

    Statement *GetStatement_() const;

    ~While();

    bool IsUseful() const override;

private:
    Expression *condition_;
    Statement *statement_;
};
