//
// Created by burca on 20/03/2018.
//
#include "operator.h"
#include <algorithm>
#include <iterator>

using namespace std;

int GetIndexFromLabel(const string &operator_name, int pos_in_rule) {
    if (operator_name == "++" && pos_in_rule == 1) {
        return 0;
    }
    if (operator_name == "--" && pos_in_rule == 1) {
        return 1;
    }
    if (operator_name == "++" && pos_in_rule == 0) {
        return 2;
    }
    if (operator_name == "--" && pos_in_rule == 0) {
        return 3;
    }
    if (operator_name == "+" && pos_in_rule == 0) {
        return 4;
    }
    if (operator_name == "-" && pos_in_rule == 0) {
        return 5;
    }
    if (operator_name == "+" && pos_in_rule == 1) {
        return 11;
    }
    if (operator_name == "-" && pos_in_rule == 1) {
        return 12;
    }
    return static_cast<int>(distance(operator_labels, find(operator_labels, operator_labels + 38, operator_name)));
}

Operator::Operator(const std::string &operator_name, unsigned int pos_operator) {
    this->identifier_ = GetIndexFromLabel(operator_name, pos_operator);
}

bool Operator::Equals(const Operator &other) const {
    return this->identifier_ == other.identifier_;
}
