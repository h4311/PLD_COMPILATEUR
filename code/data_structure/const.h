#pragma once

#include "expression.h"
#include "type.h"

// TODO review the structure of Const class
class Const : public Expression {
public:
    Const(CType type, int64_t value)
            : Expression(type), value_(value) {}

    bool Equals(const Statement &other) const override;

    std::string Print() const override;

    int64_t GetValue_() const;

    ~Const() override = default;

    std::string BuildIR(CFG *cfg) const override;

    bool HasSymbol() const override;

    bool IsUseful() const override;

    long *EvaluateExpression() override;

private:
    int64_t value_;
};
