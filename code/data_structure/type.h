//
// Created by Horia on 3/11/2018.
//
#pragma once

#include <string>
#include <iterator>
#include <algorithm>

enum CType {
    VOID = 0,
    CHAR = 1,
    INT32_T = 4,
    INT64_T = 8
};

class CTypeUtil {
public:

    static CType stringToCType(const std::string &type_name) {
        if (type_name == "void")
            return VOID;
        if (type_name == "char")
            return CHAR;
        if (type_name == "int32_t")
            return INT32_T;
        return INT64_T;
    }

    static CType GetBiggestType(CType type1, CType type2) {
        if (type1 > type2) {
            return type1;
        }
        return type2;
    }

};