//
// Created by Horia on 3/13/2018.
//
#include "unary_op_expression.h"

using namespace std;

bool UnaryOpExpression::Equals(const Statement &other) const {
    if (const auto *otherUnaryOpExpression = dynamic_cast<const UnaryOpExpression *>(&other)) {
        return this->expression_->Equals(*otherUnaryOpExpression->expression_)
               && this->expressionOperator_->Equals(*otherUnaryOpExpression->expressionOperator_);
    }
    return false;
}

string UnaryOpExpression::Print() const {
    return this->expressionOperator_->Print() + this->expression_->Print();
}

Expression *UnaryOpExpression::GetExpression_() const {
    return expression_;
}

UnaryOpExpression::~UnaryOpExpression() {
    delete this->expression_;
}

bool UnaryOpExpression::HasSymbol() const {
    return expression_->HasSymbol();
}

long *UnaryOpExpression::EvaluateExpression() {
    auto op = static_cast<int>(*expressionOperator_);
    long expression = *(expression_->EvaluateExpression());
    long toReturn;
    switch (op) {
        case UNARY_PLUS:
            return expression_->EvaluateExpression();
        case UNARY_MINUS:
            toReturn = -expression;
            break;
        case BITWISE_NOT:
            toReturn = ~expression;
            break;
        case LOGICAL_NOT:
            toReturn = !expression;
            break;
        default:
            break;
    }
    return new long(toReturn);
}

string UnaryOpExpression::BuildIR(CFG *cfg) const {
    string var1 = expression_->BuildIR(cfg);

    string res = cfg->CreateNewTempVar(GetType_());
    auto op = static_cast<int>(*expressionOperator_);
    switch (op) {
        case UNARY_PLUS:
            break;
        case UNARY_MINUS:
            cfg->currentBB_->AddIRInstr(IRInstruction::opposite, GetType_(), {res, var1});
            break;
        case BITWISE_NOT:
            cfg->currentBB_->AddIRInstr(IRInstruction::complement_bitwise, GetType_(), {res, var1});
            break;
        case LOGICAL_NOT:
            cfg->currentBB_->AddIRInstr(IRInstruction::negation, GetType_(), {res, var1});
            break;
        default:
            break;
    }

    return res;
}

bool UnaryOpExpression::IsUseful() const {
    return expression_->IsUseful();
}
