//
// Created by perewoulpy on 27/03/18.
//
#include "ir_instruction.h"
#include "basic_block.h"
#include "cfg.h"

void IRInstruction::FillParametersRegisters(std::ostream &o, bool hasRet) {
    int offset = hasRet ? 1 : 0;

    if (params_.size() == 1 + offset) o << "\tmovq\t$0" << ", %rdi" << std::endl; //I have no idea why, but gcc does it
    if (params_.size() > 1 + offset) o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rdi" << std::endl;
    if (params_.size() > 2 + offset) o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rsi" << std::endl;
    if (params_.size() > 3 + offset) o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[3]) << ", %rdx" << std::endl;
    if (params_.size() > 4 + offset) o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[4]) << ", %rcx" << std::endl;
    if (params_.size() > 5 + offset) o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[5]) << ", %r8" << std::endl;
    if (params_.size() > 6 + offset) o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[6]) << ", %r9" << std::endl;
}

void IRInstruction::GenAsm(std::ostream &o) {
    switch (op_) {
        case call:
            FillParametersRegisters(o, t_ != VOID);
            o << "\tcall\t" << params_[0] << "\t#call " << params_[0] << std::endl;
            if (t_ != VOID)
                o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[params_.size() - 1]) << std::endl;
            break;
        case ldconst:
            o << "\tmovq\t$" << params_[1] << ", " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#ldconst " << params_[0] << " <- " << params_[1] << std::endl;
            break;
        case copy:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#copy " << params_[0] << " <- " << params_[1] << std::endl;
            break;
        case wmem:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[0]) << ", %rax" << std::endl;
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %r10" << std::endl;
            o << "\tmovq\t%r10, (%rax)" << "\t#wmem (" << params_[0] << ") <- " << params_[1] << std::endl;
            break;
        case rmem:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tmovq\t(%rax), %r10" << std::endl;
            o << "\tmovq\t%r10, " << bb_->cfg_->IRRegToAsm(params_[0])
              << "\t#rmem " << params_[0] << " <- (" << params_[1] << ")" << std::endl;
            break;
        case add:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\taddq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#add " << params_[0] << " <- " << params_[1] << " + " << params_[2] << std::endl;
            break;
        case sub:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tsubq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#sub " << params_[0] << " <- " << params_[1] << " - " << params_[2] << std::endl;
            break;
        case mul:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\timulq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#mul " << params_[0] << " <- " << params_[1] << " * " << params_[2] << std::endl;
            break;
        case div:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tcqto\t" << std::endl; //sign extend for division
            o << "\tidivq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#div " << params_[0] << " <- " << params_[1] << " / " << params_[2] << std::endl;
            break;
        case rem:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tcqto\t" << std::endl; //sign extend for division
            o << "\tidivq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << std::endl;
            o << "\tmovq\t%rdx, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#rem " << params_[0] << " <- " << params_[1] << " % " << params_[2] << std::endl;
            break;
        case shl:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rcx" << std::endl;
            o << "\tshlq\t%cl, %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#shl " << params_[0] << " <- " << params_[1] << " << " << params_[2] << std::endl;
            break;
        case shr:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rcx" << std::endl;
            o << "\tshrq\t%cl, %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#shl " << params_[0] << " <- " << params_[1] << " << " << params_[2] << std::endl;
            break;
        case opposite:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tnegq\t" << " %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#negq " << params_[0] << " <- -" << params_[1] << std::endl;
            break;
        case complement_bitwise:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tnotq\t" << " %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#negq " << params_[0] << " <- ~" << params_[1] << std::endl;
            break;
        case negation :
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tcmpq\t" << "$0" << ", %rax" << std::endl;
            o << "\tsete\t%al" << std::endl; //set the value of the register to 1 if condition else 0
            o << "\tmovzbq\t%al, %rax"
              << std::endl; //move the value of the 8 bits registers extended to 64 bits with zeros
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#cmp_ne " << params_[0] << " <- !" << params_[1] << std::endl;
            break;
        case cmp_ne:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\txor\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#cmp_ne " << params_[0] << " <- " << params_[1] << " != " << params_[2] << std::endl;
            break;
        case cmp_eq:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tcmpq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rax" << std::endl;
            o << "\tsete\t%al" << std::endl; //set the value of the register to 1 if condition else 0
            o << "\tmovzbq\t%al, %rax"
              << std::endl; //move the value of the 8 bits registers extended to 64 bits with zeros
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#cmp_eq " << params_[0] << " <- " << params_[1] << " == " << params_[2] << std::endl;
            break;
        case cmp_le:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tcmpq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rax" << std::endl;
            o << "\tsetle\t%al" << std::endl;
            o << "\tmovzbq\t%al, %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#cmp_le " << params_[0] << " <- " << params_[1] << " <= " << params_[2] << std::endl;
            break;
        case cmp_lt:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tcmpq\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rax" << std::endl;
            o << "\tsetl\t%al" << std::endl;
            o << "\tmovzbq\t%al, %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#cmp_lt " << params_[0] << " <- " << params_[1] << " < " << params_[2] << std::endl;
            break;
        case bit_and:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tand\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#bit_and " << params_[0] << " <- " << params_[1] << " and " << params_[2] << std::endl;
            break;
        case bit_or:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\tor\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#bit_or " << params_[0] << " <- " << params_[1] << " or " << params_[2] << std::endl;
            break;
        case bit_xor:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[1]) << ", %rax" << std::endl;
            o << "\txor\t" << bb_->cfg_->IRRegToAsm(params_[2]) << ", %rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#bit_xor " << params_[0] << " <- " << params_[1] << " xor " << params_[2] << std::endl;
            break;
        case inc:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[0]) << ", %rax" << std::endl;
            o << "\tincq\t%rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#inc " << params_[0] << " <- " << params_[0] << " + 1 " << std::endl;
            break;
        case dec:
            o << "\tmovq\t" << bb_->cfg_->IRRegToAsm(params_[0]) << ", %rax" << std::endl;
            o << "\tdecq\t%rax" << std::endl;
            o << "\tmovq\t%rax, " << bb_->cfg_->IRRegToAsm(params_[0]);
            o << "\t#dec " << params_[0] << " <- " << params_[0] << " - 1 " << std::endl;
            break;
        default:
            break;
    }

}
