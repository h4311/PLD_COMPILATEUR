//
// Created by perewoulpy on 27/03/18.
//
#include "basic_block.h"
#include "cfg.h"
#include "statement.h"
#include "block.h"

#include <utility>

using namespace std;

void BasicBlock::GenAsm(std::ostream &o) {
    o << label_ << ":" << endl;

    for (IRInstruction *instruction : instrs_) {
        instruction->GenAsm(o);
    }
    if (exitTrue_ == nullptr) //the basic block is a leaf of the cfg
        o << "\tjmp\t" << cfg_->GetReturnLabel() << endl;
    else {
        if (exitFalse_ != nullptr) { //the basic block divides into two new basic blocks
            o << "\tcmpq\t$0, " << cfg_->IRRegToAsm(conditionVar_) << endl;
            o << "\tje\t" << exitFalse_->GetLabel_() << endl;
        }
        //in any case the basic block must have a jump to its exitTrue_
        o << "\tjmp\t" << exitTrue_->GetLabel_() << endl;
    }
}

BasicBlock::BasicBlock(CFG *cfg, std::string entryLabel) {
    cfg_ = cfg;
    label_ = entryLabel;
    cfg_->AddBB(this);
    exitTrue_ = nullptr;
    exitFalse_ = nullptr;
    exitsLocked_ = false;
}

void BasicBlock::SetExitTrue_(BasicBlock *block) {
    if (!exitsLocked_) {
        exitTrue_ = block;
    }
}

void BasicBlock::SetExitFalse_(BasicBlock *block) {
    if (!exitsLocked_) {
        exitFalse_ = block;
    }
}

void BasicBlock::AddIRInstr(IRInstruction::Operation op, CType t, std::vector<std::string> params) {
    IRInstruction *instruction = new IRInstruction(this, op, t, params);
    instrs_.push_back(instruction);
}

BasicBlock *BasicBlock::GetExitTrue_() const {
    return exitTrue_;
}

BasicBlock *BasicBlock::GetExitFalse_() const {
    return exitFalse_;
}

const string &BasicBlock::GetLabel_() const {
    return label_;
}

void BasicBlock::SetConditionVar_(const std::string &conditionVar) {
    this->conditionVar_ = conditionVar;
}

void BasicBlock::LockExits() {
    this->exitsLocked_ = true;
}

BasicBlock::~BasicBlock() {
    for (auto *i : instrs_) {
        delete i;
    }
    instrs_.clear();
    instrs_.shrink_to_fit();
}
