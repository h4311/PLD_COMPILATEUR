//
// Created by Horia on 3/12/2018.
//
#include "const.h"

using namespace std;

bool Const::Equals(const Statement &other) const {
    if (const auto *otherConst = dynamic_cast<const Const *>(&other)) {
        return this->type_ == otherConst->type_ && this->value_ == otherConst->value_;
    }
    return false;
}

string Const::Print() const {
    return this->type_ + "(" + to_string(this->value_) + ")";
}

int64_t Const::GetValue_() const {
    return value_;
}

bool Const::HasSymbol() const {
    return false;
}

bool Const::IsUseful() const {
    return false;
}

long *Const::EvaluateExpression() {
    return &value_;
}

string Const::BuildIR(CFG *cfg) const {
    string var = cfg->CreateNewTempVar(type_);
    cfg->currentBB_->AddIRInstr(IRInstruction::ldconst, type_, {var, to_string(value_)});
    return var;
}
