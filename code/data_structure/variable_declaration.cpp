//
// Created by Horia on 3/12/2018.
//
#include "variable_declaration.h"
#include "const.h"

using namespace std;

VariableDeclaration::VariableDeclaration(CType type, const string &id, Expression *value)
        : Definition(type, move(id)), array_(false), arraySize_(nullptr) {
    if (value != nullptr) {
        this->values_.push_back(value);
    }
}

VariableDeclaration::VariableDeclaration(CType type, const string &id, vector<Expression *> values,
                                         Expression *array_size)
        : Definition(type, move(id)), array_(true), values_(std::move(values)) {
    if (array_size) {
        this->arraySize_ = array_size;
    } else {
        this->arraySize_ = new Const(INT64_T, values.size());
    }
}

bool VariableDeclaration::Equals(const Definition &other) const {
    if (const auto *otherVariableDeclaration = dynamic_cast<const VariableDeclaration *>(&other)) {
        return this->Equals(*otherVariableDeclaration);
    }
    return false;
}

bool VariableDeclaration::Equals(const Statement &other) const {
    if (const auto *otherVariableDeclaration = dynamic_cast<const VariableDeclaration *>(&other)) {
        return this->Equals(*otherVariableDeclaration);
    }
    return false;
}

bool VariableDeclaration::Equals(const VariableDeclaration &other) const {
    if (this->type_ != other.type_) {
        return false;
    }
    if (this->id_ != other.id_) {
        return false;
    }
    if (this->array_) {
        if (!this->arraySize_->Equals(*other.arraySize_)) {
            return false;
        }
    }
    if (this->values_.size() != other.values_.size()) {
        return false;
    }
    for (int i = 0; i < this->values_.size(); i++) {
        if (!this->values_[i]->Equals(*other.values_[i])) {
            return false;
        }
    }
    return true;
}

string VariableDeclaration::Print() const {
    string to_return = Definition::Print();
    if (!this->array_) {
        if (!this->values_.empty()) {
            to_return += "(" + this->values_[0]->Print() + ")";
        }
    } else {
        to_return += "[" + this->arraySize_->Print() + "]";
        if (!this->values_.empty()) {
            to_return += "({";
            for (int i = 0; i < this->values_.size() - 1; i++) {
                to_return += this->values_[i]->Print() + ", ";
            }
            to_return += this->values_.back()->Print() + "})";
        }
    }
    return to_return;
}

void VariableDeclaration::InitVariable(Expression *value) {
    this->values_[0] = value;
}

void VariableDeclaration::InitVariable(Expression *value, unsigned long index) {
    this->values_[index] = value;
}

const bool VariableDeclaration::GetArray_() const {
    return array_;
}

const vector<Expression *> &VariableDeclaration::GetValues_() const {
    return values_;
}

Expression *VariableDeclaration::GetArraySize_() const {
    return arraySize_;
}

VariableDeclaration::~VariableDeclaration() {
    for (auto *value : this->values_) {
        delete value;
    }
    this->values_.clear();
    if (arraySize_ != nullptr)
        delete this->arraySize_;
}

bool VariableDeclaration::HasSymbolInExpression() {
    for (Expression *expression : values_) {
        if (expression->HasSymbol())
            return true;
    }
    if (arraySize_ != nullptr)
        if (arraySize_->HasSymbol())
            return true;

    return false;
}

string VariableDeclaration::BuildIR(CFG *cfg) const {
    if (!used_)
        return "nope";
    if (values_.empty())
        return "nope";
    if (!array_) {
        string rValue = values_[0]->BuildIR(cfg);
        cfg->currentBB_->AddIRInstr(IRInstruction::copy, GetType_(), {GetId_(), rValue});
        return "nope";
    }

    long eval = *(arraySize_->EvaluateExpression());

    long limit = (values_.size() < eval) ? values_.size() : eval;

    if (limit < 1)
        return "nope";
    if (limit == 1) {
        string rValue = values_[0]->BuildIR(cfg);
        cfg->currentBB_->AddIRInstr(IRInstruction::copy, GetType_(), {GetId_(), rValue});
    } else {
        int offset = -cfg->GetVarIndex(GetId_());
        string var = cfg->CreateNewTempVar(INT64_T);
        cfg->currentBB_->AddIRInstr(IRInstruction::ldconst, INT64_T, {var, to_string(offset)});
        cfg->currentBB_->AddIRInstr(IRInstruction::add, INT64_T, {var, "!bp", var});
        string arrayElemSize = cfg->CreateNewTempVar(INT64_T);
        cfg->currentBB_->AddIRInstr(IRInstruction::ldconst, INT64_T,
                                    {arrayElemSize, to_string(8/*GetType_()*/)});

        string rValue = values_[0]->BuildIR(cfg);
        cfg->currentBB_->AddIRInstr(IRInstruction::wmem, GetType_(), {var, rValue});
        for (int i = 1; i < limit; i++) {
            cfg->currentBB_->AddIRInstr(IRInstruction::add, INT64_T, {var, arrayElemSize, var});
            rValue = values_[i]->BuildIR(cfg);
            cfg->currentBB_->AddIRInstr(IRInstruction::wmem, GetType_(), {var, rValue});
        }
    }
    return "nope";
}

long *VariableDeclaration::EvaluateSize() {
    if (arraySize_ != nullptr)
        return arraySize_->EvaluateExpression();
    return nullptr;
}

bool VariableDeclaration::IsUseful() const {
    return true;
}
