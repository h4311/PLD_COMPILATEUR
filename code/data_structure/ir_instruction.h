//
// Created by burca on 3/26/2018.
//
#pragma once

#include <vector>
#include <string>
#include <ostream>
#include "type.h"

class BasicBlock;

/** The class for one 3-address instruction */
class IRInstruction {
public:
    /** The instructions themselves -- feel free to subclass instead */
    typedef enum {
        ldconst,
        add,
        sub,
        mul,
        div,
        rem,
        shl,
        shr,
        rmem,
        wmem,
        call,
        opposite,
        complement_bitwise,
        negation,
        cmp_ne,
        cmp_eq,
        cmp_lt,
        cmp_le,
        bit_and,
        bit_or,
        bit_xor,
        copy,
        inc,
        dec
    } Operation;

    /**  constructor */
    IRInstruction(BasicBlock *bb, Operation op, CType t, std::vector<std::string> params)
            : bb_(bb), op_(op), t_(t), params_(std::move(params)) {}

    /** Actual code generation */
    void GenAsm(std::ostream &o); /**< x86 assembly code generation for this IR instruction */

    virtual ~IRInstruction() = default;

private:
    void FillParametersRegisters(std::ostream &o, bool hasRet);

    BasicBlock *bb_; /**< The BB this instruction belongs to, which provides a pointer to the CFG this instruction belong to */
    Operation op_;
    CType t_;
    std::vector<std::string> params_; /**< For 3-op instrs: d, x, y; for ldconst: d, c;  For call: label, d, params;  for wmem and rmem: choose yourself */
};
