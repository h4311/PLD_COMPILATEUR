//
// Created by Horia on 3/11/2018.
//
#include "function_call.h"

using namespace std;

bool FunctionCall::Equals(const Statement &other) const {
    if (const auto *otherFunctionCall = dynamic_cast<const FunctionCall *>(&other)) {
        if (this->id_ != otherFunctionCall->id_) {
            return false;
        }
        if (this->arguments_.size() != otherFunctionCall->arguments_.size()) {
            return false;
        }
        for (int i = 0; i < this->arguments_.size(); i++) {
            if (!this->arguments_[i]->Equals(*otherFunctionCall->arguments_[i])) {
                return false;
            }
        }
        return true;
    }
    return false;
}

string FunctionCall::Print() const {
    string to_return = this->id_ + "(";
    for (int i = 0; i < (int) this->arguments_.size() - 1; i++) {
        to_return += this->arguments_[i]->Print() + ",";
    }
    to_return += (this->arguments_.empty() ? "" : this->arguments_.back()->Print()) + ")";
    return to_return;
}

const string &FunctionCall::GetId_() const {
    return id_;
}

const vector<Expression *> &FunctionCall::GetArguments_() const {
    return arguments_;
}

FunctionCall::~FunctionCall() {
    for (Expression *&argument : this->arguments_) {
        delete argument;
    }
    this->arguments_.clear();
    this->arguments_.shrink_to_fit();
}

bool FunctionCall::HasSymbol() const {
    return true;
}

bool FunctionCall::IsUseful() const {
    return true;
}

long *FunctionCall::EvaluateExpression() {
    return nullptr;
}

string FunctionCall::BuildIR(CFG *cfg) const {
    vector<string> params;
    for (Expression *expression : arguments_) {
        params.push_back(expression->BuildIR(cfg));
    }

    if (GetType_() != VOID) {
        params.push_back(cfg->CreateNewTempVar(GetType_()));
    }

    params.insert(params.begin(), id_);

    cfg->currentBB_->AddIRInstr(IRInstruction::call, GetType_(), params);

    return params[params.size() - 1];
}
