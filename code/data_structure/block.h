#pragma once

#include "statement.h"
#include "scope.h"
#include "variable_declaration.h"
#include <utility>
#include <vector>

class Block : public Scope, public Statement {
public:

    Block() = default;

    Block(Scope *enclosingScope, std::string name)
            : Scope(enclosingScope, name) {}

    explicit Block(std::vector<Statement *> statements)
            : statements_(std::move(statements)) {}

    bool Equals(const Statement &other) const;

    std::string Print() const;

    void AddStatement(Statement *statement);

    std::vector<Statement *> GetStatements_() const;

    ~Block();

    std::string BuildIR(CFG *cfg) const override;

    bool IsUseful() const override;

private:
    std::vector<Statement *> statements_;
};
