//
// Created by Horia on 3/11/2018.
//
#pragma once

#include <string>

enum Operators {
    SUFFIX_INCREMENT,
    SUFFIX_DECREMENT,
    PREFIX_INCREMENT,
    PREFIX_DECREMENT,
    UNARY_PLUS,
    UNARY_MINUS,
    LOGICAL_NOT,
    BITWISE_NOT,
    MULTIPLICATION,
    DIVISION,
    REMAINDER,
    ADDITION,
    SUBTRACTION,
    BITWISE_LSHIFT,
    BITWISE_RSHIFT,
    LESS,
    LESSEQUAL,
    MORE,
    MOREEQUAL,
    EQUAL,
    NOTEQUAL,
    BITWISE_AND,
    BITWISE_XOR,
    BITWISE_OR,
    AND,
    OR,
    ASSIGNMENT,
    ASSIGNMENT_SUM,
    ASSIGNMENT_DIFFERENCE,
    ASSIGNMENT_PRODUCT,
    ASSIGNMENT_QUOTIENT,
    ASSIGNMENT_REMAINDER,
    ASSIGNMENT_BITWISE_LSHIFT,
    ASSIGNMENT_BITWISE_RSHIFT,
    ASSIGNMENT_BITWISE_AND,
    ASSIGNMENT_BITWISE_XOR,
    ASSIGNMENT_BITWISE_OR,
    COMMA
};

//TODO : turn into not global
const std::string operator_labels[] = {"++", "--", "++", "--", "+", "-", "!", "~", "*", "/", "%", "+", "-", "<<", ">>",
                                       "<", "<=", ">", ">=", "==", "!=", "&", "^", "|", "&&", "||", "=", "+=", "-=",
                                       "*=", "/=", "%=", "<<=", ">>=", "&=", "^=", "|=", ","};

class Operator {
public:
    explicit Operator(int identifier)
            : identifier_(identifier) {}

    Operator(const std::string &operator_name, unsigned int pos_operator);

    ~Operator() = default;

    bool Equals(const Operator &other) const;

    std::string Print() const { return operator_labels[this->identifier_]; }

    explicit operator int() const { return identifier_; }

protected:
    int identifier_;
};