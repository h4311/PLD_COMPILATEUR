#pragma once

#include "definition.h"
#include "variable_declaration.h"
#include "block.h"
#include <string>
#include <utility>

class FunctionDefinition : public Definition {
public:
    FunctionDefinition(CType type, std::string id, std::vector<VariableDeclaration *> parameters)
            : Definition(type, std::move(id)), parameters_(std::move(parameters)) {}

    bool Equals(const Definition &other) const override;

    std::string Print() const override;

    const std::vector<VariableDeclaration *> &GetParameters_() const;

    Block *GetBlock_() const;

    void AddBlock_(Block *block);

    void BuildIR(CFG *cfg) const;

    ~FunctionDefinition();

private:
    std::vector<VariableDeclaration *> parameters_;
    Block *block_;
};
