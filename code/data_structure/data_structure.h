//
// Created by perewoulpy on 27/03/18.
//

#pragma once

#include "binary_op_expression.h"
#include "binary_op_assignment.h"
#include "block.h"
#include "break.h"
#include "const.h"
#include "continue.h"
#include "definition.h"
#include "expression.h"
#include "function_call.h"
#include "function_definition.h"
#include "if.h"
#include "op_expression.h"
#include "operator.h"
#include "program.h"
#include "return.h"
#include "scope.h"
#include "statement.h"
#include "type.h"
#include "unary_op_assignment.h"
#include "unary_op_expression.h"
#include "variable_declaration.h"
#include "variable_id.h"
#include "while.h"

#include "cfg.h"
#include "basic_block.h"
#include "ir_instruction.h"
