#pragma once

#include "statement.h"

class Continue : public Statement {
public:
    Continue() = default;

    bool Equals(const Statement &other) const override { return dynamic_cast<const Continue *>(&other) != nullptr; }

    std::string Print() const override { return "continue;"; }

    std::string BuildIR(CFG *cfg) const override {
        cfg->currentBB_->SetExitTrue_(cfg->currentLoop_.top().first);
        cfg->currentBB_->SetExitFalse_(nullptr);
        cfg->currentBB_->LockExits();
        return "nope";
    }

    bool IsUseful() const override {
        return true;
    }

    ~Continue() override = default;
};
