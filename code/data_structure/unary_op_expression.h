#pragma once

#include "op_expression.h"

class UnaryOpExpression : public OpExpression {
public:
    UnaryOpExpression(Expression *expression, Operator *expression_operator)
            : OpExpression(expression_operator), expression_(expression) {}

    bool Equals(const Statement &other) const override;

    std::string Print() const override;

    Expression *GetExpression_() const;

    std::string BuildIR(CFG *cfg) const override;

    ~UnaryOpExpression() override;

    bool HasSymbol() const override;

    bool IsUseful() const override;

    long *EvaluateExpression() override;

private:
    Expression *expression_;
};
